package com.gnome.page.idea.fileUpload;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.gnome.helper.cloudStrage.CloudStorageHelper;

@Service
public class FileUploadService {
	
	@Autowired
	private CloudStorageHelper cloudStorageHelper;
	
	/**
	 * 添付ファイルデータを登録する
	 * 
	 * 
	 * @param form
	 * @param request
	 * @param response  
	 * @throws IOException 
	 */
	@Transactional(rollbackFor=Exception.class)
	public List<String> upload(FileUploadForm form) throws IOException {
		
		List<String> urls = new ArrayList<String>();
		
		//アップロード対象ファイルのlistのloopを行う
		for(MultipartFile file : form.getUploadFiles()) {

			Date date = new Date();
			SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/ddHH:mm:ss");
			String sysdate = df.format(date);

			//ファイル名取得
			String fileName = sysdate + getSuffix(file.getOriginalFilename());

			//upload
			String url = cloudStorageHelper.uploadFile(file,fileName);
			urls.add(url);
		}
		return urls;
	}
	
	private String getSuffix(String fileName) {
	    if (fileName == null)
	        return null;
	    int point = fileName.lastIndexOf(".");
	    if (point != -1) {
	        return fileName.substring(point);
	    }
	    return fileName;
	}
}