package com.gnome.page.idea.fileUpload;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

@RestController
public class FileUploadController {
	
	@Autowired
 	private FileUploadService service;
	
	/**
	 * アップロード画面
	 * 
	 * @param form
	 * @param errors
	 * @param model
	 * @param isDrop
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(path="/file-upload", method = RequestMethod.POST)
	@ResponseBody
	public String upload(FileUploadForm form) throws IOException {
		
		Gson gson = new Gson();
		
		return gson.toJson(service.upload(form));
	}
}