package com.gnome.page.idea.fileUpload;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class FileUploadForm {

	// アップロードファイル
	private List<MultipartFile> uploadFiles;
		
	public List<MultipartFile> getUploadFiles() {
		return uploadFiles;
	}

	public void setUploadFiles(List<MultipartFile> uploadFiles) {
		this.uploadFiles = uploadFiles;
	}
}
