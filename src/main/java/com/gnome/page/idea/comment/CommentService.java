package com.gnome.page.idea.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gnome.base.entity.Comment;
import com.gnome.base.repository.CommentRepository;
import com.gnome.common.consts.MessageIdConsts;
import com.gnome.common.exception.BusinessException;
import com.gnome.common.util.StringUtil;

@Service
public class CommentService {
	
	@Autowired
	private CommentRepository repo;
	
	@Autowired
	private MessageSource msgSource;
	
	/**
	 * 新規作成処理
	 * @param form
	 * @throws BusinessException
	 */
	@Transactional(rollbackFor=Exception.class)
	public void create(CommentForm form) throws BusinessException{
		
		//コメントの内容がない場合、エラー
		if(StringUtil.isEmpty(form.getContents())) {
			throw new BusinessException(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000005, new String[] {"コメントの内容"}, null));
		}
		
		Comment comment = new Comment();
		comment.setIdeaId(StringUtil.toLong(form.getIdeaId()));
		comment.setContents(form.getContents());
		
		repo.saveAndFlush(comment);
	}
}
