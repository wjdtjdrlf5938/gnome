package com.gnome.page.idea.comment;

import com.gnome.base.common.BaseForm;

public class CommentForm extends BaseForm{
	
	//アイデアid
	private String ideaId;
	
	//コメントの内容
	private String contents;
	
	// エラーメッセージ
	private String errorMsg;
	
	public String getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(String ideaId) {
		this.ideaId = ideaId;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
}
