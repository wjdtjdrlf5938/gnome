package com.gnome.page.idea.ideaEdit;

import java.util.List;

public class IdeaEditForm{
	
	//id
	private Long id;
	
	//タイトル
	private String subject;
	
	//内容
	private String contents;
	
	//カテゴリーid
	private Long categoryId;
	
	//仮登録判断
	private boolean isTemporary;
	
	//アイデアの作成
	private Long createdBy;
	
	//アイデア検索条件
	private String searchContents;
	
	//アイデア検索結果
	private List<IdeaEditEntity> ideaList;
	
	// エラーリスト
	private List<String> errList;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public boolean isTemporary() {
		return isTemporary;
	}

	public void setTemporary(boolean isTemporary) {
		this.isTemporary = isTemporary;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getSearchContents() {
		return searchContents;
	}

	public void setSearchContents(String searchContents) {
		this.searchContents = searchContents;
	}

	public List<IdeaEditEntity> getIdeaList() {
		return ideaList;
	}

	public void setIdeaList(List<IdeaEditEntity> ideaList) {
		this.ideaList = ideaList;
	}

	public List<String> getErrList() {
		return errList;
	}

	public void setErrList(List<String> errList) {
		this.errList = errList;
	}
}
