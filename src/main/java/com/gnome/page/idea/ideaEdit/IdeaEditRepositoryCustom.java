package com.gnome.page.idea.ideaEdit;

import java.util.List;

public interface IdeaEditRepositoryCustom {
	
	public List<IdeaEditEntity> findIdea(IdeaEditForm form, Long spaceId);

}
