package com.gnome.page.idea.ideaEdit;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gnome.common.exception.BusinessException;
import com.google.gson.Gson;

@RestController
public class IdeaEditController {
	
	@Autowired
	private IdeaEditService service;
	
	/**
	 * アイデア作成検索
	 * @param form
	 * @return
	 */
	@RequestMapping(path="/edit/search", method = RequestMethod.GET)
	@ResponseBody
	public String search(IdeaEditForm form) {
		
		Gson gson = new Gson();
		
		try {
			
			//検索処理
			service.search(form);
			
		}catch (BusinessException e) {
			e.printStackTrace();
			form.setErrList(e.getErrors());
		}
		
		return gson.toJson(form);
	}
	
	/**
	 * アイデア新規作成
	 * @param form
	 * @return
	 */
	@RequestMapping(path="/create", method = RequestMethod.POST)
	@ResponseBody
	public String create(IdeaEditForm form) {
		
		Gson gson = new Gson();
		
		try {
			
			//作成処理
			service.create(form);
			
		}catch (BusinessException e) {
			e.printStackTrace();
			form.setErrList(e.getErrors());
		}
		
		return gson.toJson(form);
	}
	
	/**
	 * 仮登録したアイデア削除処理
	 * @param form
	 * @return
	 */
	@RequestMapping(path="/delete", method = RequestMethod.DELETE)
	@ResponseBody
	public String delete(IdeaEditForm form, RedirectAttributes attributes) {
		
		Gson gson = new Gson();
		
		//削除処理
		service.delete(form);
		
		return gson.toJson(form);
	}
}
