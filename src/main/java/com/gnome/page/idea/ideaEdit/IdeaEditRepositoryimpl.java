package com.gnome.page.idea.ideaEdit;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class IdeaEditRepositoryimpl implements IdeaEditRepositoryCustom{
	
	@Autowired
	private EntityManager entityManager;
	
	@Override
	public List<IdeaEditEntity> findIdea(IdeaEditForm form, Long spaceId) {
		
		String sql = " SELECT "
				+ " ideas.id,"
				+ " ideas.subject,"
				+ " statuses.name AS status_name,"
				+ "  (up_votes.up_count * settings.positive_point) + (ifnull(down_votes.down_count,0) * settings.negative_point) AS point ,"
				+ " ideas.created_by AS user_id,"
				+ " users.profile_url,"
				+ " ifnull(comment.comments_count,0) AS count_comments"
				+ " FROM ideas";
		
		String joinSql = ""
				
				//statuses.name
				+ " inner JOIN developments "
				+ "   ON developments.idea_id = ideas.id"
				+ " inner join statuses"
				+ "   ON statuses.id = developments.status_id"
				
				//up_votes.up_count
				+ " LEFT JOIN "
				+ "   (SELECT count(distinct votes.id) AS up_count, votes.idea_id"
				+ "    FROM votes"
				+ "    WHERE votes.is_agreed = true"
				+ "    GROUP BY votes.idea_id) up_votes"
				+ "   ON up_votes.idea_id = ideas.id"
				
				//down_votes.down_count
				+ " LEFT JOIN "
				+ "   (SELECT count(distinct votes.id) AS down_count,  votes.idea_id "
				+ "    FROM votes "
				+ "    WHERE votes.is_agreed = false"
				+ "    GROUP BY votes.idea_id) down_votes"
				+ "   ON down_votes.idea_id = ideas.id"
				
				//spaceI
				+ " inner join "
				+ "   settings "
				+ " on settings.space_id = :spaceId "
				
				//users.profile_url
				+ " inner JOIN users"
				+ "   ON users.id = ideas.created_by "
				
				//comment.comments_count
				+ " LEFT JOIN " 
				+ "  (SELECT count(comments.id) AS comments_count, comments.idea_id "
				+ "   FROM comments "
				+ "   GROUP BY comments.idea_id) comment"
				+ "  ON comment.idea_id = ideas.id ";
		
		String conditionSql = " WHERE ideas.is_temporary = false AND ideas.merged_id IS null AND ideas.space_id = :spaceId "
				+ " AND ( ideas.subject LIKE :searchContents OR ideas.contents LIKE :searchContents )";
		
		String orderBy = " ORDER BY point DESC LIMIT 5 ";
		
		Query findQuery =  entityManager.createNativeQuery(sql + joinSql + conditionSql + orderBy, IdeaEditEntity.class);
		
		findQuery.setParameter("spaceId", spaceId);
		findQuery.setParameter("searchContents", "%" + form.getSearchContents() + "%");
		
		@SuppressWarnings("unchecked")
		List<IdeaEditEntity> ideas = findQuery.getResultList();
		
		return ideas;
		
	}
}
