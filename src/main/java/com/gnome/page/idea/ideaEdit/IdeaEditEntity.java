package com.gnome.page.idea.ideaEdit;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class IdeaEditEntity {
	
	@Id
	//id
	private Long id;
	
	//アイデアタイトル
	private String subject;
	
	//ステータス名
	private String statusName;
	
	// ポイント
	private Long point;
	
	//作成者Id
	private Long userId;
	
	// プロフィール画像
	private String profileUrl;
	
	//コメント数
	private Long countComments;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public Long getPoint() {
		return point;
	}

	public void setPoint(Long point) {
		this.point = point;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}

	public Long getCountComments() {
		return countComments;
	}

	public void setCountComments(Long countComments) {
		this.countComments = countComments;
	}
}
