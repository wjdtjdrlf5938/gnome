package com.gnome.page.idea.ideaEdit;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.gnome.common.util.DecimalUtil;
import com.gnome.common.util.StringUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gnome.auth.LoginUser;
import com.gnome.base.entity.Idea;
import com.gnome.base.entity.Development;
import com.gnome.base.entity.Status;
import com.gnome.base.entity.Vote;
import com.gnome.base.entity.User;
import com.gnome.base.repository.DevelopmentRepository;
import com.gnome.base.repository.IdeaRepository;
import com.gnome.base.repository.StatusRepository;
import com.gnome.base.repository.VoteRepository;
import com.gnome.common.consts.MessageIdConsts;
import com.gnome.common.exception.BusinessException;

@Service
public class IdeaEditService {
	
	@Autowired
	private IdeaEditRepositoryCustom customRepo;
	
	@Autowired
	private IdeaRepository repo;
	
	@Autowired
	private VoteRepository voteRepo;
	
	@Autowired
	private DevelopmentRepository developmentRepo;
	
	@Autowired
	private StatusRepository statusRepo;
	
	@Autowired
	private MessageSource msgSource;
	
	
	@Transactional(rollbackFor=Exception.class)
	public void search(IdeaEditForm form) throws BusinessException {
		
		//タイトル入力チェック
		nameCheck(form);
		
		// スペースID
		Long spaceId = ((LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getLoginUser().getSpaceId();
		
		List<IdeaEditEntity> ideaList =  customRepo.findIdea(form, spaceId);
		
		form.setIdeaList(ideaList);
		
	}
	
	/**
	 * アイデア作成・更新処理
	 * @param form
	 * @throws NoSuchMessageException
	 * @throws BusinessException
	 */
	@Transactional(rollbackFor=Exception.class)
	public void create(IdeaEditForm form) throws NoSuchMessageException, BusinessException{
		
		//バリデーションチェック
		validation(form);
		
		//ログインしたユーザー情報
		User loginUser = ((LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getLoginUser();
		
		Idea idea = new Idea();
		//更新処理の場合、
		if(!DecimalUtil.isEmpty(form.getId())) {
			idea = repo.findById(form.getId()).get();
		}
		idea.setSpaceId(loginUser.getSpaceId());
		idea.setSubject(form.getSubject());
		idea.setContents(form.getContents());
		idea.setCategoryId(form.getCategoryId());
		idea.setTemporary(form.isTemporary());
		
		Idea ideaResult = repo.saveAndFlush(idea);
		
		//新規作成の場合、
		if(DecimalUtil.isEmpty(form.getId())) {
			ideaVote(form, ideaResult, loginUser);
		}
		
		//アイデア作成者設定（仮登録ユーザー詳細画面移動用）
		form.setCreatedBy(ideaResult.getCreatedBy());
	}
	
	/**
	 * アイデア削除処理
	 * @param form
	 */
	@Transactional(rollbackFor=Exception.class)
	public void delete(IdeaEditForm form) {
		
		//仮登録したアイデア削除処理
		repo.deleteById(form.getId());
	}
	
	
	/**
	 * バリデーションチェック
	 * @param form
	 * @throws NoSuchMessageException
	 * @throws BusinessException
	 */
	private void validation(IdeaEditForm form) throws BusinessException {
		
		List<String> errs = new ArrayList<String>();
		
		//カテゴリーがない場合、
		if(StringUtil.isEmpty(form.getCategoryId())) {
			errs.add(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000005, new String[] {"カテゴリー"}, null));
		}
		
		//内容がない場合、
		if(StringUtil.isEmpty(form.getContents())) {
			errs.add(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000005, new String[] {"アイデアの内容"}, null));
		}
		
		//初期のステータスがない場合、
		Optional<Status> optionalStatus = statusRepo.findByIsInitial(true);
		if(!optionalStatus.isPresent()) {
			errs.add(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000005, new String[] {"初期ステータスが存在しないためアイデア登録"}, null));
		}
		
		if (errs.size() > 0) {
			throw new BusinessException(errs);
		}
	}
	
	private void nameCheck(IdeaEditForm form) throws BusinessException {
		
		List<String> errs = new ArrayList<String>();
		
		//タイトルがない場合、
		if(StringUtil.isEmpty(form.getSearchContents())) {
			errs.add(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000005, new String[] {"タイトル"}, null));
		}
		
		if (errs.size() > 0) {
			throw new BusinessException(errs);
		}
	}
	
	/**
	 * アイデア初期投票・開発設定
	 * @param form
	 * @param ideaResult
	 */
	private void ideaVote(IdeaEditForm form, Idea ideaResult, User loginUser) {
		
		form.setId(ideaResult.getId());
		
		//自分のアイデア初期投票
		Vote votes = new Vote();
		votes.setIdeaId(form.getId());
		
		votes.setUserId(loginUser.getId());
		//自分が投稿したアイデアは賛成投票
		votes.setAgreed(true);
		voteRepo.saveAndFlush(votes);
		
		//開発テーブルに初期状況設定
		Development develop = new Development();
		develop.setIdeaId(form.getId());
		develop.setStatusId(statusRepo.findByIsInitial(true).get().getId());
		developmentRepo.saveAndFlush(develop);
	}
}
