package com.gnome.page.idea.ideaDetail;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class IdeaDetailEntitiy {
	
	@Id
    private Long id;
	
	//タイトル
	private String subject;
	
	//内容
	private String contents;
	
	//マージしたid
	private Long mergedId;
		
	//投票(賛成・反対)	
	private Long agreed;
	
	//カテゴリーid
	private Long categoryId;
	
	//カテゴリー名
	private String categoryName;
	
	//ステータス名
	private String statusName;
	
	// ステータス完了フラグ
	private boolean isCompleted;
	
	//お気に入りフラグ
	private boolean isFavorited;
	
	//親カテゴリーid
	private Long parentCategoryId;
	
	//アイデア作成者
	private String userName;
	
	// プロフィール画像
	private String profileUrl;
	
	//総投票数
	private Long voteCount;
	
	// 賛成投票数
	private Long upCount;
	
	// 賛成投票数
	private Long downCount;
	
	// ポイント
	private Long point;
	
	//仮登録判断
	private Long temporary;
	
	//アイデア作成日
	private String createdAt;
	
	//アイデア作成者
	private Long userId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public Long getMergedId() {
		return mergedId;
	}

	public void setMergedId(Long mergedId) {
		this.mergedId = mergedId;
	}

	public Long getAgreed() {
		return agreed;
	}

	public void setAgreed(Long agreed) {
		this.agreed = agreed;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public boolean isCompleted() {
		return isCompleted;
	}

	public void setCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	public boolean isFavorited() {
		return isFavorited;
	}

	public void setFavorited(boolean isFavorited) {
		this.isFavorited = isFavorited;
	}

	public Long getParentCategoryId() {
		return parentCategoryId;
	}

	public void setParentCategoryId(Long parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}

	public Long getVoteCount() {
		return voteCount;
	}

	public void setVoteCount(Long voteCount) {
		this.voteCount = voteCount;
	}

	public Long getUpCount() {
		return upCount;
	}

	public void setUpCount(Long upCount) {
		this.upCount = upCount;
	}

	public Long getDownCount() {
		return downCount;
	}

	public void setDownCount(Long downCount) {
		this.downCount = downCount;
	}

	public Long getPoint() {
		return point;
	}

	public void setPoint(Long point) {
		this.point = point;
	}

	public Long getTemporary() {
		return temporary;
	}

	public void setTemporary(Long temporary) {
		this.temporary = temporary;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}	
}
