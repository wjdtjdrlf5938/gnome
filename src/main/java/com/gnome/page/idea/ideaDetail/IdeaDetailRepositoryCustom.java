package com.gnome.page.idea.ideaDetail;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.gnome.base.entity.User;

public interface IdeaDetailRepositoryCustom {
	
	public Optional<IdeaDetailEntitiy> findByIdeaId(Long ideaId, User loginUser);
	
	public Page<IdeaDetailCommentEntity> findComments(IdeaDetailForm form, Pageable pageable, int count);
	
	public IdeaDetailCommentEntity findAdminComment(Long ideaId);

}
