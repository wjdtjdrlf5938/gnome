package com.gnome.page.idea.ideaDetail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gnome.common.exception.BusinessException;

@Controller
public class IdeaDetailController {

	@Autowired
	private IdeaDetailService service;
	
	/**
	 * アイデア詳細検索
	 * @param form
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/ideas/{id}")
    public String index(@PathVariable("id") String id, IdeaDetailForm form, BindingResult errors, Model model) {
		
		try {
			
			//アイデアid設定
			form.setId(id);
			
			//詳細検索処理
			service.search(form);
			
			model.addAttribute("name", form.getEntity().getSubject());
			
		}catch(BusinessException e){
			e.printStackTrace();
			for(String obj : e.getErrors()){
				errors.reject("", obj);
			}
		}
		
		return "idea/ideaDetail";
	}
}
