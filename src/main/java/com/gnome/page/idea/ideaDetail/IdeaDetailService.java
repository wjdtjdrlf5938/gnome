package com.gnome.page.idea.ideaDetail;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gnome.auth.LoginUser;
import com.gnome.base.entity.User;
import com.gnome.base.repository.CommentRepository;
import com.gnome.base.repository.IdeaRepository;
import com.gnome.common.consts.MessageIdConsts;
import com.gnome.common.exception.BusinessException;
import com.gnome.common.util.StringUtil;



@Service
public class IdeaDetailService {
	
	@Autowired
	private IdeaDetailRepositoryCustom customRepo;
	
	@Autowired
	private CommentRepository commentRepo;
	
	@Autowired
	private MessageSource msgSource;
	
	@Autowired
	private IdeaRepository ideaRepo;
	
	/**
	 * アイデア詳細検索処理
	 * @param form
	 */
	@Transactional(rollbackFor=Exception.class)
	public void search(IdeaDetailForm form) throws BusinessException{
		
		//ログイン情報
		User loginUser = ((LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getLoginUser();
		
		Long ideaId = StringUtil.toLong(form.getId());
		
		Optional<IdeaDetailEntitiy> optional = customRepo.findByIdeaId(ideaId, loginUser);
		
		// 存在しない場合、エラー
		if(!optional.isPresent()) {
			throw new BusinessException(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000001, null, null));
		}
		
		form.setEntity(optional.get());
		
		//アイデアに紐付けるコメントリスト
		Pageable pageable = PageRequest.of(form.getPageOfInt(), form.getRecordsOfInt());
		
		//コメント検索
		int totalCnt = commentRepo.countByIdeaId(ideaId);
		form.setCommentCnt(totalCnt);
		
		Page<IdeaDetailCommentEntity> ideaCommentList = customRepo.findComments(form, pageable, totalCnt);	//コメント検索
		form.setFirst(ideaCommentList.isFirst());
		form.setLast(ideaCommentList.isLast());
		form.setNumber(ideaCommentList.getNumber());
		form.setTotalPages(ideaCommentList.getTotalPages());
		form.setCommentList(ideaCommentList.getContent());
		
		//最新管理者コメント
		form.setAdminComment(customRepo.findAdminComment(ideaId));
		
		// マージ数
		form.setMergedCnt(ideaRepo.countByMergedId(ideaId));
	}
}
