package com.gnome.page.idea.ideaDetail;

import java.sql.Timestamp;

import javax.persistence.Entity;

import com.gnome.base.common.BaseEntity;

@Entity
public class IdeaMergedListEntity extends BaseEntity{
	
	private static final long serialVersionUID = 1L;

	//タイトル
	private String subject;
		
	//内容
	private String contents;
	
	//アイデア作成者名
	private String userName;
	
	//アイデア作成者名Id
	private Long createdBy;
		
	//アイデア作成者日
	private Timestamp createdAt;

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContents() {
		
		String contentTag = contents.replaceAll("\\<.*?>","");
		return contentTag;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
}
