package com.gnome.page.idea.ideaDetail;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.gnome.base.entity.User;

@Repository
public class IdeaDetailRepositoryImpl implements IdeaDetailRepositoryCustom{
	
	@Autowired
	 private EntityManager entityManager;
	
	@Override
	public Optional<IdeaDetailEntitiy> findByIdeaId(Long ideaId, User loginUser) {
		
		String sql = " SELECT "
				+ " ideas.id,"
				+ " ideas.subject,"
				+ " ideas.contents,"
				+ " ideas.merged_id,"
				+ " user_vote.is_agreed AS agreed,"
				+ " ideas.category_id,"
				+ " categories.name AS category_name,"
				+ " statuses.name AS status_name,"
				+ " statuses.is_completed,"
				+ " categories.parent_id AS parent_category_id,"
				+ " case when favorites.id IS null then 0 else 1 end as is_favorited,"
				+ " concat(users.last_name, users.first_name) AS user_name,"
				+ " users.profile_url,"
				+ " ifnull(all_votes.vote_count,0) as vote_count,"
				+ " ifnull(up_votes.up_count, 0) as up_count,"				
				+ " ifnull(down_votes.down_count, 0) as down_count,"
				+ " (ifnull(up_votes.up_count,0) * settings.positive_point) + (ifnull(down_votes.down_count,0) * settings.negative_point) AS point ,"
				+ " ideas.is_temporary AS temporary,"
				+ " DATE_FORMAT(ideas.created_at,'%Y/%m/%d %H:%i') AS created_at,"
				+ " ideas.created_by as user_id"
				+ " FROM ideas"
				
				+ " inner JOIN users"
				+ "   ON users.id = ideas.created_by"
				
				+ " inner join "
				+ "   settings "
				+ " on settings.space_id = :spaceId "
				
				+ " LEFT JOIN "
				+ "   (SELECT count(id) AS vote_count, idea_id "
				+ "    FROM votes "
				+ "    GROUP BY idea_id) all_votes"
				+ "   ON all_votes.idea_id = ideas.id"
				
				+ " LEFT JOIN "
				+ "   (SELECT count(id) AS up_count, idea_id"
				+ "    FROM votes"
				+ "    WHERE is_agreed = true"
				+ "    GROUP BY idea_id) up_votes"
				+ "   ON up_votes.idea_id = ideas.id"
				
				+ " LEFT JOIN "
				+ "   (SELECT count(id) AS down_count, idea_id"
				+ "    FROM votes"
				+ "    WHERE is_agreed = false"
				+ "    GROUP BY idea_id) down_votes"
				+ "   ON down_votes.idea_id = ideas.id"
				
				+ " LEFT JOIN votes user_vote"
				+ "   ON user_vote.idea_id = ideas.id"
				+ "   AND user_vote.user_id = :userId"
				
				+ " LEFT JOIN categories"
				+ "   ON categories.id = ideas.category_id"
				
				+ " inner JOIN developments"
				+ " ON developments.idea_id = ideas.id"
				
				+ "  inner JOIN statuses"
				+ "   ON statuses.id = developments.status_id"
				
				+ " LEFT JOIN favorites"
				+ "   ON favorites.idea_id = ideas.id"
				+ "   AND favorites.created_by = :userId"
				
				+ " WHERE ideas.id = :ideaId AND ideas.space_id = :spaceId AND ideas.is_temporary = false ";
				
		Query findQuery =  entityManager.createNativeQuery(sql, IdeaDetailEntitiy.class);
		
		//idパラメーター設定
		findQuery.setParameter("userId", loginUser.getId());
		findQuery.setParameter("ideaId", ideaId);
		findQuery.setParameter("spaceId", loginUser.getSpaceId());
		
		IdeaDetailEntitiy detail = null;
		
		try {
			detail = (IdeaDetailEntitiy) findQuery.getSingleResult();	
		} catch(NoResultException ex) {
			
		}
		
		return Optional.ofNullable(detail);
	}
	
	@Override
	public Page<IdeaDetailCommentEntity> findComments(IdeaDetailForm form, Pageable pageable, int count) {
		
		String sql = " SELECT "
				+ " comments.id,"
				+ " comments.contents,"
				+ " concat(users.last_name, users.first_name) AS user_name,"
				+ " users.profile_url,"
				+ " DATE_FORMAT(comments.created_at,'%Y/%m/%d %H:%i') AS created_at,"
				+ " comments.created_by as user_id"
				+ " FROM comments"
				+ " inner JOIN users"
				+ " ON users.id = comments.created_by"
				+ " WHERE comments.idea_id = :ideaId";
		
		String orderBy = " ORDER BY comments.updated_at ";

		if(form.getCommentSort() == 1) {
			orderBy += " DESC ";
		}
		
		Query findQuery =  entityManager.createNativeQuery(sql + orderBy, IdeaDetailCommentEntity.class);
		
		findQuery.setParameter("ideaId", form.getId());

		@SuppressWarnings("unchecked")
		List<IdeaDetailCommentEntity> comment = findQuery
				.setFirstResult(pageable.getPageNumber()*pageable.getPageSize())
				.setMaxResults(pageable.getPageSize())
				.getResultList();
		
		return new PageImpl<IdeaDetailCommentEntity>(comment, pageable, count);
	}
	
	
	@Override
	public IdeaDetailCommentEntity findAdminComment(Long ideaId) {
		
		String sql = " SELECT "
				+ " comments.id,"
				+ " comments.contents,"
				+ " concat(users.last_name, users.first_name) AS user_name,"
				+ " users.profile_url,"
				+ " DATE_FORMAT(comments.created_at,'%Y/%m/%d %H:%i') AS created_at,"
				+ " comments.created_by as user_id"
				+ " FROM comments"
				+ " inner JOIN users"
				+ " ON users.id = comments.created_by "
				+ " WHERE comments.idea_id = :ideaId "
				+ " AND users.is_admin = true";
		
		String orderBy = " ORDER BY comments.updated_at DESC LIMIT 1";
		
		Query findQuery =  entityManager.createNativeQuery(sql + orderBy, IdeaDetailCommentEntity.class);
		
		findQuery.setParameter("ideaId", ideaId);
		
		IdeaDetailCommentEntity adminComment = new IdeaDetailCommentEntity();
		
		try {
			adminComment = (IdeaDetailCommentEntity) findQuery.getSingleResult();
		}catch(NoResultException ex) {
		}
		
		return adminComment;
	}
	
}
