package com.gnome.page.idea.ideaDetail;

import javax.persistence.Entity;

import javax.persistence.Id;

@Entity
public class IdeaDetailCommentEntity {
	
	@Id
	//コメントid
	private Long id;
	
	//内容
	private String contents;
	
	//コメント作成者
	private String userName;
	
	// プロフィール画像
	private String profileUrl;
	
	//コメント作成者Id
	private Long userId;
	
	//コメント作成日
	private String createdAt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
}
