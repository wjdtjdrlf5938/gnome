package com.gnome.page.idea.ideaDetail;

import java.util.List;

import com.gnome.base.common.BaseForm;

public class IdeaDetailForm extends BaseForm{
	
	// id
	private String id;
	
	// アイデア詳細
	private IdeaDetailEntitiy entity;
	
	//コメントリスト
	private List<IdeaDetailCommentEntity> commentList;
	
	//コメントソート条件
	private Long commentSort = 0L;
	
	//最新管理者コメント
	private IdeaDetailCommentEntity adminComment;
	
	//コメントリスト数
	private int commentCnt;
	
	// マージ数
	private  int mergedCnt;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public IdeaDetailEntitiy getEntity() {
		return entity;
	}

	public void setEntity(IdeaDetailEntitiy entity) {
		this.entity = entity;
	}

	public List<IdeaDetailCommentEntity> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<IdeaDetailCommentEntity> commentList) {
		this.commentList = commentList;
	}

	public Long getCommentSort() {
		return commentSort;
	}

	public void setCommentSort(Long commentSort) {
		this.commentSort = commentSort;
	}

	public IdeaDetailCommentEntity getAdminComment() {
		return adminComment;
	}

	public void setAdminComment(IdeaDetailCommentEntity adminComment) {
		this.adminComment = adminComment;
	}

	public int getCommentCnt() {
		return commentCnt;
	}

	public void setCommentCnt(int commentCnt) {
		this.commentCnt = commentCnt;
	}

	public int getMergedCnt() {
		return mergedCnt;
	}

	public void setMergedCnt(int mergedCnt) {
		this.mergedCnt = mergedCnt;
	}
}
