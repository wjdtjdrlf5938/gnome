package com.gnome.page.idea.ideaList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Repository;

import com.gnome.common.util.StringUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

@Repository
public class IdeaListRepositoryImpl implements IdeaListRepositoryCustom{
	
	@Autowired
	private EntityManager entityManager;
	
	@Override
	public Page<IdeaListEntity> findIdea(IdeaListForm form, Long userId, Long spaceId, Pageable pageable) {
		
		String countSql = " SELECT count(distinct ideas.id) FROM ideas";
				
		String sql = "SELECT"
				+ " ideas.id,"
				+ " ideas.subject,"
				+ " ideas.contents,"
				+ " statuses.name AS status_name,"
				+ " statuses.is_completed,"
				+ " case when favorites.id IS null then 0 else 1 end as is_favorited,"
				+ "  (up_votes.up_count * settings.positive_point) + (ifnull(down_votes.down_count,0) * settings.negative_point) AS point ,"
				+ " user_vote.is_agreed AS agreed,"
				+ " ifnull(comment.comments_count,0) AS count_comments,"
				+ " concat(users.last_name, users.first_name) AS user_name, "
				+ " DATE_FORMAT(ideas.created_at,'%Y/%m/%d %H:%i') AS created_at,"
				+ " ideas.created_by AS user_id,"
				+ " users.profile_url "
				+ " FROM ideas";
				
		String joinSql = ""
				+ " LEFT JOIN "
				+ "   (SELECT count(votes.id) AS up_count, votes.idea_id"
				+ "    FROM votes"
				+ "    WHERE votes.is_agreed = true"
				+ "    GROUP BY votes.idea_id) up_votes"
				+ "   ON up_votes.idea_id = ideas.id"
				
				+ " LEFT JOIN "
				+ "   (SELECT count(votes.id) AS down_count,  votes.idea_id "
				+ "    FROM votes "
				+ "    WHERE votes.is_agreed = false"
				+ "    GROUP BY votes.idea_id) down_votes"
				+ "   ON down_votes.idea_id = ideas.id"
				
				+ " INNER JOIN "
				+ "   settings "
				+ " on settings.space_id = :spaceId "
				
				+ " LEFT JOIN votes user_vote"
				+ "   ON user_vote.idea_id = ideas.id"
				+ "   AND user_vote.user_id = :userId"
				
				+ " INNER JOIN users"
				+ "   ON users.id = ideas.created_by"
				
				+ " LEFT JOIN " 
				+ "  (SELECT count(comments.id) AS comments_count, comments.idea_id "
				+ "   FROM comments "
				+ "   GROUP BY comments.idea_id) comment"
				+ "  ON comment.idea_id = ideas.id"
				
				+ " INNER JOIN categories"
				+ "   ON categories.id = ideas.category_id"
				
				+ " INNER JOIN developments "
				+ "   ON developments.idea_id = ideas.id"
				+ " INNER join statuses"
				+ "   ON statuses.id = developments.status_id"
				
				+ " LEFT JOIN favorites"
				+ "   ON favorites.idea_id = ideas.id"
				+ "   AND favorites.created_by = :userId";
		
		String conditionSql = " WHERE ideas.is_temporary = false AND ideas.merged_id IS null AND ideas.space_id = :spaceId ";
		
		Map<String, Object> bindParameters = new HashMap<String, Object>();
		
		if(!StringUtil.isEmpty(form.getParentCategoryId())) {
			conditionSql += " AND categories.parent_id = :parentCategory ";
			bindParameters.put("parentCategory", form.getParentCategoryId());
		}
		
		if(!StringUtil.isEmpty(form.getCategoryId())) {
			conditionSql += " AND ideas.category_id = :categoryId ";
			bindParameters.put("categoryId", form.getCategoryId());
		};
		
		if(!StringUtil.isEmpty(form.getStatusId()) && !"0".equals(form.getStatusId())) {
			conditionSql += " AND statuses.id = :status ";
			bindParameters.put("status", form.getStatusId());
		}
		
		bindParameters.put("userId", userId);
		bindParameters.put("spaceId", spaceId);
		
		String orderBy = " ORDER BY point DESC";
		
		if("1".equals(form.getSort())) {
			orderBy = "ORDER BY ideas.created_at DESC";
		}
		
		Query findQuery =  entityManager.createNativeQuery(sql + joinSql + conditionSql + orderBy, IdeaListEntity.class);
		Query countQuery = entityManager.createNativeQuery(countSql+ joinSql + conditionSql);
				
		for (Map.Entry<String, Object> param : bindParameters.entrySet()) {
			findQuery.setParameter(param.getKey(), param.getValue());
			countQuery.setParameter(param.getKey(), param.getValue());
		}
		
		int total = ((Number) countQuery.getSingleResult()).intValue();
		
		@SuppressWarnings("unchecked")
		List<IdeaListEntity> ideas = findQuery
				.setFirstResult(pageable.getPageNumber()*pageable.getPageSize())
				.setMaxResults(pageable.getPageSize())
				.getResultList();
		
		return new PageImpl<IdeaListEntity>(ideas, pageable, total);
	}

}
