package com.gnome.page.idea.ideaList;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IdeaListRepositoryCustom {
	
	public Page<IdeaListEntity> findIdea(IdeaListForm form, Long userId, Long spaceId, Pageable pageable);
	
}
