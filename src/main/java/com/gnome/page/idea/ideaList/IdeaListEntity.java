package com.gnome.page.idea.ideaList;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class IdeaListEntity {
	
	@Id
	//id
	private Long id;

	//タイトル
	private String subject;
	
	//アイデア内容
	private String contents;
	
	//ステータス名
	private String statusName;
		
	// ステータス完了フラグ
	private boolean isCompleted;
	
	//お気に入りフラグ
	private boolean isFavorited;
	
	// ポイント
	private Long point;
	
	//投票(賛成・反対)	
	private Long agreed;
	
	//コメント数
	private Long countComments;
	
	//作成者
	private String userName;
	
	//作成日
	private String createdAt;
		
	//作成者Id
	private Long userId;

	// プロフィール画像
	private String profileUrl;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContents() {
		
		String contentTag = contents.replaceAll("\\<.*?>","");
		return contentTag;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public boolean isCompleted() {
		return isCompleted;
	}

	public void setCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	public boolean isFavorited() {
		return isFavorited;
	}

	public void setFavorited(boolean isFavorited) {
		this.isFavorited = isFavorited;
	}

	public Long getPoint() {
		return point;
	}

	public void setPoint(Long point) {
		this.point = point;
	}

	public Long getAgreed() {
		return agreed;
	}

	public void setAgreed(Long agreed) {
		this.agreed = agreed;
	}

	public Long getCountComments() {
		return countComments;
	}

	public void setCountComments(Long countComments) {
		this.countComments = countComments;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}
}
