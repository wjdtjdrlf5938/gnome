package com.gnome.page.idea.ideaList;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gnome.auth.LoginUser;
import com.gnome.base.entity.User;

@Service
public class IdeaListService {
	
	@Autowired
	private IdeaListRepositoryCustom customRepo;
	
	
	@Transactional(rollbackFor=Exception.class)
	public void search(IdeaListForm form) {
		
		//ログインしたユーザー情報
		User loginUser = ((LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getLoginUser();
		
		//ページネーション用ページ情報
		Pageable pageable = PageRequest.of(form.getPageOfInt(), form.getRecordsOfInt());
		Page<IdeaListEntity> idealist = customRepo.findIdea(form, loginUser.getId(), loginUser.getSpaceId(), pageable);
		
		form.setFirst(idealist.isFirst());
		form.setLast(idealist.isLast());
		form.setNumber(idealist.getNumber());
		form.setTotalPages(idealist.getTotalPages());
		
		//検索結果
		form.setIdeaList(idealist.getContent());
	}
}
