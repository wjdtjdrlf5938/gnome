package com.gnome.page.idea.ideaList;

import java.util.List;

import com.gnome.base.common.BaseForm;

public class IdeaListForm extends BaseForm{
	
	//ID
	private String id;
	
	private String parentCategoryId;
	
	private String categoryId;
	
	private String statusId;
	
	private String sort = "0";
	
	//検索結果
	private List<IdeaListEntity> ideaList;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParentCategoryId() {
		return parentCategoryId;
	}

	public void setParentCategoryId(String parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public List<IdeaListEntity> getIdeaList() {
		return ideaList;
	}

	public void setIdeaList(List<IdeaListEntity> ideaList) {
		this.ideaList = ideaList;
	}
}
