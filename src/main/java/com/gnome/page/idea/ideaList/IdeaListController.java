package com.gnome.page.idea.ideaList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes({ "ideaListForm" })
public class IdeaListController {
	
	@Autowired
	private IdeaListService service;
	
	/**
	 * アイデア検索
	 * @param form
	 * @return
	 */
	@RequestMapping(value = "/ideas")
    public String index(IdeaListForm form) {
		
		service.search(form);
		
		return "idea/ideaList";
	}

}
