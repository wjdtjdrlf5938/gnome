package com.gnome.page.idea.vote;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface VoteRepositoryCustom {
	
	public Page<VoteEntitiy> findByIdeaId(Long ideaId, boolean isAgreed, Pageable pageable);

}
