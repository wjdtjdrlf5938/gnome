package com.gnome.page.idea.vote;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class VoteRepositoyiImpl implements VoteRepositoryCustom {
	
	@Autowired
	private EntityManager entityManager;
	
	@Override
	public Page<VoteEntitiy> findByIdeaId(Long ideaId, boolean isAgreed, Pageable pageable) {
		
		String countSql = "select count(1) ";
		
		String sql = " SELECT "
				+ " votes.id,"
				+ " votes.user_id,"
				+ " concat(users.last_name, users.first_name) AS user_name,"
				+ " users.profile_url,"
				+ " votes.is_agreed,"
				+ " votes.version,"
				+ " votes.created_at,"
				+ " votes.created_by,"
				+ " votes.updated_at,"
				+ " votes.updated_by";
		
		String conditionSql = ""
				+ " FROM votes"
				+ " inner JOIN users"
				+ "   ON users.id = votes.user_id"
				+ " WHERE votes.idea_id = :ideaId"
				+ " and votes.is_agreed = :isAgreed ";
		
		String orderBy =" order by votes.created_by ";
		
		Query findQuery =  entityManager.createNativeQuery(sql + conditionSql + orderBy, VoteEntitiy.class);
		Query countQuery = entityManager.createNativeQuery(countSql+ conditionSql);
		
		//パラメータ設定
		findQuery.setParameter("ideaId", ideaId);
		findQuery.setParameter("isAgreed", isAgreed);
		countQuery.setParameter("ideaId", ideaId);
		countQuery.setParameter("isAgreed", isAgreed);
		
		int total = ((Number) countQuery.getSingleResult()).intValue();
		
		@SuppressWarnings("unchecked")
		List<VoteEntitiy> votes = findQuery
				.setFirstResult(pageable.getPageNumber()*pageable.getPageSize())
				.setMaxResults(pageable.getPageSize())
				.getResultList();
		
		return new PageImpl<VoteEntitiy>(votes, pageable, total);
	}
	
}
