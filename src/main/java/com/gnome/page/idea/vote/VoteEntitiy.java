package com.gnome.page.idea.vote;

import javax.persistence.Entity;

import com.gnome.base.common.BaseEntity;

@Entity
public class VoteEntitiy extends BaseEntity{
	
	private static final long serialVersionUID = 1L;

	//ユーザid
	public Long userId;
	
	//ユーザ名
	public String userName;
	
	//投票(賛成・反対)
	public boolean isAgreed;
	
	// プロフィール
	public String profileUrl;
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public boolean isAgreed() {
		return isAgreed;
	}

	public void setAgreed(boolean isAgreed) {
		this.isAgreed = isAgreed;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}
}
