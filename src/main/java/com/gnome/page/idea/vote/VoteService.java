package com.gnome.page.idea.vote;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gnome.auth.LoginUser;
import com.gnome.base.entity.Vote;
import com.gnome.base.entity.User;
import com.gnome.base.repository.VoteRepository;
import com.gnome.common.util.DecimalUtil;
import com.gnome.common.util.StringUtil;
import com.gnome.page.idea.ideaDetail.IdeaDetailEntitiy;
import com.gnome.page.idea.ideaDetail.IdeaDetailRepositoryCustom;

@Service
public class VoteService {
	
	@Autowired
	private VoteRepository repo;
	
	@Autowired
	private IdeaDetailRepositoryCustom detailRepo;
	
	@Autowired
	private VoteRepositoryCustom customRepo;
	
	/**
	 * 検索処理
	 * 
	 * @param form
	 */
	public void index(VoteForm form) {
		
		Pageable pageable = PageRequest.of(StringUtil.toInteger(form.getPage()), 30);
		
		Page<VoteEntitiy> votes = customRepo.findByIdeaId(StringUtil.toLong(form.getIdeaId()),form.isAgreed(), pageable);
		
		// 検索結果
		form.setTotal(DecimalUtil.toString(votes.getTotalElements()));
		form.setResultList(votes.getContent());
		form.setLast(votes.isLast());
	}
	
	/**
	 * 投票処理
	 * @param form
	 */
	@Transactional(rollbackFor=Exception.class)
	public void vote(VoteForm form) {
		
		//ログインしたユーザー情報
		User loginUser = ((LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getLoginUser();
		
		Vote votes = new Vote();
		
		votes.setIdeaId(StringUtil.toLong(form.getIdeaId()));
		votes.setUserId(loginUser.getId());
		
		////投票(賛成・反対)設定
		votes.setAgreed(form.isAgreed());
		
		repo.saveAndFlush(votes);
		
		Optional<IdeaDetailEntitiy> optional = detailRepo.findByIdeaId(StringUtil.toLong(form.getIdeaId()), loginUser);
		form.setPoint(String.format("%,d", optional.get().getPoint()));
	}
	
	/**
	 * 削除処理
	 * @param form
	 */
	@Transactional(rollbackFor=Exception.class)
	public void delete(VoteForm form) {
		
		//ログインしたユーザー情報
		User loginUser = ((LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getLoginUser();
		
		repo.deleteByIdeaIdAndCreatedBy(StringUtil.toLong(form.getIdeaId()), loginUser.getId());
	}
}
