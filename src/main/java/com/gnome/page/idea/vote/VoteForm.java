package com.gnome.page.idea.vote;

import java.util.List;

public class VoteForm{
	
	//アイデアid
	private String ideaId;
	
	//投票(賛成・反対)
	private boolean isAgreed;
	
	// ポイント数
	private String point;
	
	// ページ数
	private String page;
	
	// 最終
	private boolean last;
	
	// 検索結果
	private List<VoteEntitiy> resultList;
	
	// トータル件数
	private String total;
	
	public String getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(String ideaId) {
		this.ideaId = ideaId;
	}

	public boolean isAgreed() {
		return isAgreed;
	}

	public void setAgreed(boolean isAgreed) {
		this.isAgreed = isAgreed;
	}

	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public boolean isLast() {
		return last;
	}

	public void setLast(boolean last) {
		this.last = last;
	}

	public List<VoteEntitiy> getResultList() {
		return resultList;
	}

	public void setResultList(List<VoteEntitiy> resultList) {
		this.resultList = resultList;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}
}
