package com.gnome.page.idea.vote;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

@RestController
public class VoteController {
	
	@Autowired
	private VoteService service;
	
	@RequestMapping(path="/vote", method = RequestMethod.GET)
	@ResponseBody
	public String index(VoteForm form) {
		
		Gson gson = new Gson();
		
		// 検索処理
		service.index(form);
		
		return gson.toJson(form);
	}
	
	/**
	 * 投票処理
	 * @param form
	 * @return
	 */
	@RequestMapping(path="/vote", method = RequestMethod.POST)
	@ResponseBody
	public String vote(VoteForm form) {
		
		Gson gson = new Gson();
		
		//投票処理
		service.vote(form);
		
		return gson.toJson(form);
	}
	
	/**
	 * 投票取り消す処理
	 * @param form
	 * @return
	 */
	@RequestMapping(path="/vote/delete", method = RequestMethod.DELETE)
	@ResponseBody
	public String delete(VoteForm form) {
		
		Gson gson = new Gson();
		
		//投票削除処理
		service.delete(form);
		
		return gson.toJson(form);
	}

}
