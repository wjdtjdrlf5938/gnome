package com.gnome.page.idea.favorite;

public class FavoriteForm {
	
	private String ideaId;
	
	private boolean favoriteFlg;

	public String getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(String ideaId) {
		this.ideaId = ideaId;
	}

	public boolean isFavoriteFlg() {
		return favoriteFlg;
	}

	public void setFavoriteFlg(boolean favoriteFlg) {
		this.favoriteFlg = favoriteFlg;
	}
}
