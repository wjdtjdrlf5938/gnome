package com.gnome.page.idea.favorite;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gnome.auth.LoginUser;
import com.gnome.base.entity.Favorite;
import com.gnome.base.entity.User;
import com.gnome.base.repository.FavoriteRepository;
import com.gnome.common.util.StringUtil;

@Service
public class FavoriteService {
	
	@Autowired
	private FavoriteRepository repo;
	
	@Transactional(rollbackFor=Exception.class)
	public void update(FavoriteForm form) {
		
		// ログインしたユーザー情報
		User loginUser = ((LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getLoginUser();
		// スペースID
		Long spaceId = loginUser.getSpaceId();
		// アイデアID
		Long ideaId = StringUtil.toLong(form.getIdeaId());
		
		Optional<Favorite> optional = repo.findBySpaceIdAndIdeaIdAndCreatedBy(spaceId, ideaId, loginUser.getId());
		
		Favorite entity = new Favorite();
		//存在した場合、削除
		if(optional.isPresent()) {
			entity = optional.get();
			form.setFavoriteFlg(false);
			repo.delete(entity);
			return;
		}
		//存在しない場合、作成
		entity.setSpaceId(spaceId);
		entity.setIdeaId(ideaId);
		repo.saveAndFlush(entity);
	}
}
