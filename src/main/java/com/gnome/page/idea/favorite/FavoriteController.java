package com.gnome.page.idea.favorite;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

@RestController
public class FavoriteController {
	
	@Autowired
	private FavoriteService service;
	
	/**
	 * お気に入り更新
	 * @param form
	 * @return
	 */
	@RequestMapping(path="/favorite", method = RequestMethod.POST)
	@ResponseBody
	public String update(FavoriteForm form) {
		
		Gson gson = new Gson();
		
		//投票処理
		service.update(form);
		
		return gson.toJson(form);
	}
}
