package com.gnome.page.common.initPwChange;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gnome.auth.LoginUser;
import com.gnome.base.entity.User;
import com.gnome.base.repository.UserRepository;
import com.gnome.common.consts.MessageIdConsts;
import com.gnome.common.exception.BusinessException;

@Service
public class InitPwChangeService {

	@Autowired
	UserRepository userRepo;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	MessageSource msgSource;
	
	/**
	 * 更新処理
	 * 
	 * @param form
	 * @throws BusinessException
	 */
	@Transactional(rollbackFor=Exception.class)
	public void update(InitPwChangeForm form) throws BusinessException {
		
		LoginUser loginUser =
                (LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
		Optional<User> checkUser = userRepo.findById(loginUser.getLoginUser().getId());
				
		if (!checkUser.isPresent()) {
			throw new BusinessException(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000001, null, null));
		}

		User user = checkUser.get();

		if (passwordEncoder.matches(form.getPassword(), user.getTempPassword())) {
			throw new BusinessException(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000010, new String[] {"前回と同じパスワード"}, null));
		}

		user.setPassword(passwordEncoder.encode(form.getPassword()));
		user.setTempPassword(null);
		
		userRepo.saveAndFlush(user);
		
		loginUser.setTempPassword(false);
	}
}
