package com.gnome.page.common.initPwChange;

import com.gnome.base.common.BaseForm;

public class InitPwChangeForm extends BaseForm{

	// パスワード
	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
