package com.gnome.page.common.initPwChange;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gnome.common.consts.CodeConsts;
import com.gnome.common.consts.MessageIdConsts;
import com.gnome.common.exception.BusinessException;

@Controller
public class InitPwChangeController {

	@Autowired
	private InitPwChangeService service;
	
	@Autowired
	private MessageSource msgSource;
	
	@RequestMapping(value = "/init-pw-change")
    public String index(InitPwChangeForm form) {
        return "common/initPwChange";
    }
	
	@RequestMapping(value = "/init-pw-change/update")
    public String update(InitPwChangeForm form, BindingResult errors, RedirectAttributes attributes) {
		
		try {
			service.update(form);
			
			attributes.addFlashAttribute(CodeConsts.MESSAGEKBN_SUCCESS, msgSource.getMessage(MessageIdConsts.MESSAGE_ID_I000001, new String[] {"パスワードの変更"}, null));
			
		} catch (BusinessException e) {
			e.printStackTrace();
			for(String obj : e.getErrors()){
				errors.reject("", obj);
			}
			return "common/initPwChange";
		}
		
		return "redirect:/ideas";
    }
}
