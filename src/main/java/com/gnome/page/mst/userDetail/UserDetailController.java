package com.gnome.page.mst.userDetail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.gnome.common.exception.BusinessException;

@Controller
public class UserDetailController {

	@Autowired
	private UserDetailService service;
		
	/**
	 * 
	 * @param id 
	 * @param form
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/users/{id}")
    public String index(@PathVariable("id") String id, UserDetailForm form, Model model) {
		
		try {
			
			form.setId(id);
			
			service.index(form);
			
			model.addAttribute("userName", form.getLastName()+form.getFirstName());
			
		} catch (BusinessException e) {
			e.printStackTrace();
			return "error";
		}
		
        return "mst/userDetail";
    }
}
