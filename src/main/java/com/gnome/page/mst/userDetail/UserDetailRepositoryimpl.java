package com.gnome.page.mst.userDetail;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class UserDetailRepositoryimpl implements UserDetailRepositoryCustom{
	
	@Autowired
	private EntityManager entityManager;
	
	@Override
	public Page<UserDetailIdeaEntity> findUserIdea(Long id, Long spaceId, Pageable pageable) {
		
		String countSql = " SELECT count(*) FROM ideas ";
		
		String sql = " SELECT "
				+ " ideas.id,"
				+ " ideas.subject,"
				+ " statuses.name AS status_name,"
				+ " (ifnull(up_votes.up_count,0) * settings.positive_point) + (ifnull(down_votes.down_count,0) * settings.negative_point) AS point ,"
				+ " DATE_FORMAT(ideas.created_at,'%Y/%m/%d %H:%i') AS created_at "
				+ " FROM ideas ";
				
		String joinSql =	""
				+ " LEFT JOIN "
				+ "   (SELECT count(votes.id) AS up_count, votes.idea_id"
				+ "    FROM votes"
				+ "    WHERE votes.is_agreed = true"
				+ "    GROUP BY votes.idea_id) up_votes"
				+ "   ON up_votes.idea_id = ideas.id"
				
				+ " LEFT JOIN "
				+ "   (SELECT count(distinct votes.id) AS down_count, "
				+ "           votes.idea_id "
				+ "    FROM votes"
				+ "    WHERE votes.is_agreed = false"
				+ "    GROUP BY votes.idea_id) down_votes"
				+ "   ON down_votes.idea_id = ideas.id"
				
				+ " inner join "
				+ "   settings "
				+ " on settings.space_id = :spaceId "
				
				+ " inner JOIN developments "
				+ "      ON ideas.id = developments.idea_id "
				
				+ "  inner JOIN statuses"
				+ "      ON statuses.id = developments.status_id ";
		
		
		String conditionSql = " WHERE ideas.created_by = :id AND ideas.is_temporary = false ";
		String orderBy = "ORDER BY ideas.created_at desc ";
		
		Query findQuery =  entityManager.createNativeQuery(sql + joinSql + conditionSql + orderBy, UserDetailIdeaEntity.class);
		Query countQuery = entityManager.createNativeQuery(countSql + joinSql + conditionSql);
		
		findQuery.setParameter("id", id);
		countQuery.setParameter("id", id);
		
		findQuery.setParameter("spaceId", spaceId);
		countQuery.setParameter("spaceId", spaceId);
		
		int total = ((Number) countQuery.getSingleResult()).intValue();
		
		@SuppressWarnings("unchecked")
		List<UserDetailIdeaEntity> ideas = findQuery
				.setFirstResult(pageable.getPageNumber()*pageable.getPageSize())
				.setMaxResults(pageable.getPageSize())
				.getResultList();
		
		return new PageImpl<UserDetailIdeaEntity>(ideas, pageable, total);
	}
	
	
	@Override
	public List<UserDetailTempIdeaEntity> findUserTempIdea(Long id) {
		
		String sql = " SELECT "
				+ " ideas.id,"
				+ " ideas.subject,"
				+ " ideas.contents,"
				+ " ideas.category_id,"
				+ " concat(parents_category.name, ' > ', sub_category.name) AS category_name,"
				+ " ideas.updated_at"
				+ " FROM ideas "
				+ " LEFT JOIN categories sub_category ON sub_category.id = ideas.category_id"
				+ " LEFT JOIN categories parents_category ON parents_category.id = sub_category.parent_id"
				+ " WHERE ideas.updated_by = :id AND ideas.is_temporary = true ";
		
		Query findQuery =  entityManager.createNativeQuery(sql, UserDetailTempIdeaEntity.class);
		
		findQuery.setParameter("id", id);
		
		@SuppressWarnings("unchecked")
		List<UserDetailTempIdeaEntity> tempIdeas = findQuery.getResultList();
		
		return tempIdeas;
	}

	@Override
	public Long getPoints(Long id, Long spaceId) {
		
		String sql = " SELECT "
				+ " sum((ifnull(up_votes.up_count,0) * settings.positive_point) + (ifnull(down_votes.down_count,0) * settings.negative_point)) AS point "
				+ " FROM ideas "
				
				+ " LEFT JOIN "
				+ "   (SELECT count(votes.id) AS up_count, votes.idea_id"
				+ "    FROM votes"
				+ "    WHERE votes.is_agreed = true"
				+ "    GROUP BY votes.idea_id) up_votes"
				+ "   ON up_votes.idea_id = ideas.id"
				
				+ " LEFT JOIN "
				+ "   (SELECT count(distinct votes.id) AS down_count, "
				+ "           votes.idea_id "
				+ "    FROM votes"
				+ "    WHERE votes.is_agreed = false"
				+ "    GROUP BY votes.idea_id) down_votes"
				+ "   ON down_votes.idea_id = ideas.id"
				
				+ " inner join "
				+ "   settings "
				+ " on settings.space_id = :spaceId "
				+ " WHERE ideas.created_by = :id AND ideas.is_temporary = false ";
		
		Query findQuery =  entityManager.createNativeQuery(sql);
		
		findQuery.setParameter("id", id);
		findQuery.setParameter("spaceId", spaceId);
		
		int points = 0;
		
		try {
			//points =  (int) findQuery.getSingleResult();
		}catch(NoResultException ex) {
		}
		
		return Long.valueOf(points);
	}
}
