package com.gnome.page.mst.userDetail;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserDetailRepositoryCustom {
	
	public Page<UserDetailIdeaEntity> findUserIdea(Long id, Long spaceId, Pageable pageable);
	
	public List<UserDetailTempIdeaEntity> findUserTempIdea(Long id);
	
	public Long getPoints(Long id, Long spaceId);
}
