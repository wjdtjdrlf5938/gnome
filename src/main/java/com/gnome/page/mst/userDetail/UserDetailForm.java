package com.gnome.page.mst.userDetail;

import java.util.List;

import com.gnome.base.common.BaseForm;
import com.gnome.base.entity.LoginInfo;

public class UserDetailForm extends BaseForm{

	// Id
	private String id;
	
	// 姓
	private String lastName;
	
	// 名
	private String firstName;
	
	// メールアドレス
	private String mail;
	
	// プロフィール画像
	private String profileUrl;
	
	// ログイン履歴
	private List<LoginInfo> loginInfoList;
	
	//トータル点数
	private Long totalPoint;
	
	//登録したアイデア
	private List<UserDetailIdeaEntity> ideaList;
	
	//仮登録したアイデア
	private List<UserDetailTempIdeaEntity> tempIdeaList;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}

	public List<LoginInfo> getLoginInfoList() {
		return loginInfoList;
	}

	public void setLoginInfoList(List<LoginInfo> loginInfoList) {
		this.loginInfoList = loginInfoList;
	}

	public Long getTotalPoint() {
		return totalPoint;
	}

	public void setTotalPoint(Long totalPoint) {
		this.totalPoint = totalPoint;
	}

	public List<UserDetailIdeaEntity> getIdeaList() {
		return ideaList;
	}

	public void setIdeaList(List<UserDetailIdeaEntity> ideaList) {
		this.ideaList = ideaList;
	}

	public List<UserDetailTempIdeaEntity> getTempIdeaList() {
		return tempIdeaList;
	}

	public void setTempIdeaList(List<UserDetailTempIdeaEntity> tempIdeaList) {
		this.tempIdeaList = tempIdeaList;
	}
}
