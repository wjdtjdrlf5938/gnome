package com.gnome.page.mst.userDetail;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gnome.auth.LoginUser;
import com.gnome.base.entity.LoginInfo;
import com.gnome.base.entity.User;
import com.gnome.base.repository.LoginInfoRepository;
import com.gnome.base.repository.UserRepository;
import com.gnome.common.consts.MessageIdConsts;
import com.gnome.common.exception.BusinessException;
import com.gnome.common.util.StringUtil;

@Service
public class UserDetailService {

	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private LoginInfoRepository loginInfoRepo;
	
	@Autowired
	private UserDetailRepositoryCustom detailIdeaRepository;
	
	@Autowired
	private MessageSource msgSource;
	
	/**
	 * 検索実行
	 * 
	 * @param form
	 * @throws BusinessException
	 */
	@Transactional(rollbackFor=Exception.class)
	public void index(UserDetailForm form) throws BusinessException {
		
		Long id = StringUtil.toLong(form.getId());
		
		// スペースID
		Long spaceId = ((LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getLoginUser().getSpaceId();
		
		// 検索実行
		Optional<User> userOptional = userRepo.findByIdAndSpaceId(id, spaceId);
		
		Page<LoginInfo> loginInfos = loginInfoRepo.findByUserIdOrderByLoginedAtDesc(id, PageRequest.of(0, 10));
		
		// 存在しない場合、削除された場合、エラー
		if (!userOptional.isPresent() || userOptional.get().isDeleted()) {
			throw new BusinessException(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000001, null, null));
		}
		
		User user = userOptional.get();
		form.setLastName(user.getLastName());
		form.setFirstName(user.getFirstName());
		form.setMail(user.getEmail());
		form.setProfileUrl(user.getProfileUrl());
		
		//ログイン履歴
		form.setLoginInfoList(loginInfos.getContent());
		
		//アイデアリスト
		Pageable pageable = PageRequest.of(form.getPageOfInt(), form.getRecordsOfInt());
		Page<UserDetailIdeaEntity> ideaList = detailIdeaRepository.findUserIdea(id, spaceId, pageable);
		form.setFirst(ideaList.isFirst());
		form.setLast(ideaList.isLast());
		form.setNumber(ideaList.getNumber());
		form.setTotalPages(ideaList.getTotalPages());
		form.setIdeaList(ideaList.getContent());
		
		// トータル点数
		Long points = detailIdeaRepository.getPoints(id, spaceId);
		form.setTotalPoint(points);
		
		//仮登録したアイデアリスト
		List<UserDetailTempIdeaEntity> tempIdeaList = detailIdeaRepository.findUserTempIdea(id);
		form.setTempIdeaList(tempIdeaList);
	}
}
