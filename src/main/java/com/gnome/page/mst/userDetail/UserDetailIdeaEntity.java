package com.gnome.page.mst.userDetail;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UserDetailIdeaEntity {
	
	@Id
	//id
	private Long id;

	//タイトル
	private String subject;
	
	//ステータス名
	private String statusName;
	
	// ポイント
	private Long point;
	
	//作成日
	private String createdAt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public Long getPoint() {
		return point;
	}

	public void setPoint(Long point) {
		this.point = point;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
}
