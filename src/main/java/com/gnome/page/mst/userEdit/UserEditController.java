package com.gnome.page.mst.userEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gnome.common.exception.BusinessException;
import com.google.gson.Gson;

@Controller
public class UserEditController {

	@Autowired
	private UserEditService service;
		
	/**
	 * 初期表示
	 * 
	 * @param form フォーム
	 * @return String Json
	 */
	@RequestMapping(path="/auth/users/create", method = RequestMethod.POST)
	@ResponseBody
	public String create(UserEditForm form, RedirectAttributes attributes) {
		
		Gson gson = new Gson();
		
		try {
			service.create(form);
		} catch (BusinessException e) {
			e.printStackTrace();
			form.setErrorMsg(e.getErrors().get(0));
		}
		
		return gson.toJson(form);
	}
	
	/**
	 * 検索処理
	 * 
	 * @param id
	 * @param form
	 * @return
	 */
	@RequestMapping(path="/users/edit", method = RequestMethod.GET)
	@ResponseBody
	public String edit(UserEditForm form) {
		
		Gson gson = new Gson();
		
		try {
			
			service.index(form);
			
		} catch (BusinessException e) {
			e.printStackTrace();
			form.setErrorMsg(e.getErrors().get(0));
		}
		
		return gson.toJson(form);
	}
	
	/**
	 * 更新処理
	 * 
	 * @param id
	 * @param form
	 * @return
	 */
	@RequestMapping(path="/users/update", method = RequestMethod.PUT)
	@ResponseBody
	public String update(UserEditForm form) {
		
		Gson gson = new Gson();
		
		try {
			
			service.update(form);
			
		} catch (BusinessException e) {
			e.printStackTrace();
			form.setErrorMsg(e.getErrors().get(0));
		}
		
		return gson.toJson(form);
	}
	
	/**
	 * パスワード更新
	 * @param id
	 * @param form
	 * @return
	 */
	@RequestMapping(path="/users/change", method = RequestMethod.PUT)
	@ResponseBody
	public String change(UserEditForm form) {
		
		Gson gson = new Gson();
		
		try {
			
			service.change(form);
			
		} catch (BusinessException e) {
			e.printStackTrace();
			form.setErrorMsg(e.getErrors().get(0));
		}
		
		return gson.toJson(form);
	}
}
