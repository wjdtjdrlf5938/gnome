package com.gnome.page.mst.userEdit;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gnome.auth.LoginUser;
import com.gnome.base.entity.User;
import com.gnome.base.repository.UserRepository;
import com.gnome.common.consts.MessageIdConsts;
import com.gnome.common.exception.BusinessException;
import com.gnome.common.util.StringUtil;

@Service
public class UserEditService {

	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private MessageSource msgSource;
	
	/**
	 * 検索実行
	 * 
	 * @param form
	 * @throws BusinessException
	 */
	@Transactional(rollbackFor=Exception.class)
	public void index(UserEditForm form) throws BusinessException {
		
		// スペースID
		Long spaceId = ((LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getLoginUser().getSpaceId();
		
		// 検索実行
		Optional<User> userOptional = userRepo.findByIdAndSpaceId(StringUtil.toLong(form.getId()), spaceId);
		
		// 存在しない場合、エラー
		if (!userOptional.isPresent()) {
			throw new BusinessException(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000001, null, null));
		}
		
		User user = userOptional.get();
		form.setLastName(user.getLastName());
		form.setFirstName(user.getFirstName());
		form.setMail(user.getEmail());
		form.setAdmin(user.isAdmin());
		form.setVersion(String.valueOf(user.getVersion()));
	}
	
	/**
	 * 登録処理
	 * 
	 * @param form
	 * @throws BusinessException
	 */
	@Transactional(rollbackFor=Exception.class)
	public void create(UserEditForm form) throws BusinessException {
		
		checkData(form);
		
		checkPw(form.getPw());
		
		User checkUser = userRepo.findByEmail(form.getMail());
		
		if (checkUser != null) {
			throw new BusinessException(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000002, new String[] {"メールアドレス"}, null));
		}
		
		User user = new User();
		
		// スペースID
		Long spaceId = ((LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getLoginUser().getSpaceId();
		
		user.setLastName(form.getLastName());
		user.setFirstName(form.getFirstName());
		user.setEmail(form.getMail());
		user.setSpaceId(spaceId);
		user.setAdmin(form.isAdmin());
		user.setProfileUrl("https://mdbootstrap.com/img/Photos/Avatars/img%20%2831%29.jpg");
		
		if (form.isTempPassword()) {
			user.setTempPassword(passwordEncoder.encode(form.getPw()));
		} else {
			user.setPassword(passwordEncoder.encode(form.getPw()));	
		}
		
		userRepo.saveAndFlush(user);
	}
	
	/**
	 * 更新処理
	 * 
	 * @param form
	 * @throws BusinessException
	 */
	@Transactional(rollbackFor=Exception.class)
	public void update(UserEditForm form) throws BusinessException {
		
		checkData(form);
		
		Long id = StringUtil.toLong(form.getId());
		
		User checkUser = userRepo.findByIdNotAndEmail(id, form.getMail());
		
		if (checkUser != null) {
			throw new BusinessException(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000002, new String[] {"メールアドレス"}, null));
		}
		
		Optional<User> userOptional = userRepo.findById(id);
		
		if (!userOptional.isPresent() || userOptional.get().getVersion() != Integer.parseInt(form.getVersion())) {
			throw new BusinessException(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000004, null, null));
		}
		
		User user = userOptional.get();
		
		user.setLastName(form.getLastName());
		user.setFirstName(form.getFirstName());
		user.setEmail(form.getMail());
		
		userRepo.saveAndFlush(user);
	}
	
	/**
	 * パスワード更新
	 * 
	 * @param form
	 * @throws BusinessException
	 */
	@Transactional(rollbackFor=Exception.class)
	public void change(UserEditForm form) throws BusinessException {
		
		checkPw(form.getPw());
		
		Long id = StringUtil.toLong(form.getId());
		
		Optional<User> userOptional = userRepo.findById(id);
		
		if (!userOptional.isPresent() || userOptional.get().getVersion() != Integer.parseInt(form.getVersion())) {
			throw new BusinessException(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000004, null, null));
		}
		
		User user = userOptional.get();
		
		if (form.isTempPassword()) {
			user.setPassword(null);
			user.setTempPassword(passwordEncoder.encode(form.getPw()));
		} else {
			user.setTempPassword(null);
			user.setPassword(passwordEncoder.encode(form.getPw()));	
		}
		
		userRepo.saveAndFlush(user);
	}
	
	/**
	 * 入力データをチェックする
	 * 
	 * @param form
	 * @throws NoSuchMessageException
	 * @throws BusinessException
	 */
	private void checkData(UserEditForm form) throws NoSuchMessageException, BusinessException {
		
		// 姓が入力されていない場合、エラー
		if (StringUtil.isEmpty(form.getLastName())) {
			throw new BusinessException(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000005, new String[] {"姓"}, null));
		}
		
		// 名が入力されていない場合、エラー
		if (StringUtil.isEmpty(form.getFirstName())) {
			throw new BusinessException(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000005, new String[] {"名"}, null));
		}
		
		// メールアドレスが入力されていない場合、エラー
		if (StringUtil.isEmpty(form.getMail())) {
			throw new BusinessException(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000005, new String[] {"メールアドレス"}, null));
		}
		
		// メール形式として正しくない場合、エラー
		if (!StringUtil.isMailAddr(form.getMail())) {
			throw new BusinessException(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000003, new String[] {"メールアドレス形式"}, null));
		}
	}
	
	/**
	 * パスワードの形式をチェックする
	 * 
	 * @param pw
	 * @throws NoSuchMessageException
	 * @throws BusinessException
	 */
	private void checkPw(String pw) throws NoSuchMessageException, BusinessException {
		
		if (StringUtil.isEmpty(pw)) {
			throw new BusinessException(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000005, new String[] {"パスワード"}, null));
		}
		
		Pattern p = Pattern.compile("^$|^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[!-/:-@\\[-`{-~])[!-~]*");
        Matcher m = p.matcher(pw);
        
        if (!m.matches()) {
        	throw new BusinessException(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000006, null, null));
        }
	}
}
