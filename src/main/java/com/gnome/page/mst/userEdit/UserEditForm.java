package com.gnome.page.mst.userEdit;

import com.gnome.base.common.BaseForm;

public class UserEditForm extends BaseForm{

	// Id
	private String id;
	
	// 姓
	private String lastName;
	
	// 名
	private String firstName;
	
	// メールアドレス
	private String mail;
	
	// パスワード
	private String pw;
	
	// 仮パスワードフラグ
	private boolean tempPassword;
	
	//管理者判断
	private boolean isAdmin;

	// version
	private String version;
	
	// エラーメッセージ
	private String errorMsg;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPw() {
		return pw;
	}

	public void setPw(String pw) {
		this.pw = pw;
	}

	public boolean isTempPassword() {
		return tempPassword;
	}

	public void setTempPassword(boolean tempPassword) {
		this.tempPassword = tempPassword;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
}
