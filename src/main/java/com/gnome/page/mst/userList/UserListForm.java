package com.gnome.page.mst.userList;

import java.util.List;

import com.gnome.base.common.BaseForm;

public class UserListForm extends BaseForm{

	// 名前：検索条件
	private String name;
	
	// 権限：検索条件
	private String auth;

	// ユーザー一覧：検索結果
	private List<UserListEntity> userList;
	
	// 削除対象
	private String[] delTarget;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public List<UserListEntity> getUserList() {
		return userList;
	}

	public void setUserList(List<UserListEntity> userList) {
		this.userList = userList;
	}

	public String[] getDelTarget() {
		return delTarget;
	}

	public void setDelTarget(String[] delTarget) {
		this.delTarget = delTarget;
	}
}
