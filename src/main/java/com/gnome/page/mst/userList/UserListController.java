package com.gnome.page.mst.userList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gnome.common.consts.CodeConsts;
import com.gnome.common.consts.MessageIdConsts;


@Controller
public class UserListController {

	@Autowired
	private UserListService service;
	
	@Autowired
	private MessageSource msgSource;
	
	/**
	 * 検索処理
	 * 
	 * @param form
	 * @return
	 */
	@RequestMapping(value = "/auth/users")
    public String index(UserListForm form, Model model) {
		service.search(form);
        return "mst/userList";
    }
	
	/**
	 * 削除処理
	 * 
	 * @param form
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/auth/users/delete")
    public String destroy(UserListForm form, RedirectAttributes attributes) {

		service.delete(form.getDelTarget());
		
		attributes.addFlashAttribute(CodeConsts.MESSAGEKBN_SUCCESS, msgSource.getMessage(MessageIdConsts.MESSAGE_ID_I000002, new String[] {"ユーザー"}, null));
		
		form.setPage("0");

		return "redirect:/auth/users";
    }
}
