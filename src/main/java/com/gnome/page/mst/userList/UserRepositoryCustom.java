package com.gnome.page.mst.userList;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserRepositoryCustom {

	public Page<UserListEntity> findUser(String name, String auth, Long spaceId, Pageable pageable);
}
