package com.gnome.page.mst.userList;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UserListEntity {

	// id
	@Id
	private Long id;
	
	// 姓
	private String lastName;
	
	// 名
	private String firstName;
	
	// メール
	private String email;
	
	// 最終ログイン日時
	private String lastLoginAt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLastLoginAt() {
		return lastLoginAt;
	}

	public void setLastLoginAt(String lastLoginAt) {
		this.lastLoginAt = lastLoginAt;
	}
}
