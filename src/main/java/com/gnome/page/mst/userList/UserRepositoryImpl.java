package com.gnome.page.mst.userList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.gnome.common.util.StringUtil;

@Repository
public class UserRepositoryImpl implements UserRepositoryCustom {

	@Autowired
	private EntityManager entityManager;

	@Override
	public Page<UserListEntity> findUser(String name, String auth, Long spaceId, Pageable pageable) {
		
		String countSql = " SELECT count(*) FROM users ";
		
		String sql = " SELECT users.id,"
				+ " users.last_name,"
				+ " users.first_name,"
				+ " users.email,"
				+ " DATE_FORMAT(max(logined_at),'%Y/%m/%d %H:%i') AS LAST_LOGIN_AT "
				+ " FROM users "
				+ " LEFT JOIN login_infos "
				+ " ON users.id = login_infos.user_id";
		
		String conditionSql = " WHERE users.space_id = :spaceId AND is_deleted = false";
		
		Map<String, Object> bindParameters = new HashMap<String, Object>();
		bindParameters.put("spaceId", spaceId);
		
		if (!StringUtil.isEmpty(name)) {
			conditionSql += " AND concat(users.last_name, users.first_name) LIKE :name ";
			bindParameters.put("name", "%" + name + "%");
		}
		
		if (!StringUtil.isEmpty(auth)) {
			conditionSql += " AND users.is_admin = :auth ";
			bindParameters.put("auth", auth);
		}
		
		String groupBy = " group by users.id, users.last_name, users.first_name, users.email ";
		
		Query findQuery =  entityManager.createNativeQuery(sql + conditionSql + groupBy, UserListEntity.class);
		Query countQuery = entityManager.createNativeQuery(countSql + conditionSql);
		
		for (Map.Entry<String, Object> param : bindParameters.entrySet()) {
			findQuery.setParameter(param.getKey(), param.getValue());
			countQuery.setParameter(param.getKey(), param.getValue());
		}
		
		int total = ((Number) countQuery.getSingleResult()).intValue();
		
		@SuppressWarnings("unchecked")
		List<UserListEntity> users = findQuery
							.setFirstResult(pageable.getPageNumber()*pageable.getPageSize())
							.setMaxResults(pageable.getPageSize())
							.getResultList();
		
		return new PageImpl<UserListEntity>(users, pageable, total);

	}
}
