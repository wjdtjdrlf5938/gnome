package com.gnome.page.mst.userList;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gnome.auth.LoginUser;
import com.gnome.base.entity.User;
import com.gnome.base.repository.UserRepository;
import com.gnome.common.util.StringUtil;

@Service
public class UserListService {

	@Autowired
	private UserRepositoryCustom repo;

	@Autowired
	private UserRepository userRepo;
	
	/**
	 * 検索処理
	 * 
	 * @param form
	 * @return ユーザー一覧
	 */
	@Transactional(rollbackFor=Exception.class)
	public void search(UserListForm form) {
		
		// スペースID
		Long spaceId = ((LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getLoginUser().getSpaceId();
		
		Pageable pageable = PageRequest.of(form.getPageOfInt(), form.getRecordsOfInt());
		
		Page<UserListEntity> users = repo.findUser(form.getName(), form.getAuth(), spaceId, pageable);
		
		form.setFirst(users.isFirst());
		form.setTotalPages(users.getTotalPages());
		form.setNumber(users.getNumber());
		form.setLast(users.isLast());
		form.setUserList(users.getContent());
		
	}
	
	/**
	 * 削除処理
	 * 
	 * @param delTarget
	 */
	@Transactional(rollbackFor=Exception.class)
	public void delete(String[] delTarget) {

		List<User> delList = new ArrayList<User>();

		for (String id : delTarget) {
			
			User user = userRepo.findById(StringUtil.toLong(id)).get();
			user.setDeleted(true);
			delList.add(user);
		}
		
		userRepo.saveAll(delList);
	}
}
