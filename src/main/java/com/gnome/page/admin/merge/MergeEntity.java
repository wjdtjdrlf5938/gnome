package com.gnome.page.admin.merge;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MergeEntity {
	
	@Id
	//id
	private Long id;
	
	//アイデアタイトル
	private String subject;
	
	//親カテゴリー名
	private String parentCategory;
	
	//子カテゴリー名
	private String subCategory;
	
	//アイデア内容
	private String contents;
	
	//アイデア作成日
	private String createdAt;
	
	//アイデア作成者id
	private Long createdBy;
	
	//アイデア作成者名
	private String userName;

	// 画像
	private String profileUrl;

	// ステータス名
	private String statusName;

	// ステータス完了フラグ
	private Long complete;
	
	// ポイント
	private Long point;
	
	// コメント
	private Long comment;
	
	// 賛成投票数
	private String positive;
	
	// 反対投票数
	private String negative;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getParentCategory() {
		return parentCategory;
	}

	public void setParentCategory(String parentCategory) {
		this.parentCategory = parentCategory;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public Long getComplete() {
		return complete;
	}

	public void setComplete(Long complete) {
		this.complete = complete;
	}

	public Long getPoint() {
		return point;
	}

	public void setPoint(Long point) {
		this.point = point;
	}

	public Long getComment() {
		return comment;
	}

	public void setComment(Long comment) {
		this.comment = comment;
	}

	public String getPositive() {
		return positive;
	}

	public void setPositive(String positive) {
		this.positive = positive;
	}

	public String getNegative() {
		return negative;
	}

	public void setNegative(String negative) {
		this.negative = negative;
	}
}
