package com.gnome.page.admin.merge;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MergedEntity {
	
	@Id
	//id
	private Long id;
	
	//アイデアタイトル
	private String subject;
	
	//アイデア内容
	private String contents;
	
	//アイデア作成日
	private String createdAt;
	
	//アイデア作成者id
	private Long createdBy;
	
	//アイデア作成者名
	private String userName;

	// 画像
	private String profileUrl;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}
}
