package com.gnome.page.admin.merge;

import java.util.List;

public class MergeForm {
	
	// 親アイデアID
	private String ideaId;
	
	// 検索キーワード
	private String keyword;
	
	//アイディア検索結果
	private List<MergeEntity> resultList;
	
	//アイディア検索結果
	private List<MergedEntity> mergedList;
	
	// エラーリスト
	private List<String> errList;
	
	// マージ対象一覧
	private String[] targets;

	// ページ数
	private String page = "0";
	
	// ページ最後
	private boolean last;
	
	// トータル件数
	private int totalPages;
	
	public String getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(String ideaId) {
		this.ideaId = ideaId;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public List<MergeEntity> getResultList() {
		return resultList;
	}

	public void setResultList(List<MergeEntity> resultList) {
		this.resultList = resultList;
	}

	public List<MergedEntity> getMergedList() {
		return mergedList;
	}

	public void setMergedList(List<MergedEntity> mergedList) {
		this.mergedList = mergedList;
	}

	public List<String> getErrList() {
		return errList;
	}

	public void setErrList(List<String> errList) {
		this.errList = errList;
	}

	public String[] getTargets() {
		return targets;
	}

	public void setTargets(String[] targets) {
		this.targets = targets;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public boolean isLast() {
		return last;
	}

	public void setLast(boolean last) {
		this.last = last;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
}
