package com.gnome.page.admin.merge;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MergeRepositoryCustom {
	
	public Page<MergeEntity> findIdea(Long id, String keyword, Long spaceId, Pageable pageable);

	public Page<MergedEntity> findMeredIdea(Long id, Pageable pageable);
}
