package com.gnome.page.admin.merge;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.gnome.common.util.StringUtil;

@Repository
public class MergeRepositoryImpl implements MergeRepositoryCustom{
	
	@Autowired
	private EntityManager entityManager;
	
	@Override
	public Page<MergeEntity> findIdea(Long id, String keyword, Long spaceId, Pageable pageable) {
		
		String countSql = "select count(1) ";
		
		String sql = " SELECT "
				+ " ideas.id AS id,"
				+ " ideas.subject,"
				+ " ideas.contents,"
				+ " sub_category.name AS sub_category,"
				+ " parents_category.name AS parent_category,"
				+ " DATE_FORMAT(ideas.created_at,'%Y/%m/%d %H:%i') AS created_at,"
				+ " ideas.created_by AS created_by,"
				+ " concat(users.last_name, users.first_name) AS user_name, "
				+ " users.profile_url, "
				+ "  (up_votes.up_count * settings.positive_point) + (ifnull(down_votes.down_count,0) * settings.negative_point) AS point ,"
				+ " statuses.name status_name,"
				+ " statuses.is_completed as complete, "
				+ " ifnull(comment.comments_count,0) AS comment,"
				+ " ifnull(up_votes.up_count,0) as positive,"
				+ " ifnull(down_votes.down_count,0) as negative";
		
		String conditionSql = ""
				+ " FROM ideas"
				+ " LEFT JOIN categories sub_category ON sub_category.id = ideas.category_id"
		 		+ " LEFT JOIN categories parents_category ON parents_category.id = sub_category.parent_id"
		 		+ " inner JOIN users ON users.id = ideas.created_by"
				+ " inner JOIN developments "
				+ "   ON developments.idea_id = ideas.id"
				+ " inner join statuses"
				+ "   ON statuses.id = developments.status_id"

				+ " LEFT JOIN "
				+ "   (SELECT count(distinct votes.id) AS up_count, votes.idea_id"
				+ "    FROM votes"
				+ "    WHERE votes.is_agreed = true"
				+ "    GROUP BY votes.idea_id) up_votes"
				+ "   ON up_votes.idea_id = ideas.id"
				
				+ " LEFT JOIN "
				+ "   (SELECT count(distinct votes.id) AS down_count,  votes.idea_id "
				+ "    FROM votes "
				+ "    WHERE votes.is_agreed = false"
				+ "    GROUP BY votes.idea_id) down_votes"
				+ "   ON down_votes.idea_id = ideas.id"
				
				+ " LEFT JOIN "
				+ "    ideas merged_ideas "
				+ "   ON merged_ideas.merged_id = ideas.id"
				
				+ " inner join "
				+ "   settings "
				+ " on settings.space_id = ideas.space_id "
				
				+ " LEFT JOIN " 
				+ "  (SELECT count(comments.id) AS comments_count, comments.idea_id "
				+ "   FROM comments "
				+ "   GROUP BY comments.idea_id) comment"
				+ "  ON comment.idea_id = ideas.id "
		
		 		+ " WHERE ideas.is_temporary = false AND ideas.space_id = :spaceId AND ideas.merged_id is null AND ideas.id != :id AND merged_ideas.id is null ";
		
		String orderBy = " order by point desc ";
		
		Map<String, Object> bindParameters = new HashMap<String, Object>();
		
		if(!StringUtil.isEmpty(keyword)) {
			conditionSql += " AND("
					
					+ " ideas.subject like :keyword "
					+ " or "
					+ " ideas.contents like :keyword "
					+ " or "
					+ " ideas.id = :ideaId "
					+ " or "
					+ " developments.develop_memo like :keyword "
					+ ")";
			bindParameters.put("keyword", "%" + keyword + "%");
			bindParameters.put("ideaId", keyword);
		}
		
		bindParameters.put("spaceId", spaceId);
		bindParameters.put("id", id);
		
		Query findQuery =  entityManager.createNativeQuery(sql + conditionSql + orderBy, MergeEntity.class);
		Query countQuery = entityManager.createNativeQuery(countSql+ conditionSql);
		
		for (Map.Entry<String, Object> param : bindParameters.entrySet()) {
			findQuery.setParameter(param.getKey(), param.getValue());
			countQuery.setParameter(param.getKey(), param.getValue());
		}
				
		int total = ((Number) countQuery.getSingleResult()).intValue();
		
		@SuppressWarnings("unchecked")
		List<MergeEntity> ideas = findQuery
				.setFirstResult(pageable.getPageNumber()*pageable.getPageSize())
				.setMaxResults(pageable.getPageSize())
				.getResultList();
		
		return new PageImpl<MergeEntity>(ideas, pageable, total);
	}
	
	@Override
	public Page<MergedEntity> findMeredIdea(Long id, Pageable pageable) {
		
		String countSql = "select count(1) ";
		
		String sql = " SELECT "
				+ " ideas.id AS id,"
				+ " ideas.subject,"
				+ " ideas.contents,"
				+ " DATE_FORMAT(ideas.created_at,'%Y/%m/%d %H:%i') AS created_at,"
				+ " ideas.created_by AS created_by,"
				+ " concat(users.last_name, users.first_name) AS user_name, "
				+ " users.profile_url ";
		
		String conditionSql = ""
				+ " FROM ideas"
		 		+ " inner JOIN users ON users.id = ideas.created_by"

		 		+ " WHERE ideas.merged_id = :id";
		
		String orderBy = " order by id ";
		
		Query findQuery =  entityManager.createNativeQuery(sql + conditionSql + orderBy, MergedEntity.class);
		Query countQuery = entityManager.createNativeQuery(countSql+ conditionSql);
		
		findQuery.setParameter("id", id);
		countQuery.setParameter("id", id);
		
		int total = ((Number) countQuery.getSingleResult()).intValue();
		
		@SuppressWarnings("unchecked")
		List<MergedEntity> ideas = findQuery
				.setFirstResult(pageable.getPageNumber()*pageable.getPageSize())
				.setMaxResults(pageable.getPageSize())
				.getResultList();
		
		return new PageImpl<MergedEntity>(ideas, pageable, total);
	}
}
