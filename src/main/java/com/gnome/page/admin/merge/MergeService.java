package com.gnome.page.admin.merge;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gnome.auth.LoginUser;
import com.gnome.base.entity.Idea;
import com.gnome.base.entity.Vote;
import com.gnome.base.repository.IdeaRepository;
import com.gnome.base.repository.VoteRepository;
import com.gnome.common.exception.BusinessException;
import com.gnome.common.util.StringUtil;

@Service
public class MergeService {
	
	@Autowired
	private MergeRepositoryCustom customRepo;
	
	@Autowired
	private IdeaRepository ideaRepo;
	
	@Autowired
	private VoteRepository voteRepo;
		
	/**
	 * 検索処理
	 * 
	 * @param form
	 */
	@Transactional(rollbackFor=Exception.class)
	public void mergeSearch(MergeForm form) {
		
		Pageable pageable = PageRequest.of(StringUtil.toInteger(form.getPage()), 5);
		
		// スペースID
		Long spaceId = ((LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getLoginUser().getSpaceId();
		
		Page<MergeEntity> ideas = customRepo.findIdea(StringUtil.toLong(form.getIdeaId()),form.getKeyword(), spaceId, pageable);
		
		// 検索結果
		form.setResultList(ideas.getContent());
		form.setLast(ideas.isLast());
		form.setTotalPages(ideas.getTotalPages());
	}
	
	/**
	 * マージされたアイデア取得
	 * @param form
	 */
	@Transactional(rollbackFor=Exception.class)
	public void mergedSearch(MergeForm form) {
		
		Pageable pageable = PageRequest.of(StringUtil.toInteger(form.getPage()), 5);
		
		Page<MergedEntity> ideas = customRepo.findMeredIdea(StringUtil.toLong(form.getIdeaId()),pageable);
		
		// 検索結果
		form.setMergedList(ideas.getContent());
		form.setLast(ideas.isLast());
	}
	
	/**
	 * マージ処理
	 * 
	 * @param form
	 * @throws BusinessException
	 * @throws ParseException
	 */
	@Transactional(rollbackFor=Exception.class)
	public void merge(MergeForm form) throws BusinessException, ParseException {
		
		//マージするアイデア
		Long mergeId = StringUtil.toLong(form.getIdeaId());
		
		List<Long> ids = new ArrayList<Long>();
		for (String id : form.getTargets()) {
			ids.add(StringUtil.toLong(id));
		}
		List<Idea> ideaList = ideaRepo.findByIdIn(ids);
		
		for (Idea idea : ideaList) {
			// アイデアにマージid設定
			idea.setMergedId(mergeId);
		}
		
		ideaRepo.saveAll(ideaList);
		
		// マージされるアイデアの投票検索
		List<Vote> ideaVoteList = voteRepo.findByIdeaIdIn(ids);

		// マージするアイデアの投票検索
		List<Vote> mergeIdeaVoteList = voteRepo.findByIdeaId(mergeId);
		
		//削除処理リスト
		List<Vote> ideaVoteDelList = new ArrayList<Vote>();
		
		//移動する投票リスト
		List<Vote> ideaVotesUpList = new ArrayList<Vote>();
		
		// マージされるアイデア投票を更新
		for (Vote vote : ideaVoteList) {
			
			// マージ先に同じユーザーIDがあるかどうか
			List<Vote> mergeToList = mergeIdeaVoteList.stream().filter(x -> x.getUserId() == vote.getUserId()).collect(Collectors.toList());
			
			if (mergeToList.size() > 0) {
				ideaVoteDelList.add(vote);
			}
			
			// マージされるアイデア同士で同じユーザーIDがあるかどうか
			List<Vote> inMergeList = ideaVoteList.stream().filter(x -> x.getUserId() == vote.getUserId() && x.getId() != vote.getId()).collect(Collectors.toList());
			if (inMergeList.size() > 0) {
				ideaVoteDelList.add(vote);
			}
			
			// アイデアID設定
			vote.setIdeaId(mergeId);
		}
		
		//削除処理
		voteRepo.deleteInBatch(ideaVoteDelList);
		
		//移動処理
		voteRepo.saveAll(ideaVotesUpList);
	}
}
