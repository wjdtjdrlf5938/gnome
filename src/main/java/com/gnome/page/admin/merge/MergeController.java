package com.gnome.page.admin.merge;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gnome.common.exception.BusinessException;
import com.google.gson.Gson;

@RestController
public class MergeController {
	
	@Autowired
	private MergeService service;
	
	/**
	 * マージアイデア検索処理
	 * 
	 * @param form
	 * @return
	 */
	@RequestMapping(path="/auth/merge/search", method = RequestMethod.GET)
	@ResponseBody
    public String mergesearch(MergeForm form) {
		
		Gson gson = new Gson();
		
		//検索処理
		service.mergeSearch(form);
		
		return gson.toJson(form);
	}
	
	
	/**
	 * マージ処理
	 * 
	 * @param id
	 * @param form
	 * @return
	 */
	@RequestMapping(path="/auth/merge", method = RequestMethod.POST)
	@ResponseBody
	public String merge(MergeForm form, RedirectAttributes attributes) throws ParseException{
		
		Gson gson = new Gson();
		
		try {
			
			//マージ処理
			service.merge(form);
			
		} catch (BusinessException e) {
			e.printStackTrace();
			form.setErrList(e.getErrors());
		}
		
		return gson.toJson(form);
	}

	/**
	 * マージされたアイデア一覧
	 * 
	 * @param form
	 * @return
	 */
	@RequestMapping(path="/merged", method = RequestMethod.GET)
	@ResponseBody
    public String merged(MergeForm form) {
		
		Gson gson = new Gson();
		
		//検索処理
		service.mergedSearch(form);
		
		return gson.toJson(form);
	}
	
}
