package com.gnome.page.admin.category;

import java.util.List;

import javax.validation.constraints.AssertTrue;
import com.gnome.common.util.StringUtil;

public class CategoryForm {
	
	//親カテゴリーid
	private Long parentCategoryId;
	
	//親カテゴリー順番
	private List<String> parentCategoryOrder;
	
	//親カテゴリーリスト
	private List<CategoryEntity> parentCategoryList;
	
	//子カテゴリー順番
	private List<String> categoryOrder;
	
	//子カテゴリーリスト
	private List<CategoryEntity> categoryList;
	
	//削除List
	private String[] delTarget;
	
	
	// カテゴリーリストチェック
	@AssertTrue(message="カテゴリーを設定してください")
	public boolean isListCheck() {
		
		boolean result = true;
		
		if(parentCategoryList.size() == 0) {
			result = false;
		}
		
		return result;
	}
	
	
	// カテゴリー名チェック
	@AssertTrue(message="カテゴリー名を入力してください")
	public boolean isNameCheck() {
		
		boolean result = true;
		
		for(CategoryEntity entity : parentCategoryList) {
			
			//idがない場合、処理なし
			if(StringUtil.isEmpty(entity.getId())) {
				continue;
			}
			
			//親カテゴリーの名前がない場合、エラー
			if(StringUtil.isEmpty(entity.getName())) {
				result = false;
			}
		}
		
		//子カテゴリーがあるカテゴリー制限
		if(categoryList != null) {
			for(CategoryEntity entity : categoryList) {
				
				//idがない場合、処理なし
				if(StringUtil.isEmpty(entity.getId())) {
					continue;
				}
				
				//子カテゴリー名がない場合、エラー
				if(StringUtil.isEmpty(entity.getName())) {
					result = false;
				}
			}
		}
		
		return result;
	}


	public Long getParentCategoryId() {
		return parentCategoryId;
	}


	public void setParentCategoryId(Long parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}


	public List<String> getParentCategoryOrder() {
		return parentCategoryOrder;
	}


	public void setParentCategoryOrder(List<String> parentCategoryOrder) {
		this.parentCategoryOrder = parentCategoryOrder;
	}


	public List<CategoryEntity> getParentCategoryList() {
		return parentCategoryList;
	}


	public void setParentCategoryList(List<CategoryEntity> parentCategoryList) {
		this.parentCategoryList = parentCategoryList;
	}


	public List<String> getCategoryOrder() {
		return categoryOrder;
	}


	public void setCategoryOrder(List<String> categoryOrder) {
		this.categoryOrder = categoryOrder;
	}


	public List<CategoryEntity> getCategoryList() {
		return categoryList;
	}


	public void setCategoryList(List<CategoryEntity> categoryList) {
		this.categoryList = categoryList;
	}


	public String[] getDelTarget() {
		return delTarget;
	}


	public void setDelTarget(String[] delTarget) {
		this.delTarget = delTarget;
	}
}
