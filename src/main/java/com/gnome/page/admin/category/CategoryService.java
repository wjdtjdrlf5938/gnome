package com.gnome.page.admin.category;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gnome.auth.LoginUser;
import com.gnome.base.entity.Category;
import com.gnome.base.repository.CategoryRepository;
import com.gnome.common.util.DecimalUtil;
import com.gnome.common.util.StringUtil;

@Service
public class CategoryService {
	
	@Autowired
	private CategoryRepository repo;
	
	@Autowired
	private CategoryRepositoryCustom customRepo;
	
	/**
	 * 検索処理
	 * 
	 * @param form
	 */
	@Transactional(rollbackFor=Exception.class)
	public void search(CategoryForm form) {
		
		// スペースID
		Long spaceId = ((LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getLoginUser().getSpaceId();
		
		//親カテゴリー検索処理
		List<CategoryEntity> parentCategoryList = customRepo.findParentsCategory(spaceId);
		form.setParentCategoryList(parentCategoryList);
		
		//子カテゴリー検索処理
		if(!DecimalUtil.isEmpty(form.getParentCategoryId())) {
			List<CategoryEntity> categoryList = customRepo.findSubCategory(form.getParentCategoryId());
			form.setCategoryList(categoryList);
		}
	}
	
	/**
	 * カテゴリー更新処理
	 * 
	 * @param targetList
	 * @param form
	 */
	@Transactional(rollbackFor=Exception.class)
	public void categoryUpdate(List<CategoryEntity> targetList, CategoryForm form){
		
		List<Category> addList = new ArrayList<Category>();
		
		// スペースID
		Long spaceId = ((LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getLoginUser().getSpaceId();
		
		// カテゴリー編集処理
		for(CategoryEntity entity : targetList) {
			
			//idがnullの場合、処理なし
			if(entity.getId() == null) {
				continue;
			}
			
			// カテゴリー順番
			Long orderNo = (long)form.getCategoryOrder().indexOf(entity.getId())+1;
			
			Optional<Category> optionalCategory = repo.findById(StringUtil.toLong(entity.getId()));
			
			//存在しない場合、登録処理
			Category category = new Category();
			
			category.setParentId(form.getParentCategoryId() == null ? 0l : form.getParentCategoryId());
			category.setSpaceId(spaceId);
			
			// 存在する場合
			if (optionalCategory.isPresent()) {
				category = optionalCategory.get();
			}
			
			category.setName(entity.getName());
			category.setOrder(orderNo);
			
			addList.add(category);
		}
		
		repo.saveAll(addList);
		
		// 削除処理
		delete(form.getDelTarget());
	}
	
	
	/**
	 * 削除処理
	 * @param delTarget
	 */
	private void delete(String[] delTarget) {
		
		//親カテゴリー削除リスト
		List<Category> delList = new ArrayList<Category>();
		
		for (String id : delTarget) {
			
			Long parentCategoryId = StringUtil.toLong(id);
			
			Category parentCategory = new Category();
			parentCategory.setId(parentCategoryId);
			delList.add(parentCategory);
			
			// 親カテゴリーを削除したら子カテゴリー削除
			List<Category> categoryList = repo.findByParentIdOrderByOrder(parentCategoryId);
			if(categoryList != null) {
				for (Category category : categoryList) {
					delList.add(category);
				}
			}
		}
		
		//カテゴリー削除
		repo.deleteInBatch(delList);
	}
}
