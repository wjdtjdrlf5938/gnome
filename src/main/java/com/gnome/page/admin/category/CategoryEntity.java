package com.gnome.page.admin.category;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CategoryEntity {
	
	@Id
	//Id
	private String id;
	
	//カテゴリー名
	private String name; 
	
	//親カテゴリーid
	private Long ParentId;
	
	//順番
	private String order;
	
	//参照状態
	private boolean delImposibleFlg;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Long getParentId() {
		return ParentId;
	}

	public void setParentId(Long parentId) {
		ParentId = parentId;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public boolean isDelImposibleFlg() {
		return delImposibleFlg;
	}

	public void setDelImposibleFlg(boolean delImposibleFlg) {
		this.delImposibleFlg = delImposibleFlg;
	}
}
