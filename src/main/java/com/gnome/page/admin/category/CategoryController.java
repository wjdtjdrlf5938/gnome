package com.gnome.page.admin.category;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gnome.common.consts.CodeConsts;
import com.gnome.common.consts.MessageIdConsts;
import com.gnome.common.util.StringUtil;

@Controller
public class CategoryController {
	
	@Autowired
	private CategoryService service;
	
	@Autowired
	private MessageSource msgSource;
	
	/**
	 * カテゴリー検索
	 * @param form
	 * @return
	 */
	@RequestMapping(value = "/auth/category")
    public String index(CategoryForm form) {
		
		//検索処理
		service.search(form);
		
		return "admin/category";
	}
	
	/**
	 * カテゴリー更新
	 * 
	 * @param form
	 * @param errors
	 * @param attributes
	 * @return
	 */
	@RequestMapping(value = "auth/category/update")
    public String categoryUpdate(@Validated CategoryForm form, BindingResult errors, RedirectAttributes attributes) {
		
		// バリデーションチェックエラーがある場合
		if (errors.hasErrors()) {
			return "admin/category";
		}
		
		List<CategoryEntity> targetList = form.getParentCategoryList();
		
		//処理完了後、URL設定
		String url = "";
		
		// 子カテゴリー更新の場合
		if(!StringUtil.isEmpty(form.getParentCategoryId())) {
			targetList = form.getCategoryList();
			url = "?parentCategoryId=" + form.getParentCategoryId();
		}
		
		//更新処理
		service.categoryUpdate(targetList, form);

		//成功したら、メッセージ
		attributes.addFlashAttribute(CodeConsts.MESSAGEKBN_SUCCESS, msgSource.getMessage(MessageIdConsts.MESSAGE_ID_I000001, new String[] {"保存"}, null));
		
		return "redirect:/auth/category" + url;
	}
}
