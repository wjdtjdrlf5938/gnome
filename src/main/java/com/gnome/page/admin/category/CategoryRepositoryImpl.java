package com.gnome.page.admin.category;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@SuppressWarnings("unchecked")
@Repository
public class CategoryRepositoryImpl implements CategoryRepositoryCustom{
	
	@Autowired
	private EntityManager entityManager;
	
	@Override
	public List<CategoryEntity> findParentsCategory(Long spaceId) {
		
		String sql = " SELECT " 
				+ " DISTINCT categories.id," 
				+ " categories.name,"
				+ " categories.parent_id,"
				+ " categories.order,"
				+ " case when sub_category.parent_id IS null then 0 else 1 end as del_imposible_flg"
				+ " FROM categories" 
				
				+ " LEFT JOIN "
				+ "    (SELECT DISTINCT categories.parent_id "
				+ "     FROM categories"
				+ "        INNER JOIN ideas "
				+ "          ON ideas.category_id = categories.id ) sub_category"
				+ "   ON sub_category.parent_id = categories.id"
				
				+ " WHERE categories.parent_id = 0 AND categories.space_id = :spaceId ";
		
		String orderBy = " ORDER BY categories.order ";
		
		Query findQuery =  entityManager.createNativeQuery(sql + orderBy, CategoryEntity.class);
		findQuery.setParameter("spaceId", spaceId);
		
		return findQuery.getResultList();
	}
	
	@Override
	public List<CategoryEntity> findSubCategory(Long parentId) {
		
		String sql = " SELECT " 
				+ " DISTINCT categories.id,"
				+ " categories.name,"
				+ " categories.parent_id,"
				+ " categories.order,"
				+ " case when ideas.id IS null then 0 else 1 end as del_imposible_flg"
				+ " FROM categories "
				
				+ " LEFT JOIN ideas "
				+ "   ON ideas.category_id = categories.id "
				
				+ " WHERE categories.parent_id = :parentId ";
		
		String orderBy = " ORDER BY categories.order ";
		
		Query findQuery =  entityManager.createNativeQuery(sql + orderBy, CategoryEntity.class);
		
		findQuery.setParameter("parentId", parentId);
		
		return findQuery.getResultList();
	}
}
