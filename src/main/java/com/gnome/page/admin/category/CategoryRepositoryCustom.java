package com.gnome.page.admin.category;

import java.util.List;

public interface CategoryRepositoryCustom {
	
	public List<CategoryEntity> findParentsCategory(Long spaceId);
	
	public List<CategoryEntity> findSubCategory(Long parentId);


}
