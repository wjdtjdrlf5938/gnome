package com.gnome.page.admin.status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gnome.common.consts.CodeConsts;
import com.gnome.common.consts.MessageIdConsts;

@Controller
public class StatusController {
	
	@Autowired
	private StatusService service;
	
	@Autowired
	private MessageSource msgSource;
	
	/**
	 * ステータス一覧
	 * 
	 * @param form
	 * @return
	 */
	@RequestMapping(value = "/auth/status")
	public String index(StatusForm form) {
		
		//検索処理
		service.index(form);
		
		return "admin/status";
	}
	
	/**
	 * ステータス変更
	 * @param form
	 * @param errors
	 * @param attributes
	 * @return
	 */
	@RequestMapping(value = "/auth/status/update")
	public String update(@Validated StatusForm form, BindingResult errors, RedirectAttributes attributes) {
		
		// バリデーションチェックエラーがある場合
		if (errors.hasErrors()) {
			return "admin/status";
		}
		
		//更新処理
		service.update(form);
		
		//成功したら、メッセージ
		attributes.addFlashAttribute(CodeConsts.MESSAGEKBN_SUCCESS, msgSource.getMessage(MessageIdConsts.MESSAGE_ID_I000001, new String[] {"保存"}, null));

		return "redirect:/auth/status";
	}
}
