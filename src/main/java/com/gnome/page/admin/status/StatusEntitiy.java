package com.gnome.page.admin.status;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class StatusEntitiy {
	
	@Id
	//Id
	private String id;
	
	//ステータス名
	private String name;
	
	//順番
	private String order;
	
	//初期ステータス
	private boolean isInitial;
	
	//参照状態
	private boolean delImposibleFlg;

	// 完了フラグ
	private String completed;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public boolean isInitial() {
		return isInitial;
	}

	public void setInitial(boolean isInitial) {
		this.isInitial = isInitial;
	}

	public boolean isDelImposibleFlg() {
		return delImposibleFlg;
	}

	public void setDelImposibleFlg(boolean delImposibleFlg) {
		this.delImposibleFlg = delImposibleFlg;
	}

	public String getCompleted() {
		return completed;
	}

	public void setCompleted(String completed) {
		this.completed = completed;
	}
}
