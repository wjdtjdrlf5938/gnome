package com.gnome.page.admin.status;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@SuppressWarnings("unchecked")
@Repository
public class StatusRepositoryImpl implements StatusRepositoryCustom{
	
	@Autowired
	private EntityManager entityManager;
	
	@Override
	public List<StatusEntitiy> search(Long spaceId) {
		
		String sql = " SELECT "
				+ " DISTINCT statuses.id AS id,"
				+ " statuses.name,"
				+ " statuses.order,"
				+ " statuses.is_initial,"
				+ " statuses.is_completed as completed,"
				+ " case when developments.id IS null then 0 else 1 end as del_imposible_flg"
				+ " FROM statuses"
				+ " LEFT JOIN developments"
				+ "  ON developments.status_id = statuses.id"
				+ " WHERE statuses.space_id = :spaceId ";
		
		String orderBy = " ORDER BY statuses.order ";
		
		Query findQuery =  entityManager.createNativeQuery(sql + orderBy, StatusEntitiy.class);
		findQuery.setParameter("spaceId", spaceId);
		
		return findQuery.getResultList();
	}
}
