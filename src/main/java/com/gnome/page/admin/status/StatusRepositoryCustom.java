package com.gnome.page.admin.status;

import java.util.List;

public interface StatusRepositoryCustom {
	
	public List<StatusEntitiy> search(Long spaceId);

}
