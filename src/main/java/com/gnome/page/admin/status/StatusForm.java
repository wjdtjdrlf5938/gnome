package com.gnome.page.admin.status;

import java.util.List;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;

import com.gnome.common.util.StringUtil;

public class StatusForm {
	
	//ステータスリスト
	private List<StatusEntitiy> statusList;
	
	//選択肢順番
	private List<String> idList;
	
	//削除List
	private String[] delTarget;
		
	// 初期ステータス名
	@NotBlank(message="{E000005}")
	private String initStatusName;
	
	// 初期ステータスID
	private String initStatusId;
	
	//ステータス名チェック
	@AssertTrue(message="選択肢名を入力してください")
	public boolean isNameCheck() {
		
		boolean result = true;
		
		if (statusList == null) {
			return result;
		}
		
		for(StatusEntitiy entity : statusList) {
			
			//idがない場合、処理なし
			if(StringUtil.isEmpty(entity.getId())) {
				continue;
			}
			
			// 名前がない場合、エラー
			if(StringUtil.isEmpty(entity.getName())) {
				result = false;
			}
		}
		return result;
	}

	public List<StatusEntitiy> getStatusList() {
		return statusList;
	}

	public void setStatusList(List<StatusEntitiy> statusList) {
		this.statusList = statusList;
	}

	public List<String> getIdList() {
		return idList;
	}

	public void setIdList(List<String> idList) {
		this.idList = idList;
	}

	public String[] getDelTarget() {
		return delTarget;
	}

	public void setDelTarget(String[] delTarget) {
		this.delTarget = delTarget;
	}

	public String getInitStatusName() {
		return initStatusName;
	}

	public void setInitStatusName(String initStatusName) {
		this.initStatusName = initStatusName;
	}

	public String getInitStatusId() {
		return initStatusId;
	}

	public void setInitStatusId(String initStatusId) {
		this.initStatusId = initStatusId;
	}
}
