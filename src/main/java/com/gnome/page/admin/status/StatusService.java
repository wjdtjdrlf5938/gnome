package com.gnome.page.admin.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gnome.auth.LoginUser;
import com.gnome.base.entity.Status;
import com.gnome.base.repository.StatusRepository;
import com.gnome.common.util.StringUtil;

@Service
public class StatusService {
	
	@Autowired
	private StatusRepository repo;
	
	@Autowired
	private StatusRepositoryCustom customRepo;
	
	/**
	 * 検索処理
	 * @param form
	 */
	@Transactional(rollbackFor=Exception.class)
	public void index(StatusForm form) {
		
		// スペースID
		Long spaceId = ((LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getLoginUser().getSpaceId();
		
		//検索処理
		List<StatusEntitiy> statusList = customRepo.search(spaceId);
		
		List<StatusEntitiy> initList = statusList.stream().filter(x -> x.isInitial()).collect(Collectors.toList());
		StatusEntitiy initEntity = new StatusEntitiy();
		
		if (initList.size() > 0) {
			initEntity = initList.get(0);
		}
		statusList.remove(initEntity);
		
		form.setStatusList(statusList);
		form.setInitStatusId(initEntity.getId());
		form.setInitStatusName(initEntity.getName());
	}
	
	/**
	 * ステータス更新
	 * @param form
	 */
	@Transactional(rollbackFor=Exception.class)
	public void update(StatusForm form)  {
		
		List<Status> updateList = new ArrayList<Status>();
		
		// スペースID
		Long spaceId = ((LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getLoginUser().getSpaceId();
		
		// 更新対象リスト作成
		createList(updateList, form, spaceId);
		
		Status initStatus = new Status();
		initStatus.setSpaceId(spaceId);
		initStatus.setOrder(1l);
		initStatus.setInitial(true);
		
		if(!StringUtil.isEmpty(form.getInitStatusId())) {
			// 初期ステータス
			Optional<Status> initStatusOptional = repo.findById(StringUtil.toLong(form.getInitStatusId()));
			if (initStatusOptional.isPresent()) {
				initStatus = initStatusOptional.get();
			}
		}
		initStatus.setName(form.getInitStatusName());
		updateList.add(initStatus);
		
		repo.saveAll(updateList);
		
		// 削除処理
		delete(form.getDelTarget());
	}
	
	/**
	 * 更新対象リスト作成
	 * 
	 * @param updateList
	 * @param form
	 * @param spaceId
	 */
	private void createList(List<Status> updateList, StatusForm form, Long spaceId) {
		
		if (form.getStatusList() == null) {
			return;
		}
		
		//編集処理
		for(StatusEntitiy entity : form.getStatusList()) {
			
			//idがnullの場合、処理なし
			if(entity.getId() == null) {
				continue;
			}
			
			//ソート順番
			Long orderNo = (long)form.getIdList().indexOf(entity.getId())+1;
		
			Optional<Status> optionalStatus = repo.findById(StringUtil.toLong(entity.getId()));
			
			Status status = new Status();
			status.setInitial(false);
			status.setSpaceId(spaceId);
			
			//存在した場合
			if (optionalStatus.isPresent()) {
				status = optionalStatus.get();
			}
			
			status.setName(entity.getName());
			status.setCompleted(!StringUtil.isEmpty(entity.getCompleted()));
			status.setOrder(orderNo);
			updateList.add(status);
		}
	}
	
	/**
	 * 削除処理
	 * @param delTarget
	 */
	private void delete(String[] delTarget){
		
		List<Status> delList = new ArrayList<Status>();
		
		for (String id : delTarget) {
			Status status = new Status();
			status.setId(StringUtil.toLong(id));
			delList.add(status);
		}
		
		repo.deleteInBatch(delList);
	}
}
