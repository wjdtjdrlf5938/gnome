package com.gnome.page.admin.editManagement;

import java.text.ParseException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gnome.auth.LoginUser;
import com.gnome.base.entity.Development;
import com.gnome.base.repository.DevelopmentRepository;
import com.gnome.common.consts.MessageIdConsts;
import com.gnome.common.exception.BusinessException;
import com.gnome.common.util.DecimalUtil;
import com.gnome.common.util.StringUtil;

@Service
public class EditManagementService {
	
	@Autowired
	private DevelopmentRepository developmentRepo;
	
	@Autowired
	private EditManagementRepositoryCustom customRepo;
	
	@Autowired
	private MessageSource msgSource;
	
	
	/**
	 * 検索処理
	 * 
	 * @param form
	 * @throws NoSuchMessageException 
	 * @throws BusinessException
	 * @throws ParseException 
	 */
	@Transactional(rollbackFor=Exception.class)
	public void search(EditManagementForm form) throws NoSuchMessageException, BusinessException {
		
		// スペースID
		Long spaceId = ((LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getLoginUser().getSpaceId();
		
		// 検索実行
		Optional<EditManagementEntity> ideaOptional = customRepo.findIdea(StringUtil.toLong(form.getId()), spaceId);
		
		// 存在しない場合、エラー
		if(!ideaOptional.isPresent()) {
			throw new BusinessException(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000001, null, null));
		}
		
		EditManagementEntity entity = ideaOptional.get();
		form.setDevelopmentMemo(entity.getDevelopmentMemo());
		form.setExpectedOn(entity.getExpectedOn());
		form.setReleasedOn(entity.getReleasedOn());
		form.setStatusId(entity.getStatusId());
		form.setVersion(DecimalUtil.toString(entity.getVersion()));
		
		form.setEntity(entity);
	}
	
	
	/**
	 * 更新処理
	 * 
	 * @param form
	 * @throws BusinessException
	 * @throws ParseException 
	 */
	@Transactional(rollbackFor=Exception.class)
	public void update(EditManagementForm form) throws BusinessException, ParseException {
		
		Long ideaId = StringUtil.toLong(form.getId());
		
		Optional<Development> devOptional = developmentRepo.findByIdeaId(ideaId);
		
		//アイデアがない場合、エラー
		if (!devOptional.isPresent() || devOptional.get().getVersion() != Integer.parseInt(form.getVersion())) {
			throw new BusinessException(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000004, null, null));
		}
		
		Development development = devOptional.get();
		development.setStatusId(form.getStatusId());
		development.setExpectedOn(StringUtil.toDate(form.getExpectedOn()));
		development.setReleasedOn(StringUtil.toDate(form.getReleasedOn()));
		development.setDevelopMemo(form.getDevelopmentMemo());
		developmentRepo.saveAndFlush(development);
	}
}
