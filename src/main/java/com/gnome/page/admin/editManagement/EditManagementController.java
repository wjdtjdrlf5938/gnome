package com.gnome.page.admin.editManagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.BindingResult;

import java.text.ParseException;

import com.gnome.common.exception.BusinessException;
import com.google.gson.Gson;

@RestController
public class EditManagementController {
	
	@Autowired
	private EditManagementService service;
	
	
	/**
	 * 検索処理
	 * 
	 * @param id
	 * @param form
	 * @return
	 */
	@RequestMapping(path="/auth/management/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String search(@PathVariable("id") String id, EditManagementForm form, BindingResult errors){
		
		Gson gson = new Gson();
		
		//検索処理
		try {
			
			service.search(form);
		
		} catch (BusinessException e) {
			e.printStackTrace();
			form.setErrList(e.getErrors());
		}

		return gson.toJson(form);
	}
	
	/**
	 * 更新処理
	 * 
	 * @param id
	 * @param form
	 * @return
	 */
	@RequestMapping(path="/auth/management/update", method = RequestMethod.PUT)
	@ResponseBody
	public String update(EditManagementForm form, BindingResult errors) throws ParseException{
		
		Gson gson = new Gson();
		
		try {
			
			//更新処理
			service.update(form);
			
		} catch (BusinessException e) {
			e.printStackTrace();
			form.setErrList(e.getErrors());
		}
		
		return gson.toJson(form);
	}
}
