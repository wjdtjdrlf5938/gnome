package com.gnome.page.admin.editManagement;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class EditManagementEntity {
	
	@Id
	//アイデアのid
	private Long id;
	
	//開発メモの内容
	private String developmentMemo;
	
	//リリース予定日
	private String expectedOn;
	
	//リリース日
	private String releasedOn;
	
	//アイデアのタイトル
	private String subject;
	
	//アイデアの内容
	private String contents;
	
	//アイデアの子カテゴリー
	private String subCategory;
	
	//アイデアの親カテゴリー
	private String parentCategory;
	
	//アイデアのステータスid
	private Long statusId;
	
	//アイデア作成日
	private String createdAt;
	
	//アイデア作成者id
	private Long createdBy;
	
	//アイデア作成者名
	private String userName;
	
	//version
	private Long version;

	// プロフィール画像
	private String profileUrl;
	
	// ステータス名
	private String statusName;
	
	// ステータス完了フラグ
	private Long completeFlg;
	
	// ポイント
	private String point;
	
	// コメント数
	private String comment;
	
	// 賛成投票数
	private String positive;
	
	// 反対投票数
	private String negative;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDevelopmentMemo() {
		return developmentMemo;
	}

	public void setDevelopmentMemo(String developmentMemo) {
		this.developmentMemo = developmentMemo;
	}

	public String getExpectedOn() {
		return expectedOn;
	}

	public void setExpectedOn(String expectedOn) {
		this.expectedOn = expectedOn;
	}

	public String getReleasedOn() {
		return releasedOn;
	}

	public void setReleasedOn(String releasedOn) {
		this.releasedOn = releasedOn;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public String getParentCategory() {
		return parentCategory;
	}

	public void setParentCategory(String parentCategory) {
		this.parentCategory = parentCategory;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public Long getCompleteFlg() {
		return completeFlg;
	}

	public void setCompleteFlg(Long completeFlg) {
		this.completeFlg = completeFlg;
	}

	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getPositive() {
		return positive;
	}

	public void setPositive(String positive) {
		this.positive = positive;
	}

	public String getNegative() {
		return negative;
	}

	public void setNegative(String negative) {
		this.negative = negative;
	}
}
