package com.gnome.page.admin.editManagement;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class EditManagementRepositoryImpl implements EditManagementRepositoryCustom{
	
	@Autowired
	private EntityManager entityManager;
	
	@Override
	public Optional<EditManagementEntity> findIdea(Long ideaId, Long spaceId) {
		
		String sql = " SELECT "
				 + " ideas.id,"
				 + " developments.develop_memo AS development_memo,"
				 + " DATE_FORMAT(developments.expected_on,'%Y/%m/%d')  AS expected_on,"
				 + " DATE_FORMAT(developments.released_on,'%Y/%m/%d')  AS released_on,"
				 + " ideas.subject,"
				 + " ideas.contents,"
				 + " sub_category.name AS sub_category,"
				 + " parents_category.name AS parent_category,"
				 + " statuses.id AS status_id,"
				 + " DATE_FORMAT(ideas.created_at,'%Y/%m/%d %H:%i') AS created_at,"
				 + " ideas.created_by AS created_by,"
				 + " concat(users.last_name, users.first_name) AS user_name,"
				 + " developments.version,"
				 + " users.profile_url,"
				 + " statuses.name as status_name, "
				 + " statuses.is_completed as complete_flg,"
				 + " format((ifnull(up_votes.up_count,0) * settings.positive_point) + (ifnull(down_votes.down_count,0) * settings.negative_point),0) AS point ,"
				 + " ifnull(comment.comments_count,0) AS comment, "
				 + " ifnull(up_votes.up_count,0) as positive,"
				 + " ifnull(down_votes.down_count,0) as negative"
		
				 + " FROM ideas"
				 
				 + " inner JOIN developments ON ideas.id = developments.idea_id"
				 + " inner JOIN statuses ON statuses.id = developments.status_id"
				 + " LEFT JOIN categories sub_category ON sub_category.id = ideas.category_id"
				 + " LEFT JOIN categories parents_category ON parents_category.id = sub_category.parent_id"
				 + " inner JOIN users ON users.id = ideas.created_by"
				 + " LEFT JOIN "
				+ "   (SELECT count(distinct votes.id) AS up_count, votes.idea_id"
				+ "    FROM votes"
				+ "    WHERE votes.is_agreed = true"
				+ "    GROUP BY votes.idea_id) up_votes"
				+ "   ON up_votes.idea_id = ideas.id"
				
				+ " LEFT JOIN "
				+ "   (SELECT count(distinct votes.id) AS down_count,  votes.idea_id "
				+ "    FROM votes "
				+ "    WHERE votes.is_agreed = false"
				+ "    GROUP BY votes.idea_id) down_votes"
				+ "   ON down_votes.idea_id = ideas.id"
				+ " inner join "
				+ "   settings "
				+ " on settings.space_id = ideas.space_id "
				+ " LEFT JOIN " 
				+ "  (SELECT count(comments.id) AS comments_count, comments.idea_id "
				+ "   FROM comments "
				+ "   GROUP BY comments.idea_id) comment"
				+ "  ON comment.idea_id = ideas.id "
				
				 + " WHERE developments.idea_id = :ideaId AND ideas.space_id = :spaceId";
		
		Query findQuery =  entityManager.createNativeQuery(sql, EditManagementEntity.class);
		
		//idパラメーター設定
		findQuery.setParameter("ideaId", ideaId);
		findQuery.setParameter("spaceId", spaceId);
		
		EditManagementEntity detail = null;
		
		try {
			detail = (EditManagementEntity) findQuery.getSingleResult();	
		} catch(NoResultException ex) {
		}
		
		return Optional.ofNullable(detail);
	}
}
