package com.gnome.page.admin.editManagement;

import java.util.Optional;

public interface EditManagementRepositoryCustom {
	
	public Optional<EditManagementEntity> findIdea(Long ideaId , Long spaceId);

}
