package com.gnome.page.admin.editManagement;

import java.util.List;

import com.gnome.base.common.BaseForm;

public class EditManagementForm extends BaseForm{
	
	// ID
	private String id;
	
	//開発メモの内容
	private String developmentMemo;
	
	// リリース予定日
	private String expectedOn;
	
	// リリース日
	private String releasedOn;
	
	//アイデアのステータスid
	private Long statusId;
	
	// アイデア詳細
	private EditManagementEntity entity;
	
	//version
	private String version;
	
	// エラーリスト
	private List<String> errList;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDevelopmentMemo() {
		return developmentMemo;
	}

	public void setDevelopmentMemo(String developmentMemo) {
		this.developmentMemo = developmentMemo;
	}

	public String getExpectedOn() {
		return expectedOn;
	}

	public void setExpectedOn(String expectedOn) {
		this.expectedOn = expectedOn;
	}

	public String getReleasedOn() {
		return releasedOn;
	}

	public void setReleasedOn(String releasedOn) {
		this.releasedOn = releasedOn;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public EditManagementEntity getEntity() {
		return entity;
	}

	public void setEntity(EditManagementEntity entity) {
		this.entity = entity;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public List<String> getErrList() {
		return errList;
	}

	public void setErrList(List<String> errList) {
		this.errList = errList;
	}
}
