package com.gnome.page.admin.report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gnome.base.entity.Report;
import com.gnome.base.repository.ReportRepository;

import com.gnome.common.consts.MessageIdConsts;
import com.gnome.common.exception.BusinessException;
import com.gnome.common.util.StringUtil;

@Service
public class ReportService {
	
	@Autowired
	private ReportRepository repo;
	
	@Autowired
	private MessageSource msgSource;
	
	
	@Transactional(rollbackFor=Exception.class)
	public void create(ReportForm form) throws BusinessException {
		
		//報告の内容がない場合、エラー
		if(StringUtil.isEmpty(form.getContents())) {
			throw new BusinessException(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000005, new String[] {"報告理由"}, null));
		}
		
		Report report = new Report();
		report.setIdeaId(form.getIdeaId());
		report.setCommentId(form.getCommentId());
		report.setContents(form.getContents());
		
		repo.saveAndFlush(report);
		
	}

}
