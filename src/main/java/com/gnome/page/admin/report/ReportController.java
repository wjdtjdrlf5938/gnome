package com.gnome.page.admin.report;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gnome.common.exception.BusinessException;
import com.google.gson.Gson;

@RestController
public class ReportController {
	
	@Autowired
	private ReportService service;
	
	/**
	 * 新規作成
	 * @param form
	 * @return
	 */
	@RequestMapping(path="/reports/create", method = RequestMethod.POST)
	@ResponseBody
	public String create(ReportForm form) {
		
		Gson gson = new Gson();
		
		try {
			//作成処理
			service.create(form);
			
		}catch (BusinessException e) {
			e.printStackTrace();
			form.setErrorMsg(e.getErrors().get(0));
		}
		
		return gson.toJson(form);
	}
}
