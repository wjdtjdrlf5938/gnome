package com.gnome.page.admin.report;

public class ReportForm {
	
	//アイデアid
	private Long ideaId;
	
	//コメントid
	private Long commentId;
	
	//コメントの内容
	private String contents;
	
	// エラーメッセージ
	private String errorMsg;

	public Long getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(Long ideaId) {
		this.ideaId = ideaId;
	}

	public Long getCommentId() {
		return commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
}
