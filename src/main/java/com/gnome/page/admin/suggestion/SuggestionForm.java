package com.gnome.page.admin.suggestion;

import java.util.List;

public class SuggestionForm {
	
	//アイデアid
	private Long ideaId;
	
	//アイデアのURL
	private String url;
	
	//報告内容
	private String contents;
	
	// エラーリスト
	private List<String> errList;

	public Long getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(Long ideaId) {
		this.ideaId = ideaId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public List<String> getErrList() {
		return errList;
	}

	public void setErrList(List<String> errList) {
		this.errList = errList;
	}
}
