package com.gnome.page.admin.suggestion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gnome.common.exception.BusinessException;
import com.google.gson.Gson;

@RestController
public class SuggestionController {
	
	@Autowired
	private SuggestionService service;
	
	/**
	 * マージ提案新規作成
	 * @param form
	 * @return
	 */
	@RequestMapping(path="/suggestons/create", method = RequestMethod.POST)
	@ResponseBody
	public String create(SuggestionForm form) {
		
		Gson gson = new Gson();
		
		try {
			//作成処理
			service.create(form);
			
		}catch (BusinessException e) {
			e.printStackTrace();
			form.setErrList(e.getErrors());
		}
		
		return gson.toJson(form);
	}
}
