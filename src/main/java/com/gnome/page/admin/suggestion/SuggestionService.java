package com.gnome.page.admin.suggestion;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gnome.base.entity.Suggestion;
import com.gnome.base.repository.SuggestionRepository;
import com.gnome.common.consts.MessageIdConsts;
import com.gnome.common.exception.BusinessException;
import com.gnome.common.util.StringUtil;

@Service
public class SuggestionService {
	
	@Autowired
	private SuggestionRepository repo;
	
	@Autowired
	private MessageSource msgSource;
	
	@Transactional(rollbackFor=Exception.class)
	public void create(SuggestionForm form) throws BusinessException{
		
		//バリデーションチェック
		validation(form);
		
		Suggestion suggestion = new Suggestion();
		suggestion.setIdeaId(form.getIdeaId());
		suggestion.setUrl(form.getUrl());
		suggestion.setContents(form.getContents());
		
		repo.saveAndFlush(suggestion);
		
	}
	
	
	/**
	 * バリデーションチェック
	 * @param form
	 * @throws NoSuchMessageException
	 * @throws BusinessException
	 */
	private void validation(SuggestionForm form) throws BusinessException {
		
		List<String> errs = new ArrayList<String>();
		
		//URLがない場合、
		if(StringUtil.isEmpty(form.getUrl())) {
			errs.add(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000005, new String[] {"アイデアのURL"}, null));
		}
		
		//URLのパターンが正しくない場合、
		if(!StringUtil.isEmpty(form.getUrl())) {
			if(!StringUtil.isUrl(form.getUrl())) {
				errs.add(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000005, new String[] {"正しいURL"}, null));
			}
		}
		
		//内容がない場合、
		if(StringUtil.isEmpty(form.getContents())) {
			errs.add(msgSource.getMessage(MessageIdConsts.MESSAGE_ID_E000005, new String[] {"提案理由"}, null));
		}
		
		if (errs.size() > 0) {
			throw new BusinessException(errs);
		}
	}
}
