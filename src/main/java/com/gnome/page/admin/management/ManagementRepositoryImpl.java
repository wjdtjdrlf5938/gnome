package com.gnome.page.admin.management;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import com.gnome.common.util.StringUtil;


@Repository
public class ManagementRepositoryImpl implements ManagementRepositoryCustom{
	
	@Autowired
	private EntityManager entityManager;
	
	@Override
	public Page<ManagementEntity> findIdea(ManagementForm form, Long spaceId, Pageable pageable) {
		
		String countSql = " SELECT count(*) FROM developments ";
		
		String sql = " SELECT "
				 + " developments.idea_id,"
				 + " developments.expected_on,"
				 + " developments.released_on,"
				 + " statuses.name AS status_name,"
				 + " ideas.subject,"
				 + " all_votes.vote_count,"
				 + " up_votes.up_count,"
				 + " ideas.created_at AS idea_created_at"
				 + " FROM developments";
		
		String joinSql = ""
				+ " inner JOIN ideas ON ideas.id = developments.idea_id"
				+ " LEFT JOIN statuses ON statuses.id = developments.status_id"
				+ " LEFT JOIN categories sub_category ON sub_category.id = ideas.category_id"
				+ " LEFT JOIN categories parent_category ON parent_category.id = sub_category.parent_id"
				+ " LEFT JOIN (SELECT count(distinct votes.id) AS vote_count, votes.idea_id FROM votes GROUP BY votes.idea_id) all_votes ON all_votes.idea_id = developments.idea_id"
				+ " LEFT JOIN (SELECT count(distinct votes.id) AS up_count, votes.idea_id FROM votes WHERE votes.is_agreed = true GROUP BY votes.idea_id) up_votes ON up_votes.idea_id = developments.idea_id";
		
		String conditionSql = " WHERE 1 = 1 AND ideas.is_temporary = false AND ideas.space_id = :spaceId ";
		
		Map<String, Object> bindParameters = new HashMap<String, Object>();
		
		bindParameters.put("spaceId", spaceId);
		
		if(!StringUtil.isEmpty(form.getSearchContents())) {
			conditionSql += " AND (ideas.subject LIKE :searchContents "
					+ " OR ideas.contents LIKE :searchContents  "
					+ " OR developments.develop_memo LIKE :searchContents )"
					+ " OR developments.idea_id LIKE :searchContents";
			bindParameters.put("searchContents", "%" + form.getSearchContents() + "%");
		}
		
		if(!StringUtil.isEmpty(form.getParentCategoryId())) {
			conditionSql += " AND parent_category.id = :parentCategoryId";
			bindParameters.put("parentCategoryId", form.getParentCategoryId());
		};
		
		if(!StringUtil.isEmpty(form.getCategoryId())) {
			conditionSql += " AND sub_category.id = :subCategoryId";
			bindParameters.put("subCategoryId", form.getCategoryId());
		};
		
		if(!StringUtil.isEmpty(form.getStatusId())) {
			conditionSql += " AND statuses.id = :statusId ";
			bindParameters.put("statusId", form.getStatusId());
		};
		
		if(!StringUtil.isEmpty(form.getIdeaFrom())) {
			conditionSql += " AND DATE_FORMAT(ideas.created_at,'%Y/%m/%d') >= :ideaFrom";
			bindParameters.put("ideaFrom", form.getIdeaFrom());
		}
		
		if(!StringUtil.isEmpty(form.getIdeaFrom())) {
			conditionSql += " AND DATE_FORMAT(ideas.created_at,'%Y/%m/%d') <= :ideaTo ";
			bindParameters.put("ideaTo", form.getIdeaTo());
		}
		
		if(!StringUtil.isEmpty(form.getExpecteFrom())) {
			conditionSql += " AND DATE_FORMAT(developments.expected_on, '%Y/%m/%d' ) >= :expercteFrom";
			bindParameters.put("expercteFrom", form.getExpecteFrom());
		}
		
		if(!StringUtil.isEmpty(form.getExpecteTo())) {
			conditionSql += " AND DATE_FORMAT(developments.expected_on, '%Y/%m/%d' ) <= :expercteTo";
			bindParameters.put("expercteTo", form.getExpecteTo());
		}
		
		if(!StringUtil.isEmpty(form.getReleaseFrom())) {
			conditionSql += " AND DATE_FORMAT(developments.released_on, '%Y/%m/%d' ) >= :releaseFrom";
			bindParameters.put("releaseFrom", form.getReleaseFrom());
		}
		
		if(!StringUtil.isEmpty(form.getReleaseTo())) {
			conditionSql += " AND DATE_FORMAT(developments.released_on, '%Y/%m/%d' ) <= :releaseTo";
			bindParameters.put("releaseTo", form.getReleaseTo());
		}
		
		Query findQuery =  entityManager.createNativeQuery(sql + joinSql + conditionSql, ManagementEntity.class);
		Query countQuery = entityManager.createNativeQuery(countSql + joinSql + conditionSql);
		
		for (Map.Entry<String, Object> param : bindParameters.entrySet()) {
			findQuery.setParameter(param.getKey(), param.getValue());
			countQuery.setParameter(param.getKey(), param.getValue());
		}
		
		int total = ((Number) countQuery.getSingleResult()).intValue();
		
		@SuppressWarnings("unchecked")
		List<ManagementEntity> ideas = findQuery
				.setFirstResult(pageable.getPageNumber()*pageable.getPageSize())
				.setMaxResults(pageable.getPageSize())
				.getResultList();
		
		return new PageImpl<ManagementEntity>(ideas, pageable, total);
	}
}
