package com.gnome.page.admin.management;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ManagementRepositoryCustom {
	
	public Page<ManagementEntity> findIdea(ManagementForm form, Long spaceId, Pageable pageable);

}
