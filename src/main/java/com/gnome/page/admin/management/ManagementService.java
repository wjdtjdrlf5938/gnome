package com.gnome.page.admin.management;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gnome.auth.LoginUser;
import com.gnome.base.entity.Idea;
import com.gnome.base.repository.IdeaRepository;
import com.gnome.common.util.StringUtil;

@Service
public class ManagementService {
	
	@Autowired
	private ManagementRepositoryCustom customRepo;
	
	@Autowired
	private IdeaRepository ideaRepo;
	
	/**
	 * 検索処理
	 * 
	 * @param form
	 */
	@Transactional(rollbackFor=Exception.class)
	public void search(ManagementForm form) {

		// スペースID
		Long spaceId = ((LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getLoginUser().getSpaceId();
		
		Pageable pageable = PageRequest.of(form.getPageOfInt(), form.getRecordsOfInt()); Page<ManagementEntity> idealist = customRepo.findIdea(form, spaceId, pageable);
		
		form.setFirst(idealist.isFirst());
		form.setLast(idealist.isLast());
		form.setNumber(idealist.getNumber());
		form.setTotalPages(idealist.getTotalPages());
		
		//検索結果
		form.setIdeaList(idealist.getContent());
	}
	
	/**
	 * 削除処理
	 * 
	 * @param form
	 */
	@Transactional(rollbackFor=Exception.class)
	public void delete(ManagementForm form) {
		
		List<Long> ids = new ArrayList<Long>();
		
		for (String id : form.getOperationTarget()) {
			ids.add(StringUtil.toLong(id));
		}
		
		// 削除対象のアイデアにマージされていたアイデアも取得
		List<Idea> mergedIdeas = ideaRepo.findByMergedIdIn(ids);
		
		for (Idea idea : mergedIdeas) {
			ids.add(idea.getId());
		}
		
		// 削除実行
		ideaRepo.deleteByIdIn(ids);
	}
}
