package com.gnome.page.admin.management;

import java.util.List;

import com.gnome.base.common.BaseForm;

public class ManagementForm extends BaseForm{
	
	//検索内容
	private String searchContents;
	
	//親カテゴリーid
	private Long parentCategoryId;
	
	//子カテゴリーid
	private Long categoryId;
	
	//検索カテゴリー名
	private String categoryName;

	// ステータス
	private Long statusId;
	
	// 投稿開始日
	private String ideaFrom;
	
	// 投稿終了日
	private String ideaTo;
	
	// 完了予定開始日
	private String expecteFrom;
	
	// 完了予定終了日
	private String expecteTo;
	
	// 完了開始日
	private String releaseFrom;
	
	// 完了終了日
	private String releaseTo;
	
	//アイディアリスト(検索結果)
	private List<ManagementEntity> ideaList;
	
	// 操作対象
	private String[] operationTarget;
	
	// 詳細表示
	private boolean isSearchDtlDisp;

	public String getSearchContents() {
		return searchContents;
	}

	public void setSearchContents(String searchContents) {
		this.searchContents = searchContents;
	}

	public Long getParentCategoryId() {
		return parentCategoryId;
	}

	public void setParentCategoryId(Long parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public String getIdeaFrom() {
		return ideaFrom;
	}

	public void setIdeaFrom(String ideaFrom) {
		this.ideaFrom = ideaFrom;
	}

	public String getIdeaTo() {
		return ideaTo;
	}

	public void setIdeaTo(String ideaTo) {
		this.ideaTo = ideaTo;
	}

	public String getExpecteFrom() {
		return expecteFrom;
	}

	public void setExpecteFrom(String expecteFrom) {
		this.expecteFrom = expecteFrom;
	}

	public String getExpecteTo() {
		return expecteTo;
	}

	public void setExpecteTo(String expecteTo) {
		this.expecteTo = expecteTo;
	}

	public String getReleaseFrom() {
		return releaseFrom;
	}

	public void setReleaseFrom(String releaseFrom) {
		this.releaseFrom = releaseFrom;
	}

	public String getReleaseTo() {
		return releaseTo;
	}

	public void setReleaseTo(String releaseTo) {
		this.releaseTo = releaseTo;
	}

	public List<ManagementEntity> getIdeaList() {
		return ideaList;
	}

	public void setIdeaList(List<ManagementEntity> ideaList) {
		this.ideaList = ideaList;
	}

	public String[] getOperationTarget() {
		return operationTarget;
	}

	public void setOperationTarget(String[] operationTarget) {
		this.operationTarget = operationTarget;
	}

	public boolean isSearchDtlDisp() {
		return isSearchDtlDisp;
	}

	public void setSearchDtlDisp(boolean isSearchDtlDisp) {
		this.isSearchDtlDisp = isSearchDtlDisp;
	}
}