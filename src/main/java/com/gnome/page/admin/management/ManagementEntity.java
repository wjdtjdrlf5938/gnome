package com.gnome.page.admin.management;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ManagementEntity{
	
	@Id
	//開発状況に紐づいているアイデアのid
	private Long ideaId;
	
	//リリース予定日
	private Date expectedOn;
	
	//リリース日
	private Date releasedOn;
	
	//検索されたアイデアのタイトル
	private String subject;
	
	//検索されたアイデアのステータス名
	private String statusName;
	
	//検索されたアイデアの投票数
	private Long voteCount;
	
	//検索されたアイデアの賛成投票数
	private Long upCount;
	
	//検索されたアイデアの作成日
	private Timestamp ideaCreatedAt;

	public Long getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(Long ideaId) {
		this.ideaId = ideaId;
	}

	public Date getExpectedOn() {
		return expectedOn;
	}

	public void setExpectedOn(Date expectedOn) {
		this.expectedOn = expectedOn;
	}

	public Date getReleasedOn() {
		return releasedOn;
	}

	public void setReleasedOn(Date releasedOn) {
		this.releasedOn = releasedOn;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public Long getVoteCount() {
		return voteCount;
	}

	public void setVoteCount(Long voteCount) {
		this.voteCount = voteCount;
	}

	public Long getUpCount() {
		return upCount;
	}

	public void setUpCount(Long upCount) {
		this.upCount = upCount;
	}

	public Timestamp getIdeaCreatedAt() {
		return ideaCreatedAt;
	}

	public void setIdeaCreatedAt(Timestamp ideaCreatedAt) {
		this.ideaCreatedAt = ideaCreatedAt;
	}
}
