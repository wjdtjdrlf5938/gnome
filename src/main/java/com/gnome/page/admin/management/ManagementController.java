package com.gnome.page.admin.management;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gnome.common.consts.CodeConsts;
import com.gnome.common.consts.MessageIdConsts;

@Controller
public class ManagementController {
	
	@Autowired
	private ManagementService service;
	
	@Autowired
	private MessageSource msgSource;
	
	/**
	 * 検索処理
	 * 
	 * @param form
	 * @return
	 */
	@RequestMapping(value = "/auth/management")
    public String index(ManagementForm form, BindingResult errors) {
		
		// バリデーションチェックエラーがある場合
		if (errors.hasErrors()) {
			return "admin/management";
		}
			
		service.search(form);
			
		return "admin/management";

	}
	
	/**
	 * 削除処理
	 * 
	 * @param form
	 * @param attributes
	 * @return
	 */
	@RequestMapping(value = "/auth/management/delete")
    public String delete(ManagementForm form, RedirectAttributes attributes) {
		
		service.delete(form);
			
		attributes.addFlashAttribute(CodeConsts.MESSAGEKBN_SUCCESS, msgSource.getMessage(MessageIdConsts.MESSAGE_ID_I000002, new String[] {"アイデア"}, null));
		
		return "redirect:/auth/management";

	}
}
