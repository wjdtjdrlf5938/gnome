package com.gnome.common.exception;

import java.util.ArrayList;
import java.util.List;

/**
 * 業務例外
 *
 */
public class BusinessException  extends Exception{

	private static final long serialVersionUID = 1L;
	
	private List<String> errors;

	public BusinessException() {
	}

	public BusinessException(String msg) {
	    errors = new ArrayList<String>();
	    errors.add(msg);
	}

	public BusinessException(List<String> errs) {
	    errors = new ArrayList<String>();
	    errors.addAll(errs);
	}
	
	public List<String> getErrors(){
	    return this.errors;
	}

}
