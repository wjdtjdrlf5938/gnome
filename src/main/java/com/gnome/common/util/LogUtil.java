package com.gnome.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ログ出力 ユーティリティークラス
 *
 */
public class LogUtil {

	protected final static Logger logger = LoggerFactory.getLogger(LogUtil.class);
    public static void info(String msg) {
        logger.info(msg);
    }
    public static void warn(String msg) {
        logger.warn(msg);
    }
    public static void error(String msg) {
        logger.error(msg);
    }
}
