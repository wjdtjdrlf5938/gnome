package com.gnome.common.util;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 文字ユーティリティークラス
 *
 */
public class StringUtil {

	/**
	 * 入力チェック
	 *
	 * @param str チェックを行う文字列
	 * @return true：空白
	 */
	public static boolean isEmpty(Object str){
		if(str == null || ("").equals(str)){
			return true;
		}
		return false;
	}
	

	/**
	 * StringからIntegerに変換
	 * 
	 * @param val
	 * @return int型数値
	 */
	public static Integer toInteger(String val){
		if(isEmpty(val)){
			return 0;
		}
		Integer dec = null;
		try{
			dec = Integer.parseInt(val);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return dec;
	}
	
	/**
	 * Stringからlongに変換
	 * 
	 * @param val
	 * @return long型数値
	 */
	public static Long toLong(String val){
		if(isEmpty(val)){
			return null;
		}
		return Long.valueOf(val);
	}
	
	
	/**
	 * メールアドレス形式として正しいかチェックする
	 * 
	 * @param mail
	 * @return true 正しい形式
	 * 		   false 誤った形式
	 */
	public static boolean isMailAddr(String mail) {
		String mailFormat = "^[a-zA-Z0-9!#$%&'_`/=~\\*\\+\\-\\?\\^\\{\\|\\}]+(\\.[a-zA-Z0-9!#$%&'_`/=~\\*\\+\\-\\?\\^\\{\\|\\}]+)*+(.*)@[a-zA-Z0-9][a-zA-Z0-9\\-]*(\\.[a-zA-Z0-9\\-]+)+$";
		if (!mail.matches(mailFormat)) {
		    return false;
		}
		return true;
	}
	
	/**
	 * URL形式として正しいかチェックする
	 * 
	 * @param url
	 * @return true 正しい形式
	 * 		   false 誤った形式
	 */
	public static boolean isUrl(String url) {
		
		Pattern p = Pattern.compile("(https?://[^/]+/)");
		Matcher m = p.matcher(url);
		
		return m.find();
	}
	
	/**
	 * 文字列からjava.sql.Timestamp型に変換します
	 *
	 * @param String型の日付 yyyyMMdd
	 * @return java.sql.Timestamp
	 * @throws ParseException
	 */
	public static Timestamp ToTimestampOnFormat(String stringDate, String format) throws ParseException {

		return new Timestamp(new SimpleDateFormat(format).parse(stringDate).getTime());
	}
	
	/**
	 * 文字列からjava.sql.Timestamp型に変換します
	 *
	 * @param String型の日付 yyyyMMdd
	 * @return java.sql.Timestamp
	 * @throws ParseException
	 */
	public static Timestamp mergeToTimestamp(String stringDate, String stringTime) throws ParseException {
		
		String date = stringDate + " " + stringTime;
		
		return new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(date).getTime());
	}

		
	 /* 
	 * 文字型の日付と時刻からtimestamp型に変換します
	 * 
	 * @param date
	 * @param time
	 * @return
	 * @throws ParseException
	 */
	public static Timestamp ToTimestamp(String date, String time) throws ParseException {
		return new Timestamp(new SimpleDateFormat("yyyyMMddHHmm").parse(date+time).getTime());
	}
	
	/**
	 * 文字型の日付と時刻からDate型に変換します
	 * @param stringDate
	 * @param format
	 * @return
	 * @throws ParseException
	 */
	public static Date toDate(String stringDate) throws ParseException {
		
		if (StringUtil.isEmpty(stringDate)) {
			return null;
		}
		return new Date(new SimpleDateFormat("yyyy/MM/dd").parse(stringDate).getTime());
	}
}
