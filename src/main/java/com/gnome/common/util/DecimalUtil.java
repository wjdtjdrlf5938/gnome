package com.gnome.common.util;

/**
 * 数値ユーティリティークラス
 *
 */
public class DecimalUtil {

	/**
	 * 入力チェック
	 *
	 * @param val チェックを行う数値
	 * @return true：空白
	 */
	public static boolean isEmpty(Long val){
		if(val == null || val < 1){
			return true;
		}
		return false;
	}
	
	/**
	 * LongからStringに変換する
	 * 
	 * @param val
	 * @return
	 */
	public static String toString(Long val) {
		if (val == null) {
			return "";
		}
		
		return String.valueOf(val);
	}
}
