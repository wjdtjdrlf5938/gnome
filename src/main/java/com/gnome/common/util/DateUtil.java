package com.gnome.common.util;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * 日付ユーティリティークラス
 *
 */
public class DateUtil {

	/**
	 * timestampを文字に変換する
	 * 
	 * @param tm
	 * @param format
	 * @return
	 */
	public static String convertTimestampToString(Timestamp tm, String format) {
		if (tm == null) {
			return null;
		}

		DateFormat fmt = new SimpleDateFormat(format);
		return fmt.format(tm);
	}
	
	/**
	 * Dateを文字に変換する
	 * @param date
	 * @param format
	 * @return
	 */
	public static String convertDateToString(Date date, String format) {
		if (date == null) {
			return null;
		}
		
		DateFormat fmt = new SimpleDateFormat(format);
		return fmt.format(date);
	}
}
