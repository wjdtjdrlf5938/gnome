package com.gnome.common.dialect;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.IExpressionContext;
import org.thymeleaf.dialect.IExpressionObjectDialect;
import org.thymeleaf.expression.IExpressionObjectFactory;

@Component
public class SelectListDialect implements IExpressionObjectDialect {

	// Thymeleafで使用する名前
    private static final String SELECT_LIST_EXPRESSION_NAME = "SelectList";
    
    // 名前管理するSet
    private Set<String> allExpressionNames = new HashSet<String>();
    
    // 実装クラス
    @Autowired
    private SelectList selectList;
    
    @Override
    public IExpressionObjectFactory getExpressionObjectFactory() {
        return new IExpressionObjectFactory() {
            @Override
            public Set<String> getAllExpressionObjectNames() {
            	allExpressionNames.add(SELECT_LIST_EXPRESSION_NAME);
                
                return allExpressionNames;
            }

            @Override
            public Object buildObject(IExpressionContext context, String expressionObjectName) {
                // 独自Utilityのインスタンスと名前を紐付け
                if(expressionObjectName.equals(SELECT_LIST_EXPRESSION_NAME)){
                    return selectList;
                }
                return null;
            }

            @Override
            public boolean isCacheable(String expressionObjectName) {
                // 必要に応じて実装
                return false;
            }
        };
    }

    @Override
    public String getName() {
        return "SelectList";
    }
}
