package com.gnome.common.dialect;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

import com.gnome.base.entity.Category;
import com.gnome.base.entity.Status;
import com.gnome.base.entity.User;
import com.gnome.base.repository.CategoryRepository;
import com.gnome.base.repository.StatusRepository;
import com.gnome.base.repository.UserRepository;

/**
 * タイムリーフのコンボボックスに設定する値を取得
 *
 */
@Component
public final class SelectList {
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private CategoryRepository categoryRepo;
	
	@Autowired
	private StatusRepository statusRepo;
	
	/**
	 * ユーザー一覧を取得する
	 * 
	 * @return
	 */
	public List<User> getUsers() {
		return userRepo.findAll();
	}
	
	/**
	 * カテゴリー一覧を取得する
	 * 
	 * @return
	 */
	//親カテゴリー一覧取得
	public List<Category> getParentsCatrgory() {
		return categoryRepo.findByParentIdOrderByOrder(0l);
	}
	
	//子カテゴリー一覧取得
	public List<Category> getSubCatrgory() {
		return categoryRepo.findByParentIdNotOrderByOrder(0l);
	}
	
	
	/**
	 * ステータス一覧を取得する
	 * 
	 * @return
	 */
	//ステータス一覧取得
	public List<Status> getStatus() {
		return statusRepo.findAll(Sort.by(Direction.DESC, "isInitial").and(Sort.by("order")));
	}
}
