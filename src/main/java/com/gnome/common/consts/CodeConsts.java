package com.gnome.common.consts;

/**
 * コード・区分 定数クラス
 *
 */
public class CodeConsts {

	/**	メッセージ区分(成功) */
	public static final String MESSAGEKBN_SUCCESS = "successMessage";
	/**	メッセージ区分(ワーニング) */
	public static final String MESSAGEKBN_WARNING = "warningMessage";
	
}
