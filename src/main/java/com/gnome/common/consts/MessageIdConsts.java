package com.gnome.common.consts;

/**
 * メッセージID定数
 *
 */
public class MessageIdConsts {

	// ========================INFOMATION========================
	public static final String MESSAGE_ID_I000001 = "I000001";
	
	public static final String MESSAGE_ID_I000002 = "I000002";
	
	// ===========================ERROR===========================
	public static final String MESSAGE_ID_E000001 = "E000001";
	
	public static final String MESSAGE_ID_E000002 = "E000002";
	
	public static final String MESSAGE_ID_E000003 = "E000003";
	
	public static final String MESSAGE_ID_E000004 = "E000004";
	
	public static final String MESSAGE_ID_E000005 = "E000005";
	
	public static final String MESSAGE_ID_E000006 = "E000006";
	
	public static final String MESSAGE_ID_E000007 = "E000007";
	
	public static final String MESSAGE_ID_E000008 = "E000008";
	
	public static final String MESSAGE_ID_E000009 = "E000009";
	
	public static final String MESSAGE_ID_E000010 = "E000010";
}
