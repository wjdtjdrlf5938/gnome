package com.gnome.base.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gnome.base.entity.Idea;

@Transactional
public interface IdeaRepository extends JpaRepository<Idea, Long>, JpaSpecificationExecutor<Idea>{

	public int countByMergedId(Long mergedId);
	
	public List<Idea> findByIdIn(List<Long> ids);
	
	public List<Idea> findByMergedIdIn(List<Long> ids);
	
	public void deleteByIdIn(List<Long> ids);
}
