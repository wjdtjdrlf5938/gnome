package com.gnome.base.repository;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gnome.base.entity.Favorite;

@Transactional
public interface FavoriteRepository extends JpaRepository<Favorite, Long>, JpaSpecificationExecutor<Favorite>{
	
	Optional<Favorite> findBySpaceIdAndIdeaIdAndCreatedBy(Long spaceId, Long ideaId, Long userId);

}
