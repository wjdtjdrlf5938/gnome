package com.gnome.base.repository;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gnome.base.entity.User;

@Transactional
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

	public User findByEmail(String email);
	
	public Optional<User> findByIdAndSpaceId(Long id, Long spaceId);
	
	public User findByIdNotAndEmail(Long id, String email);
	
}
