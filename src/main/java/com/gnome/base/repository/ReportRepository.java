package com.gnome.base.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gnome.base.entity.Report;

@Transactional
public interface ReportRepository extends JpaRepository<Report, Long>, JpaSpecificationExecutor<Report>{

}
