package com.gnome.base.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gnome.base.entity.Category;

@Transactional
public interface CategoryRepository extends JpaRepository<Category, Long>, JpaSpecificationExecutor<Category>{
	
	public List<Category> findByParentIdOrderByOrder(Long categoryId);
	
	public List<Category> findByParentIdNotOrderByOrder(Long categoryId);

}
