package com.gnome.base.repository;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gnome.base.entity.Status;

@Transactional
public interface StatusRepository extends JpaRepository<Status, Long>, JpaSpecificationExecutor<Status>{
	
	public Optional<Status> findByIsInitial(boolean isFirst);

}
