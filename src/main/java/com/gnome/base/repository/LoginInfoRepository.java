package com.gnome.base.repository;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gnome.base.entity.LoginInfo;

@Transactional
public interface LoginInfoRepository extends JpaRepository<LoginInfo, Long>, JpaSpecificationExecutor<LoginInfo> {
	
	public Page<LoginInfo> findByUserIdOrderByLoginedAtDesc(Long userId, Pageable pageable);
}
