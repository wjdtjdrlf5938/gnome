package com.gnome.base.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gnome.base.entity.Space;

@Transactional
public interface SpaceRepository extends JpaRepository<Space, Long>, JpaSpecificationExecutor<Space>{
}
