package com.gnome.base.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gnome.base.entity.Vote;

@Transactional
public interface VoteRepository extends JpaRepository<Vote, Long>, JpaSpecificationExecutor<Vote>{
	
	public void deleteByIdeaIdAndCreatedBy(Long ideaId, Long createdBy);
	
	public List<Vote> findByIdeaIdIn(List<Long> ids);

	public List<Vote> findByIdeaId(Long id);
}
