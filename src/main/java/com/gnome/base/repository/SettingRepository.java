package com.gnome.base.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gnome.base.entity.Setting;

@Transactional
public interface SettingRepository extends JpaRepository<Setting, Long>, JpaSpecificationExecutor<Setting>{
}
