package com.gnome.base.repository;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gnome.base.entity.Development;

@Transactional
public interface DevelopmentRepository extends JpaRepository<Development, Long>, JpaSpecificationExecutor<Development>{
	
	public Optional<Development> findByIdeaId(Long ideaId);
	
}
