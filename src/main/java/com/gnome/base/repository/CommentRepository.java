package com.gnome.base.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gnome.base.entity.Comment;;

@Transactional
public interface CommentRepository extends JpaRepository<Comment, Long>, JpaSpecificationExecutor<Comment>{
	
	public int countByIdeaId(Long ideaId);

}
