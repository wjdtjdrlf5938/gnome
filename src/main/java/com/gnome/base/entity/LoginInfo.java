package com.gnome.base.entity;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gnome.base.common.BaseEntity;

@Entity
@Table(name="login_infos")
public class LoginInfo extends BaseEntity{

	private static final long serialVersionUID = 1L;
	private Long userId;
	
	private Timestamp loginedAt;
	
	private String ipAddress;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Timestamp getLoginedAt() {
		return loginedAt;
	}

	public void setLoginedAt(Timestamp loginedAt) {
		this.loginedAt = loginedAt;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
}
