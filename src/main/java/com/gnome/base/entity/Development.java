package com.gnome.base.entity;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gnome.base.common.BaseEntity;

@Entity
@Table(name="developments")
public class Development extends BaseEntity{
	
	private static final long serialVersionUID = 1L;
	//開発状況に紐づいているアイデアのid
	private Long ideaId;
	
	//ステータスid
	private Long statusId;
	
	//開発メモ
	private String developMemo;
	
	//リリース予定日
	private Date expectedOn;
	
	//リリース日
	private Date releasedOn;

	public Long getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(Long ideaId) {
		this.ideaId = ideaId;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public String getDevelopMemo() {
		return developMemo;
	}

	public void setDevelopMemo(String developMemo) {
		this.developMemo = developMemo;
	}

	public Date getExpectedOn() {
		return expectedOn;
	}

	public void setExpectedOn(Date expectedOn) {
		this.expectedOn = expectedOn;
	}

	public Date getReleasedOn() {
		return releasedOn;
	}

	public void setReleasedOn(Date releasedOn) {
		this.releasedOn = releasedOn;
	}
}
