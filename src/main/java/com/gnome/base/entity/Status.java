package com.gnome.base.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gnome.base.common.BaseEntity;

import javax.persistence.Column;

@Entity
@Table(name="statuses")
public class Status extends BaseEntity{
	
	private static final long serialVersionUID = 1L;
	// スペースID
	private Long spaceId;
	
	//ステータス名
	private String name;
	
	// 並び順
	@Column(name="`order`")
	private Long order;
	
	//初期ステータス判断
	private boolean isInitial;

	// 完了フラグ
	private boolean isCompleted;
	
	public Long getSpaceId() {
		return spaceId;
	}

	public void setSpaceId(Long spaceId) {
		this.spaceId = spaceId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getOrder() {
		return order;
	}

	public void setOrder(Long order) {
		this.order = order;
	}

	public boolean isInitial() {
		return isInitial;
	}

	public void setInitial(boolean isInitial) {
		this.isInitial = isInitial;
	}

	public boolean isCompleted() {
		return isCompleted;
	}

	public void setCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}
}
