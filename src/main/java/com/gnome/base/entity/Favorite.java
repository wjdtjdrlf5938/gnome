package com.gnome.base.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gnome.base.common.BaseEntity;

@Entity
@Table(name="favorites")
public class Favorite extends BaseEntity{
	
	private static final long serialVersionUID = 1L;
	
	// スペースID
	private Long spaceId;
	
	//アイデアID
	private Long ideaId;

	public Long getSpaceId() {
		return spaceId;
	}

	public void setSpaceId(Long spaceId) {
		this.spaceId = spaceId;
	}

	public Long getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(Long ideaId) {
		this.ideaId = ideaId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
