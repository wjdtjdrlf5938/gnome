package com.gnome.base.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gnome.base.common.BaseEntity;

@Entity
@Table(name="ideas")
public class Idea extends BaseEntity{
	
	private static final long serialVersionUID = 1L;
	// スペースID
	private Long spaceId;
	
	//タイトル
	private String subject;
	
	//内容
	private String contents;
	
	//マージしたアイデアのid
	private Long mergedId;
	
	//カテゴリーid
	private Long categoryId;
	
	//仮登録判断
	private boolean isTemporary;

	public Long getSpaceId() {
		return spaceId;
	}

	public void setSpaceId(Long spaceId) {
		this.spaceId = spaceId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public Long getMergedId() {
		return mergedId;
	}

	public void setMergedId(Long mergedId) {
		this.mergedId = mergedId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public boolean isTemporary() {
		return isTemporary;
	}

	public void setTemporary(boolean isTemporary) {
		this.isTemporary = isTemporary;
	}
}