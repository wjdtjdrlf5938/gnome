package com.gnome.base.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gnome.base.common.BaseEntity;

@Entity
@Table(name="suggestions")
public class Suggestion extends BaseEntity{
	
	private static final long serialVersionUID = 1L;
	//アイデアid
	private Long ideaId;
	
	//アイデアのURL
	private String url;
	
	//報告内容
	private String contents;

	public Long getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(Long ideaId) {
		this.ideaId = ideaId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}
}
