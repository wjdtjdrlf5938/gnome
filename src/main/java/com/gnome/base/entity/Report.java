package com.gnome.base.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gnome.base.common.BaseEntity;

@Entity
@Table(name="reports")
public class Report extends BaseEntity{
	
	private static final long serialVersionUID = 1L;
	//アイデアid
	private Long ideaId;
	
	//コメントid
	private Long commentId;
	
	//報告内容
	private String contents;

	public Long getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(Long ideaId) {
		this.ideaId = ideaId;
	}

	public Long getCommentId() {
		return commentId;
	}

	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}
}
