package com.gnome.base.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gnome.base.common.BaseEntity;

@Entity
@Table(name="settings")
public class Setting extends BaseEntity{
	
	private static final long serialVersionUID = 1L;
	// スペースID
	private Long spaceId;
	
	// ネガティブポイント
	private Long negativePoint;
	
	// ポジティブポイント
	private Long positivePoint;

	public Long getSpaceId() {
		return spaceId;
	}

	public void setSpaceId(Long spaceId) {
		this.spaceId = spaceId;
	}

	public Long getNegativePoint() {
		return negativePoint;
	}

	public void setNegativePoint(Long negativePoint) {
		this.negativePoint = negativePoint;
	}

	public Long getPositivePoint() {
		return positivePoint;
	}

	public void setPositivePoint(Long positivePoint) {
		this.positivePoint = positivePoint;
	}
}