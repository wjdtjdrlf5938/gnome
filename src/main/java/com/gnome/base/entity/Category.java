package com.gnome.base.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.gnome.base.common.BaseEntity;

@Entity
@Table(name="categories")
public class Category extends BaseEntity{
	
	private static final long serialVersionUID = 1L;
	// スペースID
	private Long spaceId;
	
	// カテゴリー名
	private String name;
	
	//親カテゴリーid
	private Long parentId;
	
	// 並び順
	@Column(name="`order`")
	private Long order;

	public Long getSpaceId() {
		return spaceId;
	}

	public void setSpaceId(Long spaceId) {
		this.spaceId = spaceId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Long getOrder() {
		return order;
	}

	public void setOrder(Long order) {
		this.order = order;
	}
}
