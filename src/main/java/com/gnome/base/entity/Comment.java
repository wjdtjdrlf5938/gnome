package com.gnome.base.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gnome.base.common.BaseEntity;

@Entity
@Table(name="comments")
public class Comment extends BaseEntity{
	
	private static final long serialVersionUID = 1L;
	//コメントと紐づいているアイデアのid
	private Long ideaId;
	
	//内容
	private String contents;

	public Long getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(Long ideaId) {
		this.ideaId = ideaId;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}
}
