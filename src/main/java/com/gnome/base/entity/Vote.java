package com.gnome.base.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gnome.base.common.BaseEntity;

@Entity
@Table(name="votes")
public class Vote extends BaseEntity{
	
	private static final long serialVersionUID = 1L;
	
	//投票テーブルに紐づいているアイデアのid
	private Long ideaId;
	
	//投票してユーザーid
	private Long userId;
	
	//賛反判断
	private boolean isAgreed;

	public Long getIdeaId() {
		return ideaId;
	}

	public void setIdeaId(Long ideaId) {
		this.ideaId = ideaId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public boolean isAgreed() {
		return isAgreed;
	}

	public void setAgreed(boolean isAgreed) {
		this.isAgreed = isAgreed;
	}
}
