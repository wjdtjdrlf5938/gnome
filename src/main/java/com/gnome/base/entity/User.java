package com.gnome.base.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.Where;

import com.gnome.base.common.BaseEntity;

@Entity
@Table(name="users")
@Where(clause="IS_DELETED=0")
public class User extends BaseEntity {
	
	private static final long serialVersionUID = 1L;
	// スペースID
	private Long spaceId;
	
	//ユーザー性
	private String lastName;
	
	//ユーザー名
	private String firstName;
	
	//メールアドレス
	private String email;
	
	//パスワード
	private String password;
	
	//仮パスワード
	private String tempPassword;
	
	//管理者判断
	private boolean isAdmin;
	
	//削除判断
	private boolean isDeleted;
	
	//プロフィルイメージurl
	private String profileUrl;
	
	public Long getSpaceId() {
		return spaceId;
	}

	public void setSpaceId(Long spaceId) {
		this.spaceId = spaceId;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTempPassword() {
		return tempPassword;
	}

	public void setTempPassword(String tempPassword) {
		this.tempPassword = tempPassword;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}
}
