package com.gnome.config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.gnome.common.dialect.SelectListDialect;
import com.gnome.common.exception.GlobalExceptionHandler;
import com.gnome.interceptor.PasswordExpirationCheckInterceptor;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;


/**
 * Beanコンフィグ
 *
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

	@Value("${app.cloud.project-id}")
	private String projectId;
	
	@Bean
	public Storage cloudStorage() {
		
		Credentials credentials = null;
		InputStream inputStream = this.getClass().getResourceAsStream("/app/auth/storage.json");
    	
		try {
			credentials = GoogleCredentials.fromStream(inputStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Storage storage = StorageOptions.newBuilder().setCredentials(credentials)
	      .setProjectId(projectId).build().getService();
		return storage;
	}
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
	   return builder.build();
	}

	/**
	 * コントロールを実装しない代わりにURLとテンプレートを紐付ける
	 */
	@Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
    }

	/**
	 * バリデーション情報を登録
	 */
	@Override
    public Validator getValidator() {
        return localValidatorFactoryBean();
    }

	/**
	 * メッセージプロパティを登録
	 * 
	 * @return
	 */
	@Bean
	public MessageSource messageSource() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
	}

	/**
	 * バリデーションエラー時に表示するメッセージ取得場所を登録
	 * 
	 * @return
	 */
	@Bean
	public LocalValidatorFactoryBean localValidatorFactoryBean() {
        LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
        localValidatorFactoryBean.setValidationMessageSource(messageSource());
        return localValidatorFactoryBean;
    }

	/**
	 * 例外発生時の共通処理クラスを登録
	 * 
	 * @return
	 */
	@Bean
	public HandlerExceptionResolver handlerExceptionResolverBean() {
		return new GlobalExceptionHandler();
	}

	/**
	 * タイムリーフ上で使用するダイアレクト情報を登録
	 * 
	 * @return
	 */
	@Bean
    public SelectListDialect selectListDialect() {
        return new SelectListDialect();
    }

	/**
	 * パスワードエンコード情報を登録
	 * 
	 * @return
	 */
	@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

	/**
	 * インターセプター（コントロール共通処理）情報を登録
	 */
	@Override
    public void addInterceptors(InterceptorRegistry registry) {
        
        registry.addInterceptor(new PasswordExpirationCheckInterceptor())
			.addPathPatterns("/**")  // 対象とするパターンを指定する
	        .excludePathPatterns("/login", "/init-pw-change", "/init-pw-change/update"); // 除外するパス(パターン)を指定する
    }
}
