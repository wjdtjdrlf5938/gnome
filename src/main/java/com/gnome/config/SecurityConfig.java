package com.gnome.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.security.web.csrf.MissingCsrfTokenException;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.gnome.auth.LoginServiceImpl;
import com.gnome.auth.SessionExpiredAuthEntryPoint;

/**
 * セキュリティコンフィグ
 *
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	public void configure(WebSecurity web) throws Exception {
		// セキュリティ設定を無視するリクエスト設定
		// 静的モジュールは除外
		web.ignoring().antMatchers(
	                            "/css/**",
	                            "/js/**",
	                            "/img/**");
	}

	@Override
    protected void configure(HttpSecurity http) throws Exception {
		
		// 認可の設定
        http.authorizeRequests()
            .antMatchers("/login","/error", "/login?**").permitAll() // ログイン画面は全ユーザーアクセス許可
            .antMatchers("/css/**").permitAll()
            .antMatchers("/js/**").permitAll()
            .antMatchers("/img/**").permitAll()
            .antMatchers("/favicon.ico").permitAll()
            .antMatchers("/auth/**").hasAnyRole("ADMIN")	//管理画面
            .anyRequest().authenticated()	  // それ以外は全て認証無しの場合アクセス不許可
            .and().rememberMe()			// ログイン状態にする
            .and().exceptionHandling()
            // 通常のRequestとAjaxを両方対応するSessionTimeout用
            .authenticationEntryPoint(authenticationEntryPoint())
            .accessDeniedHandler(accessDeniedHandler());
        
        // ログイン設定
        http.formLogin()
            .loginProcessingUrl("/login")   // 認証処理のパス
            .loginPage("/login")            // ログインフォームのパス
            .successForwardUrl("/")     // 認証成功時の遷移先
            .usernameParameter("loginId").passwordParameter("password");  // ユーザー名、パスワードのパラメータ名
        
        // ログアウト設定
        http.logout()
            .logoutRequestMatcher(new AntPathRequestMatcher("/logout**"))       // ログアウト処理のパス
            .logoutSuccessUrl("/login"); // ログアウト成功時の遷移先
	}

    @Bean
    AccessDeniedHandler accessDeniedHandler() {
        return new AccessDeniedHandler() {
            @Override
            public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
                if (accessDeniedException instanceof MissingCsrfTokenException) {
                    authenticationEntryPoint().commence(request, response, null);
                } else {
                    new AccessDeniedHandlerImpl().handle(request, response, accessDeniedException);
                }
            }
        };
    }
    
	@Bean
    AuthenticationEntryPoint authenticationEntryPoint() {
        return new SessionExpiredAuthEntryPoint("/login");
    }
	 
	@Configuration
    protected static class AuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter {
        @Autowired
        private LoginServiceImpl loginServiceImpl;

        @Override
        public void init(AuthenticationManagerBuilder auth) throws Exception {
            // 認証処理を行うサービスを指定する
            auth.userDetailsService(loginServiceImpl)
            // 入力値をBCryptでハッシュ化した値でパスワード認証を行う
            .passwordEncoder(new BCryptPasswordEncoder());
        }
	}
}
