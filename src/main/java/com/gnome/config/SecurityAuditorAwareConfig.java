package com.gnome.config;

import java.util.Optional;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.gnome.auth.LoginUser;

@EnableJpaAuditing
@Configuration
public class SecurityAuditorAwareConfig {

	@Bean
    public AuditorAware<Long> auditorAware()
    {
        return new SecurityAuditorAware();
    }
	
	public static class SecurityAuditorAware implements AuditorAware<Long>
    {
		@Override
        public Optional<Long> getCurrentAuditor()
        {
            Authentication authentication =
                    SecurityContextHolder.getContext().getAuthentication();
            if (authentication == null)
            {
                return Optional.ofNullable(0L);
            }
            LoginUser account = (LoginUser) authentication.getPrincipal();

            return Optional.ofNullable(account.getLoginUser().getId());
        }
    }
}
