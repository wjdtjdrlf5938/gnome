package com.gnome.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import com.gnome.auth.LoginUser;

@Component
public class PasswordExpirationCheckInterceptor implements HandlerInterceptor {
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		// 静的リソースは除外する
		if (handler instanceof ResourceHttpRequestHandler) {
            return true;
		}
		
		// ログイン情報からパスワードを取得
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		if (principal != null && ((LoginUser)principal).isTempPassword()) {
			response.sendRedirect(request.getContextPath() + "/init-pw-change");
			return false;
		}

		return true;
	}
}
