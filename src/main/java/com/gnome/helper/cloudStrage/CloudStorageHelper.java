package com.gnome.helper.cloudStrage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.cloud.storage.Acl;
import com.google.cloud.storage.Acl.Role;
import com.google.cloud.storage.Acl.User;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;

@Service
public class CloudStorageHelper {
	
	@Autowired
	private Storage cloudStorage;
	
	//バケット名指定
	@Value("${app.cloud.bucket-name}")
	private String bucketName;
		
	// [START uploadFile]
	/**
	 * BUCKET_NAMEで指定されたgoogle cloud storageバケットにファイルをアップロードします
	 * 
	 * @param filePart
	 * @param bucketName
	 * @return
	 * @throws IOException
	 */
	public String uploadFile(MultipartFile filePart, String fileName) throws IOException {

		InputStream is = filePart.getInputStream();
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		byte[] readBuf = new byte[4096];
		while (is.available() > 0) {
			int bytesRead = is.read(readBuf);
			os.write(readBuf, 0, bytesRead);
		}
		
		BlobInfo blobInfo =
				cloudStorage.create(
					BlobInfo
					.newBuilder(bucketName, fileName)
					// Modify access list to allow all users with link to read file
					.setAcl(new ArrayList<>(Arrays.asList(Acl.of(User.ofAllUsers(), Role.READER))))
					.build(),
				os.toByteArray());
		return blobInfo.getMediaLink();
	}
	// [END uploadFile]

	// [START getImageUrl]
	/**
	 * Extracts the file payload from an HttpServletRequest, checks that the file extension
	 * is supported and uploads the file to Google Cloud Storage.
	 */
	public String getImageUrl(HttpServletRequest req, HttpServletResponse resp,
			final String bucket) throws IOException, ServletException {
		
		Part filePart = req.getPart("file");
		final String fileName = filePart.getSubmittedFileName();
		String imageUrl = req.getParameter("imageUrl");
		// Check extension of file
		if (fileName != null && !fileName.isEmpty() && fileName.contains(".")) {
			final String extension = fileName.substring(fileName.lastIndexOf('.') + 1);
			String[] allowedExt = {"jpg", "jpeg", "png", "gif"};
			for (String s : allowedExt) {
				if (extension.equals(s)) {
					//Part -> MultipartFile
					//return this.uploadFile(filePart, bucket);
				}
			}
			throw new ServletException("file must be an image");
		}
		return imageUrl;
	}
	// [END getImageUrl]
	
	// [START deleteFile]
	/**
	* BUCKET_NAMEで指定されたgoogle cloud storageバケットからファイルを削除します
	* 
	* @param filePart
	* @param bucketName
	* @return
	* @throws IOException
	*/
//	public void deleteFiles(List<MailAttachment> list) {
//		List<BlobId> blobIds = new LinkedList<>();
//		for (MailAttachment entity : list) {
//			blobIds.add(BlobId.of(bucketName, entity.getCategory() + "/" + entity.getFileName()));
//		}
//		cloudStorage.delete(blobIds);
//	}
	// [END deleteFile]	  
}
