package com.gnome.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

@Component
public class AuthSuccessEventListener {

	@Autowired
	private AuthEventSharedService service;
	
	@EventListener
	public void onApplicationEvent(AuthenticationSuccessEvent event) {
		
		// ログイン情報
		LoginUser loginUser = (LoginUser) event.getAuthentication().getPrincipal();
		
		// ログイン情報詳細
		Authentication auth = event.getAuthentication();
        WebAuthenticationDetails details = (WebAuthenticationDetails) auth.getDetails();
        
		service.authenticationSuccess(loginUser.getLoginUser(), details);
	}
}
