package com.gnome.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.gnome.base.entity.User;
import com.gnome.base.repository.UserRepository;


/**
 * ログイン Service
 *
 */
@Component
public class LoginServiceImpl implements UserDetailsService {
	
	@Autowired
	private UserRepository repository;
	
	/**
	 * ログイン情報を取得する。
	 * 
	 */
	@Override
    public UserDetails loadUserByUsername(String loginId)
            throws UsernameNotFoundException {

        // 認証を行うユーザー情報を格納する
		User user = null;

        try {
        	// 社員マスタ
        	user = repository.findByEmail(loginId);
        } catch (Exception e) {
            // 取得時にExceptionが発生した場合
            throw new UsernameNotFoundException("It can not be acquired User");
        }

        // ユーザー情報が取得できたらSpring Securityで認証できる形で戻す
        return new LoginUser(user);
    }
}
