package com.gnome.auth;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Service;

import com.gnome.base.entity.LoginInfo;
import com.gnome.base.entity.User;
import com.gnome.base.repository.LoginInfoRepository;

@Service
public class AuthEventSharedService {

	@Autowired
	private LoginInfoRepository repository;
	
	public void authenticationSuccess(User user, WebAuthenticationDetails details) {
		Date date = new Date();
		LoginInfo loginInfo = new LoginInfo();
		
		loginInfo.setLoginedAt(new Timestamp(date.getTime()));
		loginInfo.setUserId(user.getId());
		loginInfo.setIpAddress(details.getRemoteAddress());
		
		repository.save(loginInfo);
	}
}
