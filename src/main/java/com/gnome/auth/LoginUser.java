/*******************************************************************************
* モジュールＩＤ：login
* モジュール名称：ログイン
* 機能概要 ：ログイン
* 
* 改訂履歴 ：2018/09/09 新規 H.Kohata
* 
* Copyright(C) 2019 Sodech Corporation. All Rights Reserved.
********************************************************************************/
package com.gnome.auth;

import org.springframework.security.core.authority.AuthorityUtils;

import com.gnome.base.entity.User;
import com.gnome.common.util.StringUtil;

/**
 * ログインユーザ情報
 *
 */
public class LoginUser extends org.springframework.security.core.userdetails.User{

	private static final long serialVersionUID = 1L;
	
	private final com.gnome.base.entity.User loginUser;
	
	private boolean isTempPassword;
	
	/**
	 * コンストラクタ
	 * 
	 * @param user
	 */
	public LoginUser(com.gnome.base.entity.User loginUser) {
		// スーパークラスのユーザーID、パスワードに値をセットする
        // 実際の認証はスーパークラスのユーザーID、パスワードで行われる
		super(loginUser.getEmail(), StringUtil.isEmpty(loginUser.getTempPassword()) ? loginUser.getPassword() : loginUser.getTempPassword(),
				loginUser.isAdmin() ? AuthorityUtils.createAuthorityList("ROLE_ADMIN") : AuthorityUtils.createAuthorityList("ROLE_GENERAL"));
		
        this.loginUser = loginUser;
        this.isTempPassword = !StringUtil.isEmpty(loginUser.getTempPassword());
    }

	/**
	 * ログインユーザ情報を取得する
	 * 
	 * @return
	 */
	public User getLoginUser() {
        return loginUser;
    }

	public boolean isTempPassword() {
		return isTempPassword;
	}

	public void setTempPassword(boolean isTempPassword) {
		this.isTempPassword = isTempPassword;
	}
}
