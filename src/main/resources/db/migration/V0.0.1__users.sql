CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `space_id` bigint(20) unsigned NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `temp_password` varchar(100) DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `profile_url` varchar(2048) DEFAULT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  CONSTRAINT `fk_users_space_id` FOREIGN KEY (`space_id`) REFERENCES  `spaces` (`id`) ON DELETE CASCADE
);

insert into users (space_id, last_name,first_name,email,password,is_admin, profile_url) values
(1,'山田','花子','test@sodech.com','$2y$10$dYMN/JLLv.pgPhrmu3MSjetSMuk82I60k9PZLeGx66CtWQM/ku7Ki',1,'https://avex.jp/aaa/assets/images/update/slide7.jpg');