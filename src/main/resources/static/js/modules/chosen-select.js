$(document).ready(function () {
  init_chosen();
});

function init_chosen() {
	$(".chosen-status").chosen({
		placeholder_text_single : "ステータス",
		width: "200px",
		allow_single_deselect: true,
		disable_search_threshold: 10
	});
	
	$(".chosen-clients").chosen({
		placeholder_text_single : "掲載店",
		width: "100%",
		allow_single_deselect: true
	});
	
	$(".chosen-edit").chosen({
		width: "100%"
	});
	
	$(".status").val($('#status').val()).trigger("chosen:updated");
	$(".clientId").val($('#clientId').val()).trigger("chosen:updated");
};