$(document).ready(function (){
	
	$('.idea-contens').summernote({
		height: 250,
		disableResizeEditor: true,
		fontNames: ["Yu Gothic", "SimSun", "Meiryo","sans-serif", "Arial","Arial Black","Courier New","Helvetica","Impact","Tahoma","Verdana"],
		lang: 'ja-JP',
		maximumImageFileSize: 1000*1024,
		toolbar: [
			['font', ['bold', 'underline', 'clear']],
			['fontname', ['fontname']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['table', ['table']],
			['insert', ['link', 'picture']],
			['view', ['fullscreen', 'codeview']]
		  ],
		  callbacks: {
                //画像がアップロードされた時の動作
                onImageUpload: function(files) {
                    sendFile(files);
                }
            }
	}).on("summernote.enter", function(we, e) {
	    $(this).summernote("pasteHTML", "<br>&#8203;");
	    e.preventDefault();
	});
	
	function sendFile(files){
		const url = $('#contextPath').val() + "file-upload";
		
		let datas = new FormData();
		
		for (let idx = 0; idx < files.length; idx++) {
			if (files[idx].size >= 1048576) {
			errorToastr('ファイルサイズが大きすぎます。');
			return;
			}
			if (!checkExt(files[idx].name)) {
			  errorToastr('jpg, jpeg, png, gif 以外のファイルはアップロードできません。');
			  return;
			}
			datas.append("uploadFiles", files[idx]);
		}
		
	    $.ajax({
			data: datas,
			enctype: "multipart/form-data",
			type: "POST",
			url: url,
			contentType: false,
			processData: false,
			beforeSend: function (xhr, settings) {
				setCsrfTokenToAjaxHeader();
			},
	        success : function(data, status, xhr) {
	        	const datas = JSON.parse(data);

	        	for (idx in datas) {
	    			const img = datas[idx];
	        		$('.idea-contens').summernote('insertImage',img);
	    		}
			}
		});
	}
});

function checkExt(filename)
{
	const allow_exts = new Array('jpg', 'jpeg', 'png', 'gif');
	const ext = getExt(filename).toLowerCase();
	if (allow_exts.indexOf(ext) === -1) return false;
	return true;
}
function getExt(filename)
{
	const pos = filename.lastIndexOf('.');
	if (pos === -1) return '';
	return filename.slice(pos + 1);
}
