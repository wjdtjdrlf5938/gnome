jQuery(function($){ 
	// デフォルトの設定を変更
    $.extend( $.fn.dataTable.defaults, {
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Japanese.json"
        }
    });
    
    $('#datatable-basic').DataTable({
	  	  info: false,
	      lengthChange: false,
	      searching: false,
	      ordering: true,
	      paging: false,
	      order: [],
	      fixedHeader: false,
	      columnDefs: [ 
	    	  {"targets": [0],"orderable": false}
		  ]
	});
    
    $('.datatable-detail').DataTable({
	  	  info: false,
	      lengthChange: false,
	      searching: false,
	      ordering: true,
	      paging: false,
	      order: [],
	      fixedHeader: false
	});
});