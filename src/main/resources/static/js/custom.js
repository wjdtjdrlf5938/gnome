$(document).ready(function () {
  init_height(),init_calender();
});

window.onresize = init_height;

function init_height() {
	const windowHeight = window.innerHeight;
	$('.window-height').css({ "height": windowHeight-120 + "px", "overflow-y": "auto"});
	$('.window-rt-height').css({ "height": windowHeight-180 + "px", "overflow-y": "auto"});
}

function init_calender() {
  $('.datepicker').datetimepicker({
	  format: 'L',
	  language: 'ja',
	  minViewMode: 0,
	  format: 'YYYY/MM/DD',
	  useCurrent: true
  });
}

$('#check-all').change(function(){
  let checkVal = false;
  const selectedArea = $('.selected-col');
  const selectedRowArea = document.getElementById('selected-row-count');
	  
  selectedArea.hide();
  
  if($(this)[0].checked){
    checkVal = true;
  	selectedRowArea.innerHTML = '選択した行：' + $('.checkbox-records').length + '行';
  	selectedArea.show();
  }

  $('.checkbox-records').each(function(index, element){
    element.checked=checkVal;
    $(this).closest('tr')[checkVal ? 'addClass' : 'removeClass']('tr-seleced');
  });
});

$('.checkbox-records').change(function(){
	
  const selectedArea = $('.selected-col');
  const selectedRowArea = document.getElementById('selected-row-count');
  const count = $('.checkbox-records:checked').length;
	  
  if (count > 0) {
    selectedRowArea.innerHTML = '選択した行：' + count + '行';
    selectedArea.show();
  } else {
    selectedArea.hide();
  }
});

window.onload = function() {
  const successMsg = $("#successMessage").val();
  if (successMsg) {
      successToastr(successMsg);
  }
  
 $(".err-msg").each(function(j, obj) {
	  errorToastr($(obj).val());
  });
}

function successToastr(msg) {
    toastr.options = {
      "closeButton": true,
      "positionClass": "toast-bottom-center",
      "timeOut": "5000"
    };
    toastr.success(msg);
}

function errorToastr(msg) {
  toastr.options = {
    "closeButton": true,
    "positionClass": "toast-bottom-center",
    "showDuration": "0",
    "hideDuration": "0",
    "timeOut": "0",
    "extendedTimeOut": "0"
  }
  toastr.error(msg);
}

function submitAction(url) {
  $('form').attr('action', url);
  $('form').submit();
}

function isMobile(){
  const regexp = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i;
  return (window.navigator.userAgent.search(regexp) !== -1);
}

function setCsrfTokenToAjaxHeader() {
  const token = $("meta[name='_csrf']").attr("content");
  const header = $("meta[name='_csrf_header']").attr("content");
  $(document).ajaxSend(function (e, xhr, options) {
    xhr.setRequestHeader(header, token);
  });
}

$('.checkbox-success').click(function(){
   $(this).toggleClass('is-checked');
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});


//アイデアのポイント会計
function pointAccounting(allVote, upVote) {
	
	if(!allVote) {
		return 0
	}
	
	const allVoteCnt = Number(allVote);
	const upVoteCnt = Number(upVote);
	
	const point = upVoteCnt*10 - (allVoteCnt - upVoteCnt)*10
	
	return point;
}