$('.vote-modal').on('show.bs.modal', function (event) {
	
	if ($('#data-geted').val() == '1') {
		return;
	}
	
	$('#data-geted').val('1');
	
	$('.vote-up-body').hide();
	$('.vote-down-body').hide();
	$('body').addClass('open-modal');

	$('.vote-down-more').hide();
	$('.vote-up-more').hide();
	
	const id = $(event.relatedTarget).attr('ideaId');
	$('#vote-idea-id').val(id);
	
	const title = $(event.relatedTarget).attr('title');
	
	$('#vote-modal-title').text('投票ユーザー (' + $('#voteCount').val() + ' )');
	
	getVoteUp(0);
	getVoteDown(0);
});

$('.vote-modal').on('hide.bs.modal', function (event) {	
	$('body').removeClass('open-modal');	
});

function getVoteUp(page) {
	
	const param = {
		agreed: true,
		ideaId: $('#vote-idea-id').val(),
		page:page
	};
	
	searchVoteList(param);
}

function getVoteDown(page) {
	
	const param = {
			agreed: false,
		ideaId: $('#vote-idea-id').val(),
		page:page
	};
	
	searchVoteList(param);
}

function searchVoteList(param) {
	
   const url = $('#contextPath').val() + 'vote';
	
	$.ajax({
		type : "GET",
		url : url,
		data: param,
		dataType : "json",
		beforeSend: function (xhr, settings) {
			setCsrfTokenToAjaxHeader();
		},
		success : function(data, status, xhr) {
			setVoteArea(data);
		},
		error : function(XMLHttpRequest, status, errorThrown) {
			if (XMLHttpRequest.status===401) {
        		window.location.href = $("#contextPath").val() + "login?timeout";
        		return;
        	}
			window.location.href = 'error';
		}
	});
}

function setVoteArea(data) {
	
	let voteKbn = 'down';
	if (data.isAgreed) {
		voteKbn = 'up';
	}
	
  for(idx in data.resultList) {

	  const vote = data.resultList[idx];
	
	  const obj=$('.vote-'+voteKbn+'-element').clone(true);
	  
	  obj.find('.users-link')[0].href = $('#contextPath').val() + 'users/' + vote.userId;
	  obj.find('.user-info-img')[0].src = vote.profileUrl;
	  obj.find('.user-name').text(vote.userName);
	  obj[0].classList.remove("d-none");
	  obj[0].classList.remove('vote-'+voteKbn+'-element');
	  obj[0].classList.add('vote-'+voteKbn+'-suggest-element');
	  $('.vote-'+voteKbn+'-area').append(obj);
	}
  
    $('.vote-modal').find('.vote-'+voteKbn+'-area').height($('vote-'+voteKbn+'-suggest-element').length * 130);
    const h = $(window).height()-500;
    $('.modal-content').css('min-height', h + 'px');
    $('.vote-'+voteKbn+'-more').show();
	if (data.last) {
		$('.vote-'+voteKbn+'-more').hide();
	}
	$('.vote-'+voteKbn+'-total-count').text(data.total);
	$('.vote-'+voteKbn+'-page').val(data.page);
	$('.vote-'+voteKbn+'-body').show();
}


$(".vote-down-more-link").on("click", function() {
	getVoteDown(parseInt($('.vote-down-page').val())+1);
});
$(".vote-up-more-link").on("click", function() {
	getVoteUp(parseInt($('.vote-up-page').val())+1);
});
