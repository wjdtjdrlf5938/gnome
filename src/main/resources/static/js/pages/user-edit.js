$('.user-create-modal').on('show.bs.modal', function (event) {
    $("#id").val('');
    $("#lastName").val('');
    $("#firstName").val('');
    $("#mail").val('');
    $("#pw").val('');
    $("#adminAccount").removeAttr('checked').prop('checked', false).change();
    $("#tempPassword").removeAttr('checked').prop('checked', false).change();
    $("#createBtn").attr('disabled',true);
    $("#buttonArea").show();
    $("#successArea").hide();
    $("#successMsgArea").hide();
    $("#inputArea").show();
});

$('.password-modal').on('show.bs.modal', function (event) {

	initProc($(this));
    $("#changePwBtn").attr('disabled',true);
    
    getData($(this), 0);
});


$('.temp-ideaId').on('click', function() {
	
	$('#deleteTempIdeaid').val($(event.currentTarget).attr('tempIdeaId'));
	
});


$('.user-create-modal').on('click', '.modal-footer #createBtn', function() {
   
	$("#createBtn").hide();
	$("#loadingBtn").show();
	
    const param = setParam();
    
    const url = $('#contextPath').val() + "auth/users/create";
    
    $.ajax({
        type : "POST",
        url : url,
        data: param,
        dataType : "json",
        beforeSend: function (xhr, settings) {
            setCsrfTokenToAjaxHeader();
        },
        success : function(data, status, xhr) {
          successProc(data);
        },
        error : function(XMLHttpRequest, status, errorThrown) {
        	if (XMLHttpRequest.status===401) {
        		window.location.href = $("#contextPath").val() + "login?timeout";
        		return;
        	}
        	window.location.href = 'error';
        }
	});
});

$('.update-modal').on('show.bs.modal', function (event) {
	  
	initProc($(this));
	
    getData($(this) ,1);
});

$('.update-modal').on('click', '.modal-footer #createBtn', function() {
  
  $("#updateLoadingBtn").show();
  $("#createBtn").hide();

  const modal = $('.update-modal');
  
  const param = {
    id: $("#id").val(),
    lastName: modal.find('.modal-body input#lastName').val(),
    firstName: modal.find('.modal-body input#firstName').val(),
    mail: modal.find('.modal-body input#mail').val(),
    version: modal.find('.modal-body input#version').val()
  };
  
  const url = $('#contextPath').val() + "users/update";
  
  $.ajax({
        type : "PUT",
        url : url,
        data: param,
        dataType : "json",
        beforeSend: function (xhr, settings) {
            setCsrfTokenToAjaxHeader();
        },
        success : function(data, status, xhr) {
          successUpdateProc(data, modal, 1);
        },
        error : function(XMLHttpRequest, status, errorThrown) {
        	if (XMLHttpRequest.status===401) {
        		window.location.href = $("#contextPath").val() + "login?timeout";
        		return;
        	}
        	window.location.href = 'error';
        }
	});
});

$('.password-modal').on('click', '.modal-footer #changePwBtn', function() {

  $("#changeLoadingBtn").show();
  $("#changePwBtn").hide();
  
  const modal = $('.password-modal');

  const param = {
	id: $("#id").val(),
    pw: modal.find('.modal-body input#pw').val(),
	tempPassword: modal.find('.modal-body input#tempPassword')[0].checked,
	version: modal.find('.modal-body input#version').val()
  };
  
  const url = $('#contextPath').val() + "users/change";
  
  $.ajax({
    type : "PUT",
	url : url,
	data: param,
	dataType : "json",
	beforeSend: function (xhr, settings) {
	  setCsrfTokenToAjaxHeader();
	},
	success : function(data, status, xhr) {
      successUpdateProc(data, modal, 0);
    },
	error : function(XMLHttpRequest, status, errorThrown) {
		if (XMLHttpRequest.status===401) {
        	window.location.href = $("#contextPath").val() + "login?timeout";
        	return;
        }
		window.location.href = 'error';
	}
  });
});

function initProc(modal) {
	
	$("createBtn").show();
	$("changePwBtn").show();
	$("changeLoadingBtn").hide();
	$("updateLoadingBtn").hide();
	
	modal.find('.modal-body input#adminAccount').removeAttr('checked').prop('checked', false).change();
	modal.find('#loadingArea').show();
	modal.find('#buttonArea').show();
	modal.find("#successArea").hide();
	modal.find("#successMsgArea").hide();
	modal.find("#inputArea").hide();
    
}

function getData(modal, kbn) {
	
	const param = {
		id: $("#id").val()
	};
	
	const url = $('#contextPath').val() + "users/edit";
	 
    $.ajax({
        type : "GET",
        url : url,
        data: param,
        dataType : "json",
        success : function(data, status, xhr) {
          setData(data, modal, kbn);
        },
        error : function(XMLHttpRequest, status, errorThrown) {
        	if (XMLHttpRequest.status===401) {
        		window.location.href = $("#contextPath").val() + "login?timeout";
        		return;
        	}
        	window.location.href = 'error';
        }
	});	
}

function setData(data, modal, kbn) {
	
	if (kbn == 1) {
		modal.find('.modal-body input#lastName').val(data.lastName);
		modal.find('.modal-body input#firstName').val(data.firstName);
		modal.find('.modal-body input#mail').val(data.mail);
		if(data.isAdmin){
			modal.find('.modal-body input#adminAccount').attr('checked', true).prop('checked', true).change();
		}
	} else {
		modal.find('.modal-body input#pw').val('');
		modal.find('.modal-body input#tempPassword')[0].checked = false;
	}
	modal.find('.modal-body input#version').val(data.version)
	modal.find("#inputArea").show();
	modal.find("#loadingArea").hide();
}

function setParam() {
  const param = {
    id: $("#id").val(),
    lastName: $("#lastName").val(),
    firstName: $("#firstName").val(),
    mail: $("#mail").val(),
    pw: $("#pw").val(),
    Admin: $("#adminAccount")[0].checked,
    tempPassword: $("#tempPassword")[0].checked,
  };
  return param;
}

function focusOutPw() {
  if ($("#pw").val() &&
      $("#pw").val().length >= 8 ) {
    $("#changePwBtn").attr('disabled',false);	
  } else {
    $("#changePwBtn").attr('disabled',true);	
  }
}

function focusOut() {
  if ($("#lastName").val() &&
      $("#firstName").val()  &&
      $("#mail").val() &&
      $("#pw").val() &&
      $("#pw").val().length >= 8 ) {
    $("#createBtn").attr('disabled',false);	
  } else {
    $("#createBtn").attr('disabled',true);	
  }
}

function successUpdateProc(data, modal, kbn) {
	
	$("#createBtn").show();
	$("#changePwBtn").show();
	
	$("#changeLoadingBtn").hide();
	$("#updateLoadingBtn").hide();
	
	if (data.errorMsg) {
		errorToastr(data.errorMsg);
		return;
	}
	
	modal.find("#buttonArea").hide();
	modal.find("#successArea").show();
	modal.find("#successMsgArea").show();
	modal.find("#inputArea").hide();
	if (kbn == 1) {
		modal.find("#successNameLabel").text(data.lastName + ' ' + data.firstName);
	}
}

function successProc(data) {
	$("#createBtn").show();
	$("#loadingBtn").hide();
	
	if (data.errorMsg) {
		errorToastr(data.errorMsg);
		return;
	}
	
    $("#buttonArea").hide();
	$("#successArea").show();
	$("#successMsgArea").show();
	$("#inputArea").hide();
	$("#successNameLabel").text(data.lastName + ' ' + data.firstName);
}
