$('.create-btn').on('click', function() {
	
	$('#ideaSearch').val('');
	
    $('.select-category-link').hide();
    $('.change-category-link').hide();
    $('.editIdeaList-msg').hide();
    $('#searchArea').show(); 
    $('#nextArea').hide();
    $('#inputArea').hide();
    $('#buttonArea').show();
    $('#tempCreateBtn').hide();
    $('#searchBtn').show();
    $('#createBtn').hide();
    $('#nextBtn').hide();
   
});

$('#searchBtn').on('click', function() {
	
	$('#searchBtn').hide();
	$("#ideaLoadingBtn").show();
	
	const param = {
		searchContents: $('#ideaSearch').val(),
	};
	
	const contextPath = $('#contextPath').val();
	
	$.ajax({
        type : "GET",
        url : contextPath + "edit/search",
        data: param,
        dataType : "json",
        success : function(data, status, xhr) {
        	editSearchsuccessProc(data);
        },
        error : function(XMLHttpRequest, status, errorThrown) {
        	if (XMLHttpRequest.status===401) {
        		window.location.href = $("#contextPath").val() + "login?timeout";
        		return;
        	}
        	window.location.href = 'error';
        }
	});
});

$('#nextBtn').on('click', function() {
	
	$('#ideaId').val('');
    $('#idea-title').val($('.edit-idea-searchTitle').text());
    $('.note-editable').text('');
    $('#selectedCategory').val('');
    $('#selectedCategory').text('');
    $('.category-name').text('');
    $('#nextBtn').hide();
	$('#tempCreateBtn').show();
	$('#createBtn').show();
    $('.select-category-link').show();
    $('#nextArea').hide();
    $('#inputArea').show();
    
});


$('.temp-idea-name').on('click', function() {
	
	$('#searchBtn').hide();
	$('#nextBtn').hide();
	$('#tempCreateBtn').show();
	$('#createBtn').show();
	
    $('#ideaId').val($(event.currentTarget).attr('id'));
    $('#idea-title').val($(event.currentTarget).attr('subject'));
    $('.idea-contens').summernote('code', $(event.currentTarget).attr('contents'));

    $('#selectedCategory').val($(event.currentTarget).attr('category'));
    $('.selectedCategory').text($(event.currentTarget).attr('categoryName'));
    
    $('.select-category-link').hide();
    $('.change-category-link').show();
    $('#buttonArea').show();
    $('#searchArea').hide();
    $('#nextArea').hide();
    $('#inputArea').show();
});


$('.create-modal').on('show.bs.modal', function (event) {	
	$('body').addClass('open-modal');	
});


$('.category-modal').on('show.bs.modal', function (event) {
	$('.category-modal').css('overflow-y','auto');
	$('.create-modal').modal('hide');
	$('body').addClass('open-modal');	
});


$(".category-modal").on('hide.bs.modal', function (event) {
	$('.create-modal').modal('show');
	$('.create-modal').css('overflow-y','auto');
	$('body').addClass('open-modal');	
});


$('.create-modal').on('hide.bs.modal', function (event) {	
	$('body').removeClass('open-modal');
});


$(".child-category-link").on("click", function() {
	
	$('#selectedCategory').val($(event.currentTarget).attr('subCategoryId'));
	$('.selectedCategory').text($(event.currentTarget).attr('categoryName') + ' > ' + $(event.currentTarget).attr('subCategoryName'));
	$('.select-category-link').hide();
    $('.change-category-link').show();
    
	$('.create-modal').show();
	$('.category-modal').hide();
});


$('.create-modal').on('click', '.modal-footer #createBtn, #tempCreateBtn', function(event) {
	   
	$("#createBtn").hide();
	$("#tempCreateBtn").hide();
	$('#searchBtn').hide();
	$('#nextBtn').hide();
	$('#searchArea').hide();
	$('#nextArea').hide();
	
    $('#inputArea').show();
	
	if($(event.currentTarget).attr('isTemporary') == 1){
		$("#tempLoadingBtn").show();
	}else{
		$("#ideaLoadingBtn").show();
	}
	
	const param = {
		id: $("#ideaId").val(),
		categoryId: $("#selectedCategory").val(),
		subject: $("#idea-title").val(),
		contents: $("#ideaContents").val(),
		Temporary: $(event.currentTarget).attr('isTemporary')
	};
	
	const contextPath = $('#contextPath').val();
    
    $.ajax({
        type : "POST",
        url : contextPath + "create",
        data: param,
        dataType : "json",
        beforeSend: function (xhr, settings) {
        	setCsrfTokenToAjaxHeader();
        },
        success : function(data, status, xhr) {
        	createSuccessProc(data, contextPath)
        },
        error : function(XMLHttpRequest, status, errorThrown) {
        	if (XMLHttpRequest.status===401) {
        		 window.location.href = $("#contextPath").val() + "login?timeout";
        		 return;
        	}
        	window.location.href = 'error';
        }
	});
});


$('.up-vote, .down-vote').on('click', function(event) {
	
	const eventTaget = $(event.currentTarget).closest('.vote-area');
	
	eventTaget.find('.up-vote').css('pointer-events','none')
	eventTaget.find('.down-vote').css('pointer-events','none')
	
	const id = $(event.currentTarget).attr('id');
	
	const key = $(event.currentTarget).attr('select');
	
	const param = {
			ideaId: id,
			agreed: key === "1"
	}
	
	const contextPath = $('#contextPath').val();
	
	$.ajax({
        type : "POST",
        url : contextPath + "vote",
        data: param,
        dataType : "json",
        beforeSend: function (xhr, settings) {
            setCsrfTokenToAjaxHeader();
        },
        success : function(data, status, xhr) {
        	if($(event.currentTarget).attr('detail') == 0) {
        		window.location.reload();
        	}
        	successProc(data, eventTaget);
        },
        error : function(XMLHttpRequest, status, errorThrown) {
        	if (XMLHttpRequest.status===401) {
        		window.location.href = $("#contextPath").val() + "login?timeout";
        		return;
        	}
        	window.location.href = 'error';
        }
	});	
});


$('.temp-idea-delete').on('click', function(event) {
	
	const id = $(this).closest('.temp-delete-modal').find('#deleteTempIdeaid').val();
	
	const param = {
			id: id	
	}
	
	const contextPath = $('#contextPath').val();
	
	$.ajax({
        type : "DELETE",
        url : contextPath + "delete",
        data: param,
        dataType : "json",
        beforeSend: function (xhr, settings) {
            setCsrfTokenToAjaxHeader();
        },
        success : function(data, status, xhr) {
        	tempIdeaDeleteSuccessProc()
        },
        error : function(XMLHttpRequest, status, errorThrown) {
        	if (XMLHttpRequest.status===401) {
        		window.location.href = $("#contextPath").val() + "login?timeout";
        		return;
        	}
        	window.location.href = 'error';
        }
	});	
});


$('.favorite-mark').on('click', function(event) {
	
	$('.favorite-mark').css('pointer-events','none');
	
	document.getElementById("favoriteBtn").classList.toggle("favorite-btn");
	document.getElementById("favoriteBtn").classList.toggle("favorite-btn-checked")
	
	document.getElementById("favoriteIcon").classList.toggle("far");
	document.getElementById("favoriteIcon").classList.toggle("fas");
	
	const id = $(event.currentTarget).attr('id');
	
	const param = {
			ideaId: id,
			favoriteFlg: true
	}
	
	const contextPath = $('#contextPath').val();
	
	$.ajax({
        type : "POST",
        url : contextPath + "favorite",
        data: param,
        dataType : "json",
        beforeSend: function (xhr, settings) {
            setCsrfTokenToAjaxHeader();
        },
        success : function(data, status, xhr) {
        	favoriteSuccessProc(data)
        },
        error : function(XMLHttpRequest, status, errorThrown) {
        	if (XMLHttpRequest.status===401) {
        		window.location.href = $("#contextPath").val() + "login?timeout";
        		return;
        	}
        	window.location.href = 'error';
        }
	});
});


function editSearchsuccessProc(data) {
	
	$("#ideaLoadingBtn").hide();
	
	if(data.errList) {
		
		$('#searchBtn').show();
		
		for (msg in data.errList) {
    		errorToastr(data.errList[msg]);
    	}
		return;
		
	}
	
	$('.edit-idea-searchTitle').text(data.searchContents);
	$('.suggest-element').remove();
	$('.editIdeaList-msg').hide();
	
	if(data.ideaList.length == 0){
		$('.editIdeaList-msg').show();
	}
	
	for(idx in data.ideaList) {
		
		const idea = data.ideaList[idx];
		
	    const obj = $('.candidate-card').clone(true);
	    
		obj.find('.edit-idea-link')[0].href = $('#contextPath').val() + 'ideas/' + idea.id;
		obj.find('.edit-idea-img')[0].src = idea.profileUrl;
		obj.find('.edit-idea-title').text(idea.subject);
		obj.find('.edit-idea-commentCnt').text(idea.countComments);
		obj.find('.edit-idea-point').text(idea.point);
		obj.find('.edit-idea-status').text(idea.statusName);
		obj[0].classList.add("suggest-element");
		obj[0].classList.remove("d-none");
		obj[0].classList.remove("candidate-card");
		
		$('.editIdeaList').append(obj);
	}
	
	$('#searchArea').hide();
    $('#nextArea').show();
    $('#nextBtn').show();
}


function successProc(data, eventTaget) {
	
	eventTaget.find('.vote-point').text(data.point);
	
	if(data.isAgreed == true) {
		eventTaget.find('.up-vote').addClass('is-checked-up');
		eventTaget.find('.up-vote').addClass('animated heartBeat');
		
		eventTaget.find('.down-vote').addClass('not-checked');
		eventTaget.find('.down-vote').addClass('animated fadeOut');
		
	}else{
		eventTaget.find('.up-vote').addClass('not-checked');
		eventTaget.find('.up-vote').addClass('animated fadeOut');
		
		eventTaget.find('.down-vote').addClass('is-checked-down');
		eventTaget.find('.down-vote').addClass('animated heartBeat');
	}
	
	eventTaget.find('.point-area').addClass('animated bounce fast');
	eventTaget.find('.up-vote').removeClass('up-vote');
	eventTaget.find('.down-vote').removeClass('down-vote');
};


function createSuccessProc(data, contextPath) {
	
	if(data.errList) {
		
		$("#ideaLoadingBtn").hide();
		$("#tempLoadingBtn").hide();
		$('#searchBtn').hide();
		$('#nextBtn').hide();
		$("#createBtn").show();
		$("#tempCreateBtn").show();
		
		if(data.categoryId){
			$('.change-category-link').show();
		}else{
			$('.select-category-link').show();
		}
		
		for (msg in data.errList) {
    		errorToastr(data.errList[msg]);
    	}
		return;
		
	}
	
	if(data.isTemporary) {
		window.location.href = contextPath + 'users/' + data.createdBy + '?tab=2';
		return
	}
	
	window.location.href = contextPath + 'ideas/' + data.id;
};


function tempIdeaDeleteSuccessProc() {
	
	$('#deleteTempArea').hide();
	$('#deleteTempButtonArea').hide();
	
	$('#successDeleteTempArea').show();
	$('#successDeleteTempButtonArea').show();
	
}

function favoriteSuccessProc(data) {
	
	if(data.favoriteFlg) {
		successToastr("お気に入り登録が完了しました。");
	}
	
	$('.favorite-mark').css('pointer-events','auto');
}