$(function () {
	$('form').submit(function () {
		$('#login-btn').hide();
		$('#loding-btn').show();
	});
});

function focusOut() {
  if ($("#password").val() &&
	  $("#confPassword").val()  &&
	  $("#password").val().length >= 8 &&
	  $("#confPassword").val().length >= 8 &&
	  $("#confPassword").val() == $("#password").val()) {
	  $("#login-btn").attr('disabled',false);	
  } else {
	  $("#login-btn").attr('disabled',true);	
  }
}

window.onload = function() {
   $(".err-msg").each(function(j, obj) {
	  errorToastr($(obj).val());
  });
}

function errorToastr(msg) {
  toastr.options = {
    "closeButton": true,
    "positionClass": "toast-bottom-center",
    "showDuration": "0",
    "hideDuration": "0",
    "timeOut": "0",
    "extendedTimeOut": "0"
  }
  toastr.error(msg);
}