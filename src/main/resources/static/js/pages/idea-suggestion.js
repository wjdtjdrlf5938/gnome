$('.suggest-modal').on('show.bs.modal', function (event) {	
	$('body').addClass('open-modal');	
});

$('.suggest-modal').on('hide.bs.modal', function (event) {	
	$('body').removeClass('open-modal');	
});

$('.suggest-link').on('click', function() {
	
	$("#successSuggestArea").hide();
	$("#successSuggestBtnArea").hide();
	$('#buttonSuggestArea').show();
	$('.Suggest-modal-body').show();
	
	$('#suggestURL').val('');
	$('#suggestContents').val('');
	
});



$('.suggest-modal').on('click', '.modal-footer #suggestBtn', function() {
	
	$("#suggestBtn").hide();
	$("#suggestLoadingBtn").show();
	
	const param = {
		ideaId: $('#ideaId').val(),
		url: $('#suggestURL').val(),
		contents: $('#suggestContents').val()
	};
	
	const contextPath = $('#contextPath').val();
	
	$.ajax({
        type : "POST",
        url : contextPath + "suggestons/create",
        data: param,
        dataType : "json",
        beforeSend: function (xhr, settings) {
            setCsrfTokenToAjaxHeader();
        },
        success : function(data, status, xhr) {
        	suggestSuccessProc(data);
        },
        error : function(XMLHttpRequest, status, errorThrown) {
        	if (XMLHttpRequest.status===401) {
        		window.location.href = $("#contextPath").val() + "login?timeout";
        		return;
        	}
        	window.location.href = 'error';
        }
	});
});


function suggestSuccessProc(data){
	
	$("#suggestBtn").show();
	$("#suggestLoadingBtn").hide();
	$('#buttonSuggestArea').show();
	
	if(data.errList) {
		for (msg in data.errList) {
			errorToastr(data.errList[msg]);
		}
		return;
	}
	
	$('#buttonSuggestArea').hide();
	$("#successSuggestArea").show();
	$("#successSuggestBtnArea").show();
	$('.Suggest-modal-body').hide();
}