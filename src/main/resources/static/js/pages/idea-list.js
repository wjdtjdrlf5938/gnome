$(document).ready(function (){
	
	if($('#ideaStatus').val()) {
		$('#navbarSupportedContent2').addClass('show');
	}
	
	//小さい画面で親カテゴリー設定
	if($('#ideaCategory').val()) {
		$("#category-sources").val($('#ideaCategory').val());
	}else{
		$('.category-body').addClass('selected-link');
	}
	
	//小さい画面で子カテゴリー設定
	if($('#ideaSubcategory').val()) {
		$("#subCategory-sources").val($('#ideaSubcategory').val());
	}
});


$(".navbar-collapse").on("shown.bs.collapse", function(e) {
	
	$(e.currentTarget).closest('.navbar-light').find(".fa-caret-up").show();
	$(e.currentTarget).closest('.navbar-light').find(".fa-caret-down").hide();
    
});


$(".navbar-collapse").on("hidden.bs.collapse", function(e) {
	
	$(e.currentTarget).closest('.navbar-light').find(".fa-caret-up").hide();
	$(e.currentTarget).closest('.navbar-light').find(".fa-caret-down").show();
    
});


$(".custom-option").on("click", function() {
	$('#ideaSort').val($('.drop-checkbox').val());
	$('form').submit();
});


$(".sort-sm-link").on("click", function() {
	
	$('#ideaSort').val($(event.currentTarget).attr('value'));
	
	$('form').submit();
});

$("#category-sources").change(function() {
	
	$('#ideaCategory').val('');
	$('#ideaSubcategory').val('');
	$('#ideaStatus').val('');
	
	if($(this).val() != 0) {
		$('#ideaCategory').val($(this).val());
	}

	$('form').submit();
});

$("#subCategory-sources").change(function() {
	
	$('#ideaSubcategory').val('');
	$('#ideaStatus').val('');
	
	if($(this).val() != 0) {
		$('#ideaSubcategory').val($(this).val());
	}

	$('form').submit();
});


$(".category-link").on("click", function() {
	
	$('#ideaCategory').val('');
	$('#ideaSubcategory').val('');
	$('#ideaStatus').val('');
	
	if($(event.currentTarget).attr('parentCategoryId')) {
		$('#ideaCategory').val($(event.currentTarget).attr('parentCategoryId'));
	}
	
	if($(event.currentTarget).attr('categoryId')) {
		$('#ideaSubcategory').val($(event.currentTarget).attr('categoryId'));
	}
	
	$('form').submit();
});


$(".status-Link").on("click", function() {
	
	if($(event.currentTarget).attr('statusId')) {
		$('#ideaStatus').val($(event.currentTarget).attr('statusId'));
	}
	
	$('form').submit();
});
