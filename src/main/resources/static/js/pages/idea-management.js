$(document).ready(function (){
	
	if($("#parentCategoryId").val()) {
    	$('.select-category-link').hide();
    	$('.change-category-link').show();
    	$('.category-cancel').show();
    }
	
    $(".ideaStatus").chosen({
    	placeholder_text_single : "ステータスを選択してください。",
		width: "100%",
		allow_single_deselect: true
	});
	
    $("#developStatus").chosen({
    	placeholder_text_single : "ステータスを選択してください。",
		width: "100%"
	});   
});


$(".parents-category-link, .child-category-link").on("click", function() {
	
	categoryReset()
	
	$('#parentCategoryId').val($(event.currentTarget).attr('categoryId'));
	$('#selectedCategory').text($(event.currentTarget).attr('categoryName'));
	$('#categoryName').val($(event.currentTarget).attr('categoryName'));
	
	if($(event.currentTarget).attr('subCategoryId')) {
		$('#categoryId').val($(event.currentTarget).attr('subCategoryId'));
		const categoryName = $(event.currentTarget).attr('categoryName') + ' > ' + $(event.currentTarget).attr('subCategoryName');
		
		$('#selectedCategory').text(categoryName);
		$('#categoryName').val(categoryName)
	}
	
	$('.select-category-link').hide();
	$('.category-cancel').show();
    $('.change-category-link').show();
    
	$('.category-modal').hide();
	
});


$(".category-cancel").on("click", function() {
	
	categoryReset();
	
	$('.select-category-link').show();
	$('.category-cancel').hide();
    $('.change-category-link').hide();
    
});

function categoryReset() {
	$('#selectedCategory').text("");
	$('#categoryId').val("");
	$('#subCategoryId').val("");
	$('#parentCategoryId').val("");
	$('#categoryName').val("");
	$('#categoryId').val("");
}


$('.develop-modal').on('show.bs.modal', function (event) {
	
	const url = $('#contextPath').val() + "auth/management/" + $(event.relatedTarget).attr('ideaId');
	$('.develop-ideaId').val($(event.relatedTarget).attr('ideaId'));
	
	$.ajax({
        type : "GET",
        url : url ,
        data: {},
        dataType : "json",
        success : function(data, status, xhr) {
          setIdeaDetailData(data);
        },
        error : function(XMLHttpRequest, status, errorThrown) {
        	if (XMLHttpRequest.status===401) {
        		window.location.href = $("#contextPath").val() + "login?timeout";
        		return;
        	}
        	window.location.href = 'error';
        }
	});
});

function setIdeaDetailData(data) {
	$(".develop-status").val(data.statusId).trigger("chosen:updated");
	
	$(".develop-expectedOn").val(data.expectedOn);
	$(".develop-releasedOn").val(data.releasedOn);

	$('.develop-memo').val(data.developmentMemo);
	
	$(".develop-version").val(data.version);
	
}

function setIdeaPreviewData(data) {
	
	if(data.errList) {
		for (msg in data.errList) {
			errorToastr(data.errList[msg]);
		}
		return;
	}
	
	const idea = data.entity;
	
	$('.develop-subject').text(idea.subject);
	$('.develop-category').text(idea.parentCategory + ">" + idea.subCategory);
	
	$('.develop-contents').text(idea.contents);
	
	$('.develop-ideaCreatedAt').text(idea.createdAt);
	$('.develop-ideaUserName').text(idea.userName);
	
	$("#developIdeaUserNameLink")[0].href = $('#contextPath').val() + 'users/' + idea.createdBy
	
	$("#edit-user-img")[0].src = idea.profileUrl;
	
	$("#previewModalIdeaArea").show();
}


$('.develop-modal').on('click', '.modal-footer #createBtn', function() {
	
	$("#createBtn").hide();
	$("#loadingBtn").show();
	
	const param = {
		id: $('.develop-ideaId').val(),
		statusId: $(".develop-status").val(),
		expectedOn: $(".develop-expectedOn").val(),
		releasedOn: $(".develop-releasedOn").val(),
		developmentMemo: $('.develop-memo').val(),
		version: $(".develop-version").val()
	};
	
	const contextPath = $('#contextPath').val();
	
	$.ajax({
        type : "PUT",
        url : contextPath + "auth/management/update",
        data: param,
        dataType : "json",
        beforeSend: function (xhr, settings) {
        	setCsrfTokenToAjaxHeader();
        },
        success : function(data, status, xhr) {
        	developUpdatesuccess(data);
        },
        error : function(XMLHttpRequest, status, errorThrown) {
        	if (XMLHttpRequest.status===401) {
        		window.location.href = $("#contextPath").val() + "login?timeout";
        		return;
        	}
        	window.location.href = 'error';
        }
	});
});

function developUpdatesuccess(data) {
	
	$("#loadingBtn").hide();
	$("#createBtn").show();
	
	if(data.errList) {
		for (msg in data.errList) {
			errorToastr(data.errList[msg]);
		}
		return;
	}
	
	successToastr('更新処理が完了しました')
};

$('.search-detail-link').on("click", function () {
  $('.detail-search-area').toggleClass('toggle-area');
  $('.easy-search-area').toggleClass('toggle-area');
  
  const result = $('#searchDtlDisp').val() === 'true' ? false : true;
  
  let val = $('#keyword-easy').val();
  
  if (!result) {
    val = $('#keyword-detail').val();
  }
  
  $('#keyword-easy').val(val);
  $('#keyword-detail').val(val);
  $('#searchContents').val(val);
  
  $('#searchDtlDisp').val(result);
});

$(".btn-manage-search").on("click", function() {
	const result = $('#searchDtlDisp').val() === 'true' ? true : false;
	let val = $('#keyword-easy').val();
	if (result) {
		val = $('#keyword-detail').val();
	}
	$('#searchContents').val(val);
	$('form').submit();
});

$(".delete-idea-btn").on("click", function() {
	const url = $('#contextPath').val() + "auth/management/delete";
	$('form').attr('action', url);
	$('form').submit();
});