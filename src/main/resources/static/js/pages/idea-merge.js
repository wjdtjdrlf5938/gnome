$('.merge-modal').on('show.bs.modal', function (event) {
	
	$('.merge-complete-area').hide();
	$('.merge-area').show();
	$('.merge-close-btn').hide();
	$('.merge-cancel-btn').show();
	$('#margeReload').val('0');
	
	$('body').addClass('open-modal');
	$('.suggest-element').remove();
	$('.more-link').hide();
	$('.merge-modal').find('.result-area').height(0);
	
	const id = $(event.relatedTarget).attr('ideaId');
	
	const title = $(event.relatedTarget).attr('title');
	
	$('#margeTitle').text(title + 'にマージ処理をします');
	
	$('.noIdea-area').hide();
	$('.merge-idea-area').hide();
	
	$('#mergeIdeaSearch').val('');
	$('#margeTargetId').val(id);
	
	$('#mergeBtn').hide();	
});

$('.merge-modal').on('hide.bs.modal', function (event) {	
	$('body').removeClass('open-modal');	
	if ($('#margeReload').val() == '1') {
		window.location.reload();
	}
});

$('.merge-modal').on('click', '.modal-body #mergeIdeaSearchBtn', function() {
	
	$('.suggest-element').remove();
	$('.more-link').hide();
	$('.count').val("0");
	$('.page').val("0");
	
	const param = {
		keyword: $('#mergeIdeaSearch').val(),
		ideaId: $('#margeTargetId').val(),
		page:0
	};
	
	search(param);
});

function search(param) {
   const contextPath = $('#contextPath').val();
	
	$.ajax({
		type : "GET",
		url : contextPath + "auth/merge/search",
		data: param,
		dataType : "json",
		beforeSend: function (xhr, settings) {
			setCsrfTokenToAjaxHeader();
		},
		success : function(data, status, xhr) {
			manageSearchSuccessProc(data)
		},
		error : function(XMLHttpRequest, status, errorThrown) {
			if (XMLHttpRequest.status===401) {
        		window.location.href = $("#contextPath").val() + "login?timeout";
        		return;
        	}
			window.location.href = 'error';
		}
	});
}

$('.merge-modal').on('click', '.modal-footer #mergeBtn', function() {
	
	let list = [];
	
	$("[name=merge-check-box]:checked").each(function() {
		  list.push( $(this).val() );
		});
	
	if (list.length == 0) {
		errorToastr('マージ対象を選択してください');
		return;
	}
	
	const param = {
		ideaId: $('#margeTargetId').val(),
		targets: list.join(',')
	};
	
	const contextPath = $('#contextPath').val();
	
	 
	$.ajax({
		type : "POST",
		url : contextPath + "auth/merge",
		data: param,
		dataType : "json",
		beforeSend: function (xhr, settings) {
			setCsrfTokenToAjaxHeader();
		},
		success : function(data, status, xhr) {
			mergeSuccessProc(data);
		},
		error : function(XMLHttpRequest, status, errorThrown) {
			if (XMLHttpRequest.status===401) {
        		window.location.href = $("#contextPath").val() + "login?timeout";
        		return;
        	}
			window.location.href = 'error';
		}
	});
	
});


function manageSearchSuccessProc(data) {

	$('.page').val(data.page);
	$('.count').val(parseInt($('.count').val())+data.resultList.length);
	
	for(idx in data.resultList) {
		
		const idea = data.resultList[idx];
		
		const obj=$('.candidate-card').clone(true);
		obj.find('.custom-control-input')[0].id = "check" + idea.id;
		obj.find('.custom-control-input')[0].value = idea.id;
		obj.find('.custom-control-input')[0].name = "merge-check-box";
		obj.find('.custom-control-label')[0].htmlFor = "check" + idea.id;
		
		obj.find('.suggest-img')[0].src = idea.profileUrl;
		let subject = idea.subject;
		if (subject.length >= 20) {
			subject = subject.substr( 0, 20 ) + '......';
		}
		obj.find('.suggest-title').text(subject);
		obj.find('.suggest-name').text(idea.userName);
		obj.find('.suggest-create-at').text(idea.createdAt);
		obj.find('.suggest-category').text(idea.parentCategory + ' > '+ idea.subCategory);
		let content = idea.contents.replace(/\<.*?>/g,'');
		if (content.length >= 35) {
			content = content.substr( 0, 35 ) + '......';
		}
		obj.find('.suggest-content').text(content);
		obj.find('.suggest-status').text(idea.statusName);
		obj.find('.suggest-comment').text(idea.comment);
		obj.find('.suggest-point').text(idea.point);
		
		obj[0].classList.add("suggest-element");
		obj[0].classList.remove("d-none");
		obj[0].classList.remove("candidate-card");
		
		const body = $('.merge-modal').find('.modal-body').height();
		const area= $('.result-area').height();
		$('.merge-modal').find('.result-area').height($('.count').val() * 130);
	    const h = $(window).height()-500;
	    $('.modal-content').css('min-height', h + 'px');
	    
		obj.find('.hidden-point').val(idea.point);
		obj.find('.hidden-status').val(idea.statusName);
		obj.find('.hidden-img').val(idea.profileUrl);
		obj.find('.hidden-name').val(idea.userName);
		obj.find('.hidden-title').val(idea.subject);
		obj.find('.hidden-parent').val(idea.parentCategory);
		obj.find('.hidden-sub').val(idea.subCategory);
		obj.find('.hidden-content').val(idea.contents);
		obj.find('.hidden-comment').val(idea.comment);
		obj.find('.hidden-create-at').val(idea.createdAt);
		obj.find('.hidden-positive').val(idea.positive);
		obj.find('.hidden-negative').val(idea.negative);
		
		$('.result-area').append(obj);
	}
	
	$('.more-link').show();
	$('.noIdea-area').hide();
	$('#mergeBtn').show();
	
	if (data.last) {
		$('.more-link').hide();
	}

	if (data.totalPages == 0) {
		$('.suggest-element').remove();
	}
	
	if ($('.suggest-element').length == 0) {
		$('.noIdea-area').show();
		$('#mergeBtn').hide();
		$('.merge-modal').find('.result-area').height(0);
	}
	
	
};


function mergeSuccessProc(data) {
	
	if(data.errList) {
		for (msg in data.errList) {
			errorToastr(data.errList[msg]);
		}
		return;
	}
	
	$('.merge-close-btn').show();
	$('.merge-cancel-btn').hide();
	$('.merge-complete-area').show();
	$('.merge-area').hide();
	$('#mergeBtn').hide();
	$('#margeReload').val('1')
}

$(".more-link").on("click", function() {

	const param = {
			keyword: $('#mergeIdeaSearch').val(),
			ideaId: $('#margeTargetId').val(),
			page: parseInt($('.page').val())+1
	}
	search(param);
});

$(".btn-preview").on("click", function() {
	const obj = $(this).closest('.suggest-merge-card');
	
	$('.merge-close-modal').show();
	
	$('.preview-close-modal').hide();
	$('.preview-modal').modal('show');
	
	const param = {
			entity:{
				point: obj.find('.hidden-point').val(),
				statusName: obj.find('.hidden-status').val(),
				profileUrl: obj.find('.hidden-img').val(),
				userName: obj.find('.hidden-name').val(),
				subject: obj.find('.hidden-title').val(),
				parentCategory: obj.find('.hidden-parent').val(),
				subCategory: obj.find('.hidden-sub').val(),
				contents:obj.find('.hidden-content').val(),
				comment:obj.find('.hidden-comment').val(),
				createdAt:obj.find('.hidden-create-at').val(),
				positive:obj.find('.hidden-positive').val(),
				negative:obj.find('.hidden-negative').val()
			}
	}
	setIdeaPreviewData(param);
});

$(".merge-close-modal").on("click", function() {
	$('.preview-close-modal').show();
	$('.merge-close-modal').hide();
	$('.preview-modal').modal('hide');
});