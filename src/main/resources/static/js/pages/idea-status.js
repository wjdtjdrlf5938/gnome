$(function () {
  $("#sortable").sortable();
  $("#sortable").disableSelection();
  $('.init-status-question').popover({
	  container: 'body'
  });
});

$('.update-btn').on('click', function() {
	$('#idList').val($('#sortable').sortable("toArray"));
	submitAction('status/update');
});

var tmp_id = 0;
$('.add-btn').on('click', function() {
	let obj = $('.temp_element').clone(true);
	obj[0].classList.remove("temp_element");
	obj.find('.form-control')[0].name = 'statusList['+$('.fix-element').length+'].name';
	obj[0].classList.add("fix-element");
	obj.show();
	obj[0].id=tmp_id;
	obj.find('.new-element').val(tmp_id);
	obj.find('.new-element')[0].name = 'statusList['+$('.fix-element').length+'].id';
	obj.find('.form-control')[0].name = 'statusList['+$('.fix-element').length+'].name';
	
	obj.find('.check-status')[0].name = 'statusList['+$('.fix-element').length+'].completed';
	obj.find('.check-status').val(tmp_id);
	obj.find('.check-status')[0].id = 'checkRecords'+tmp_id;
	obj.find('.check-status-label')[0].htmlFor = 'checkRecords'+tmp_id;
	
	tmp_id = tmp_id-1;
	
	$('#sortable').append(obj);
	$('#sortable').sortable("refresh");
});


$('.delete-element').on('click', function() {
	const selectId = $(this).parent().parent().attr('id');
	
	$(this).parent().parent().remove();
	
	if(selectId > 0) {
		let tartget = selectId;
		
		if($("#delTarget").val()) {
			tartget = ","+tartget;
		}
		$("#delTarget").val($("#delTarget").val()+tartget);
	}
});