$('.preview-modal').on('show.bs.modal', function (event) {
	
	$("#previewModalIdeaArea").hide();
	$('body').addClass('open-modal');
	
	if ("1" != $(event.relatedTarget).attr('viewFlg')) {
		return;
	}
	$('.merge-close-modal').hide();
	
	const url = $('#contextPath').val() + "auth/management/" + $(event.relatedTarget).attr('ideaId');
	
	$.ajax({
        type : "GET",
        url : url ,
        data: {},
        dataType : "json",
        success : function(data, status, xhr) {
          setIdeaPreviewData(data);
        },
        error : function(XMLHttpRequest, status, errorThrown) {
        	if (XMLHttpRequest.status===401) {
        		window.location.href = $("#contextPath").val() + "login?timeout";
        		return;
        	}
        	window.location.href = 'error';
        }
	});
});

$(".preview-close-modal").on("click", function() {
	$('body').removeClass('open-modal');
});
$(".merge-close-modal").on("click", function() {
	$('.merge-modal').addClass('over-auto');
});

function setIdeaPreviewData(data) {
	
	if(data.errList) {
		for (msg in data.errList) {
			errorToastr(data.errList[msg]);
		}
		return;
	}
	
	const idea = data.entity;
	
	$('.preview-point').text(idea.point);
	$('.preview-status').text(idea.statusName);
	$(".preview-img")[0].src = idea.profileUrl;
	$('.preview-name').text(idea.userName);
	$('.preview-subject').text(idea.subject);
	$('.preview-category').text(idea.parentCategory + ">" + idea.subCategory);
	$('.preview-content')[0].srcdoc = idea.contents;
	
	$('.preview-comment').text(idea.comment);
	$('.preview-created').text(idea.createdAt);
	$('.preview-positive').text(idea.positive);
	$('.preview-negative').text(idea.negative);
	
	$("#previewModalIdeaArea").show();
}

$('.preview-content').on('load', function(){
    try {
        $(this).height(0);
        $(this).height(this.contentWindow.document.documentElement.scrollHeight);
    } catch (e) {
    }
}).trigger('load');