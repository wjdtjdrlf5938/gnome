$('.merged-modal').on('show.bs.modal', function (event) {
	
	if ($('.merged-data-geted').val() == '1') {
		return;
	}
	
	$('.merged-data-geted').val('1');
	$('body').addClass('open-modal');

	$('.merged-more-link').hide();
	
	const id = $(event.relatedTarget).attr('ideaId');
	$('.merged-idea-id').val(id);
	
	$('#merged-list-title').text('マージされたアイデア (' + $('#mergedCnt').val() + ' )');

	searchMergedList(0);
});
$('.merged-modal').on('hide.bs.modal', function (event) {
	$('body').removeClass('open-modal');
});
function searchMergedList(page) {
	
   const url = $('#contextPath').val() + 'merged';
	
   const param = {
		   ideaId:$('.merged-idea-id').val(),
		   page: page
   }
   
	$.ajax({
		type : "GET",
		url : url,
		data: param,
		dataType : "json",
		beforeSend: function (xhr, settings) {
			setCsrfTokenToAjaxHeader();
		},
		success : function(data, status, xhr) {
			setMergedArea(data);
		},
		error : function(XMLHttpRequest, status, errorThrown) {
			if (XMLHttpRequest.status===401) {
        		window.location.href = $("#contextPath").val() + "login?timeout";
        		return;
        	}
			window.location.href = 'error';
		}
	});
}

function setMergedArea(data) {
	
  for(idx in data.mergedList) {

	  const mergedIdea = data.mergedList[idx];
	
	  const obj=$('.merged-card').clone(true);
	  
	  obj.find('.merged-title').text(mergedIdea.subject);
	  let content = mergedIdea.contents.replace(/\<.*?>/g,'');
	  if (content.length >= 100) {
		  content = content.substr( 0, 100 ) + '......';
	  }
	  obj.find('.merged-content').text(content);
	  obj.find('.merged-create-at').text(mergedIdea.createdAt);
	  obj.find('.merged-img')[0].src = mergedIdea.profileUrl;
	  obj.find('.merged-link')[0].href = $('#contextPath').val() + 'users/' + mergedIdea.userId;
	  obj.find('.merged-name').text(mergedIdea.userName);
	  
	  obj[0].classList.remove("d-none");
	  obj[0].classList.remove("merged-card");
	  obj[0].classList.add("merged-element");
	  $('.merged-result-area').append(obj);
	}
  
    $('.merged-modal').find('.merged-result-area').height($('.merged-element').length * 100);
    
    const h = $(window).height()-1000;
    $('.modal-content').css('min-height', h + 'px');
	$('.merged-page').val(data.page);
	$('.merged-more-link').show();
	if (data.last) {
		$('.merged-more-link').hide();
	}
}

$(".merged-more-link").on("click", function() {
	searchMergedList(parseInt($('.merged-page').val())+1);
});
