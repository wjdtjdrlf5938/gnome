$(".setting-parent-btn, .setting-child-btn").on("click", function() {
	
	const area = $(this).closest('.grid-item');
	
	$(this).hide()
	area.find('.add-btn').removeClass("d-none")
	area.find('.check-btn').removeClass("d-none")
	area.find('.cencel-btn').removeClass("d-none")
	
	area.find('.category-link').hide();
	area.find('.category-input').show();
	area.find('.category-ul').addClass("sortable");
	
	$(".sortable").sortable();
	$(".sortable").disableSelection();
});


var tmp_id = 0;
$('.add-btn').on('click', function() {
	
	let categoryListName = "parentCategoryList";
	
	if($(event.currentTarget).attr('category') == 1) {
		categoryListName = "categoryList";
	}
	
	let obj = $(this).closest('.grid-item').find('.temp_element').clone(true);
	obj[0].classList.remove("temp_element");
	obj.find('.form-control')[0].name = categoryListName + '['+$('.fix-element').length+'].name';
	obj[0].classList.add("fix-element");
	obj[0].classList.add("clone-element");
	obj.show();
	obj[0].id=tmp_id;
	obj.find('.new-element').val(tmp_id);
	obj.find('.new-element')[0].name = categoryListName + '['+$('.fix-element').length+'].id';
	tmp_id = tmp_id-1;
	
	$(this).closest('.grid-item').find('.category-ul').append(obj);
	
	$(this).closest('.grid-item').find('.sortable').append(obj);
	$(this).closest('.grid-item').find('.sortable').sortable("refresh");
});


$(".check-btn").on("click", function() {
	
	const sortCnt = $(this).closest('.grid-item').find('.sortable').sortable("toArray");
	
	$('#parentCategoryOrder').val('');
	$('#categoryOrder').val('');
	
	if($(event.currentTarget).attr('category') == 1) {
		//子カテゴリーだけにsortCnt設定
		$('#categoryOrder').val(sortCnt);
	}else{
		//親カテゴリーだけにsortCnt設定
		$('#parentCategoryOrder').val(sortCnt);
	}
	
	const contextPath = $('#contextPath').val();
	
	const url = contextPath + 'auth/category/update'
	
	$('form').attr('action', url);
	$('form').submit();
	
});


$(".cencel-btn").on("click", function() {
	
	window.location.reload();
});


$('.delete-element').on('click', function() {
	const selectId = $(this).closest('.category-card').find('.category-link-id').val();
	
	$(this).parent().parent().parent().remove();
	
	if(selectId > 0) {
		let tartget = selectId;
		
		if($("#delTarget").val()) {
			tartget = ","+tartget;
		}
		$("#delTarget").val($("#delTarget").val()+tartget);
	}
});