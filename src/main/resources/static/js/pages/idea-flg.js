$('.flg-modal').on('show.bs.modal', function (event) {	
	$('body').addClass('open-modal');	
});

$('.flg-modal').on('hide.bs.modal', function (event) {	
	$('body').removeClass('open-modal');	
});


$('.flg-link').on('click', function() {
	
	
	$("#successArea").hide();
	$("#successFlgArea").hide();
	$('#buttonFlgArea').show();
	$('.flg-modal-body').show();
	
	$('#flgCommentId').val('');
	$('#flgContents').val('');
	
	$('#flgCommentId').val($(event.currentTarget).attr('commentId'));
});


$('.flg-modal').on('click', '.modal-footer #flgBtn', function() {
	
	$("#flgBtn").hide();
	$("#loadingBtn").show();
	
	const param = {
		ideaId: $('#ideaId').val(),
		commentId: $('#flgCommentId').val(),
		contents: $('#flgContents').val()
	};
	
	const contextPath = $('#contextPath').val();
	
	$.ajax({
        type : "POST",
        url : contextPath + "reports/create",
        data: param,
        dataType : "json",
        beforeSend: function (xhr, settings) {
            setCsrfTokenToAjaxHeader();
        },
        success : function(data, status, xhr) {
        	flgSuccessProc(data);
        },
        error : function(XMLHttpRequest, status, errorThrown) {
        	if (XMLHttpRequest.status===401) {
        		window.location.href = $("#contextPath").val() + "login?timeout";
        		return;
        	}
        	window.location.href = 'error';
        }
	});
});


function flgSuccessProc(data){
	
	$("#flgBtn").show();
	$("#loadingBtn").hide();
	
	if(data.errorMsg) {
		errorToastr(data.errorMsg);
		return;
	}
	
	$('#buttonFlgArea').hide();
	$("#successArea").show();
	$("#successFlgArea").show();
	$('.flg-modal-body').hide();
}
