$('iframe').on("load", function(){
	
	$('iframe').contents().find('body').css('word-break','break-all');
	
    try {
        $(this).height(0);
        $(this).height(this.contentWindow.document.documentElement.scrollHeight);
    } catch (e) {
    }
}).trigger('load');

$('iframe').trigger("load");

//チャート
$ (function () {
	
	$('#vote-pieChart').hide();
	$('#not-vote').hide();
	
	if($('#voteCount').val() == 0) {
		$('#not-vote').show();
	}else{
		$('#vote-pieChart').show();
	};
	
	const ctxP = document.getElementById("vote-pie-chart").getContext('2d');
	
	const upCount = $('#upCount').val();
	const downCount = $('#downCount').val();
	
	var myPieChart = new Chart(ctxP, {
		type: 'pie',
		data: {
			labels: ["賛成", "反対"],
			datasets: [{
				data: [upCount, downCount],
				backgroundColor: ["#04844B", "#949FB1"],
				hoverBackgroundColor: ["#04844B", "#949FB1"]
			}]
		},
		options: { 
			responsive: true,
			legend: {
			      position: 'right'
			}
		}
	});
});


$('.mergeList-modal').on('show.bs.modal', function (event) {
	
	$('body').addClass('open-modal');	
});

$('.mergeList-modal').on('hide.bs.modal', function (event) {	
	$('body').removeClass('open-modal');	
});


$('.up-vote-btn, .down-vote-btn').on('click', function(event) {
	
	const eventTaget = $(event.currentTarget).closest('.idea-btn-vote');
	
	eventTaget.find('.pointer-events').hide();
	
	const id = $(event.currentTarget).attr('id');
	
	const key = $(event.currentTarget).attr('select');
	
	const param = {
			ideaId: id,
			Agreed: key === "1"
	}
	
	const contextPath = $('#contextPath').val();
	
	$.ajax({
        type : "POST",
        url : contextPath + "vote",
        data: param,
        dataType : "json",
        beforeSend: function (xhr, settings) {
            setCsrfTokenToAjaxHeader();
        },
        success : function(data, status, xhr) {
        	window.location.reload();
        },
        error : function(XMLHttpRequest, status, errorThrown) {
        	if (XMLHttpRequest.status===401) {
        		 window.location.href = $("#contextPath").val() + "login?timeout";
        		 return;
        	}
        	window.location.href = 'error';
        }
	});	
});

$('.vote-selected-btn').on('click', function(event) {
	
	const eventTaget = $(event.currentTarget).closest('.idea-btn-vote');
	
	eventTaget.find('.pointer-events').hide();
	
	const id = $(event.currentTarget).attr('id');
	
	const param = {
			ideaId: id
	}
	
	const contextPath = $('#contextPath').val();
	
	$.ajax({
        type : "DELETE",
        url : contextPath + "vote/delete",
        data: param,
        dataType : "json",
        beforeSend: function (xhr, settings) {
            setCsrfTokenToAjaxHeader();
        },
        success : function(data, status, xhr) {
        	window.location.reload();
        },
        error : function(XMLHttpRequest, status, errorThrown) {
			if (XMLHttpRequest.status===401) {
        		window.location.href = $("#contextPath").val() + "login?timeout";
        		return;
        	}
        	window.location.href = 'error';
        }
	});	
});


$('.comment-btn').on('click', function(event) {

	const id = $(event.currentTarget).attr('id');
	
	const contents = $('.idea-contens').val();
	
	const param = {
			ideaId: id,
			contents: contents
	}
	
	const contextPath = $('#contextPath').val();
	
	$.ajax({
        type : "POST",
        url : contextPath + "comments/create",
        data: param,
        dataType : "json",
        beforeSend: function (xhr, settings) {
            setCsrfTokenToAjaxHeader();
        },
        success : function(data, status, xhr) {
        	commentSuccessProc(data);
        },
        error : function(XMLHttpRequest, status, errorThrown) {
        	if (XMLHttpRequest.status===401) {
        		window.location.href = $("#contextPath").val() + "login?timeout";
        		return;
        	}
        	window.location.href = 'error';
        }
	});	
});


//コメントソート
$(".custom-option").on("click", function() {
	
	var url = $('#ideaId').val()　+ "?commentSort=" + $('.drop-checkbox').val()
	
	$('form').attr('action', url);
	$('form').submit();
});



function commentSuccessProc(data) {
	
	if(data.errorMsg) {
		errorToastr(data.errorMsg);
		return;
	}
	window.location.reload();
};

